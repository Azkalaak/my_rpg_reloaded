@startuml





/' Objects '/

namespace Titane {
	class Camera {
		+Camera()
		-position : EntityMovement
		+getPosition() : EntityMovement&
		+updatePosition(const EntityMovement& newPosition) : void
	}

	class CameraController {
		+CameraController(EntityMovement& player)
		+~CameraController()
		-_player : EntityMovement&
		-_direction : EntityMovement::Direction
		-_moveCounter : int
		-_speed : int
		-_waitRotation : int
		-_callbackMovement : size_t
		+activate(UserEvents& events) : void
		+deactivate(UserEvents& events) : void
		-move() : void
		+tick() : void
	}

	class CollisionManager {
		+CollisionManager(const MapManager& mapManager)
		-_mapManager : const MapManager&
		-_colliderList : std::list<EntityMovement*>
		+registerCharacter(EntityMovement& character) : void
		+tick() : void
		+unregisterCharacter(EntityMovement& character) : void
	}

	class ControllerManager {
		+ControllerManager(EntityMovement& player, UserEvents& events)
		-_cameraController : CameraController
		-_emptyController : EmptyController
		-_actualController : IClassicController*
		-_events : UserEvents&
		+changeController(ControllerType type) : void
		+tick() : void
	}

	class EditableAttacks {
		-_storage : StorageAttacks
		+doesAttackExist(const std::string& attackName) : bool {query}
		+getAttackId(const std::string& attackName) : int {query}
		-_attacks : std::list<AttackNode>
		+addAttack(const AttackNode& attack) : void
		+addAttack(const std::string& name, int id, int type, int category, int power, int precision, int pp, int effect1, int effect2) : void
		+load() : void
		+save() : void
		+verify() : void
	}

	class EditableEncounters {
		-_storage : StorageEncounter
		-_path : const std::string
		-_nodes : std::map<int, std::list<encounterNode>>
		+addEncounter(int mapId, const encounterNode& node) : void
		+addEncounter(int mapId, int pokemonId, int minLevel, int maxLevel) : void
		+load() : void
		+save() : void
		+verify() : void
	}

	class EditableEvolutions {
		-_storage : StorageEvolutions
		-_path : const std::string
		-_nodes : std::list<EvolutionNode>
		+addEvolution(const EvolutionNode& evolution) : void
		+addEvolution(int idFrom, int idTo, int condition) : void
		+load() : void
		+save() : void
		+verify() : void
	}

	class EditableListPokemonsAttacks {
		-_storage : StorageLinkPokemonsAttacks
		-_path : const std::string
		-_nodes : std::map<int, std::list<linkPokemonsAttacksNode>>
		+addAttack(int pokemonId, const linkPokemonsAttacksNode& attack) : void
		+addAttack(int pokemonId, int attackId, int condition) : void
		+load() : void
		+save() : void
		+verify() : void
	}

	class EditableMap {
		+EditableMap(const char* str)
		+~EditableMap()
		-map : StorageMap
		-_tpHolders : std::list<TPHolder>
		-path : std::string
		+addTP(const TPHolder& tpHolder) : void
		+addTP(const Zenith::ZenithVector2i& TPFrom, const Zenith::ZenithVector2i& TPTo, Maps mapTo) : void
		+changeMap(const char* str) : void
		+removeTP(const TPHolder& tpHolder) : void
		+removeTP(const Zenith::ZenithVector2i& TPFrom, Maps mapTo) : void
		+saveMap() : void
		+verify() : void
	}

	class EditablePokemons {
		-_storage : StoragePokemon
		-_path : const std::string
		-_pokemons : std::list<Pokemon>
		+addPokemon(const Pokemon& pokemon) : void
		+addPokemon(const std::string& name, int id, int hp, int attack, int defence, int specialAttack, int specialDefence, int speed, StoragePokemon::Types type1, StoragePokemon::Types type2) : void
		+load() : void
		+save() : void
		+verify() : void
	}

	class EmptyController {
		+~EmptyController()
		+activate(UserEvents& events) : void
		+deactivate(UserEvents& events) : void
		+tick() : void
	}

	class Encounters {
		#Encounters()
		+~Encounters()
		-_storage : StorageEncounter
		-_path : const std::string
		+getEncounter(Maps mapId) : encounterNode&
		-_random : std::random_device
		+reload() : void
	}

	class Entity {
		+Entity(sf::RenderWindow& windows, UserEvents& events, MapManager& map)
		-_controls : ControllerManager
		+transform : EntityMovement
		-_playerRenderer : PlayerRenderer
		-_triggers : TriggerManager
		-id : int
		-_team : std::array<Pokemon, 6>
		+activate() : void
		+deactivate() : void
		+render() : void
		+tick() : void
		+triggers(TitaneEngine* engine) : void
	}

	class EntityMovement {
		+direction : Direction
		+position : Zenith::ZenithVector2i
		+speedVector : Zenith::ZenithVector2i
		+idle : bool
		+operator==(const EntityMovement& character2) : bool {query}
		+speed : int
	}

	class Evolutions {
		#Evolutions()
		+~Evolutions()
		-_storage : StorageEvolutions
		+doesEvolve(int pkmId, int lvl) : bool {query}
		-_path : const std::string
		+getEvolution(int pkmId, int lvl) : int {query}
		+reload() : void
	}

	class Fader {
		+fadeIn(uint8_t increment) : bool
		+fadeOut(uint8_t increment) : bool
		-_colour : sf::Color
		+getColour() : sf::Color& {query}
		-_currAlpha : uint8_t
		+setColour(const sf::Color& newColour) : void
	}

	abstract class IClassicController {
		+~IClassicController()
		+{abstract} activate(UserEvents& events) : void
		+{abstract} deactivate(UserEvents& events) : void
		+{abstract} tick() : void
	}

	abstract class IScene {
		+IScene(TitaneEngine* engine, Titane::UserEvents& event)
		+~IScene()
		#_event : Titane::UserEvents&
		+{abstract} activate() : void
		+{abstract} deactivate() : void
		+{abstract} display(sf::RenderWindow& window) : void
		+passData(const DataScene& data) : void
		+{abstract} tick(float elapsedTime) : void
		+updateEvent(sf::RenderWindow& window) : void
	}

	class ListPokemonsAttacks {
		+ListPokemonsAttacks()
		-_storage : StorageLinkPokemonsAttacks
		+doesLearnAttack(int pkmId, int lvl) : bool {query}
		-_path : const std::string
		+getLearnedAttack(int pkmId, int lvl) : int {query}
		+getAllLearnedAttack(int pkmId, int lvl) : std::list<int> {query}
		+getAllLearnedAttackBeforeLevel(int pkmId, int lvl) : std::list<int> {query}
		+reload() : void
	}

	class Logger {
		#Logger()
		+~Logger()
		-detailLevel : char
		-detailLevel : char
		-verboseLevel : char
		-{static} doesLog : static constexpr bool
		-{static} doesLog : static constexpr bool
		+changeVerbose(VerboseLevel level) : void
		+reportErr(const char* file, const char* function, int line, Args ... args) : void {query}
		+reportInfo(const char* file, const char* function, int line, Args ... args) : void {query}
		+reportRaw(Args ... args) : void {query}
		+reportWarn(const char* file, const char* function, int line, Args ... args) : void {query}
		-{static} sendMessage(std::ostream& stream, const Args& ... args) : void
	}

	class Map {
		+Map(const char* str)
		+~Map()
		-map : StorageMap
		+isTp(Zenith::ZenithVector2i position) : TPHolder& {query}
		+isCollision(Zenith::ZenithVector2i position) : bool {query}
		-path : const std::string
		+isGrass(Zenith::ZenithVector2i position) : int {query}
		+height() : uint16_t {query}
		+width() : uint16_t {query}
		+ForEach(const Zenith::ZenithVector2i& beginPoint, const Zenith::ZenithVector2i& endPoint, const std::function<fnc>& func) : void {query}
		+dumpAsText() : void {query}
		+verify() : void
	}

	class MapManager {
		+MapManager()
		+getMap(Maps id) : Map& {query}
		-actualMap : Maps
		+getMap() : Maps {query}
		+draw(const Zenith::ZenithVector2i& topLeft, const Zenith::ZenithVector2i& bottomRight, const std::function<void ( const MapNode&, Zenith::ZenithVector2i )>& func, Maps mapId) : void {query}
		+dumpMap() : void {query}
		+setMap(Maps id) : void
	}

	class MapRenderer {
		-_fullMapTest : sf::RenderTexture
		-_sprite : sf::Sprite
		-_vec : sf::Vector2f
		+RenderLayer(sf::RenderWindow& window, StorageToTextures::Layers layer, const MapManager& map, Zenith::ZenithVector2i tileTopLeft, Zenith::ZenithVector2i tileBotRight) : void
		+RenderLayerBeta(sf::RenderWindow& window, StorageToTextures::Layers layer, const MapManager& map) : void
		+init(const MapManager& map) : void
		+preRender(StorageToTextures::Layers layer, const MapManager& map, Maps actualMap) : void
	}

	class PlayerRenderer {
		+PlayerRenderer(sf::RenderWindow& window, EntityMovement& player)
		-_player : const EntityMovement&
		-backward : int
		-frame : int
		-_intRect : sf::IntRect
		-_window : sf::RenderWindow&
		-_sprite : sf::Sprite
		-_vec : sf::Vector2f
		-deltaFrame : uint8_t
		+RenderPlayer() : void
	}

	class Pokemon {
		+Pokemon()
		+Pokemon(const PokemonNode& nodePokemon)
		+Pokemon(const std::string& _name, int _id, int _hp, int _attack, int _defence, int _specialAttack, int _specialDefence, int _speed, StoragePokemon::Types _type1, StoragePokemon::Types _type2)
		+type1 : StoragePokemon::Types
		+type2 : StoragePokemon::Types
		+operator<(const Pokemon& pkm) : bool {query}
		+operator==(const Pokemon& pkm) : bool {query}
		+operator>(const Pokemon& pkm) : bool {query}
		+level : char
		+attack : int
		+defence : int
		+experience : int
		+hp : int
		+specialAttack : int
		+specialDefence : int
		+speed : int
		+name : std::string
	}

	class Pokemons {
		#Pokemons()
		+~Pokemons()
		+getPokemon(int pokemonId) : Pokemon {query}
		-_storage : StoragePokemon
		-_path : const std::string
		+reload() : void
	}

	class RendererManager {
		-_mapRenderer : MapRenderer
		+Render(sf::RenderWindow& window, const MapManager& map, std::list<Entity> &) : void
		+init(const MapManager& map) : void
	}

	class SceneCombat {
		+SceneCombat(TitaneEngine* engine, UserEvents& events)
		+~SceneCombat()
		-_encounterSavageData : DataSavageEncounter
		-_fader : Fader
		-_rect : sf::RectangleShape
		-_sprite : sf::Sprite
		+activate() : void
		+deactivate() : void
		+display(sf::RenderWindow& window) : void
		+passData(const DataScene& data) : void
		+tick(float elapsedTime) : void
	}

	class SceneMain {
		+SceneMain(TitaneEngine* engine, UserEvents& event, sf::RenderWindow& window)
		-_camera : Camera
		-_collisionManager : Titane::CollisionManager
		-_mapManager : Titane::MapManager
		-_renderManager : Titane::RendererManager
		-_entities : std::list<Entity>
		+activate() : void
		+deactivate() : void
		+display(sf::RenderWindow& window) : void
		+tick(float) : void
	}

	class SceneStartup {
		+SceneStartup(TitaneEngine* engine, UserEvents& events)
		-_fader : Fader
		-_hasBeenPressed : bool
		-_clock : sf::Clock
		-_font : sf::Font
		-_fade : sf::RectangleShape
		-_shader : sf::Shader
		-_sprite : sf::Sprite
		-_text : sf::Text
		+activate() : void
		+deactivate() : void
		+display(sf::RenderWindow& window) : void
		-eventHandler(const bool arr) : void
		-stateDraw(sf::RenderWindow& window) : void
		+tick(float elapsedTime) : void
	}

	class StorageAttacks {
		+~StorageAttacks()
		+header : AttackHeader
		+nodes : AttackNode*
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+calcCrc() : uint32_t {query}
		+unload() : void
	}

	class StorageEncounter {
		+~StorageEncounter()
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+header : encounterHeader
		+calcCrc() : uint32_t {query}
		+unload() : void
	}

	class StorageEvolutions {
		+~StorageEvolutions()
		+header : EvolutionHeader
		+nodes : EvolutionNode*
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+calcCrc() : uint32_t {query}
		+unload() : void
	}

	class StorageLinkPokemonsAttacks {
		+~StorageLinkPokemonsAttacks()
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+nodes : linkPokemonsAttacksNode**
		+calcCrc() : uint32_t {query}
		+pokemonAttackNumber : uint8_t*
		+unload() : void
	}

	class StorageMap {
		+~StorageMap()
		+infos : MapLineInfo*
		+map : MapNode**
		+tp : TPHolder*
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+calcCrc() : uint32_t {query}
		+unload() : void
	}

	class StoragePokemon {
		+~StoragePokemon()
		+pokemons : PokemonNode*
		+load(const std::string& path) : bool
		+write(const std::string& path) : bool {query}
		+calcCrc() : uint32_t {query}
		+unload() : void
	}

	class StorageToTextures {
		+verification() : bool
		+getCharacterTexture(Character charac) : char* {query}
		+getMapTexture(Layers layer, Maps key) : char* {query}
		+getTexture(Character charac) : char* {query}
		+getTexture(Layers layer, Maps key) : char* {query}
	}

	class TextureManager {
		#TextureManager()
		#~TextureManager()
		-_storageToTextures : StorageToTextures
		+Verification() : bool
		+getTexture(const std::string& texture) : sf::Texture&
		+getTexture(StorageToTextures::Layers layer, Maps mapId) : sf::Texture&
		+getTexture(StorageToTextures::Character character) : sf::Texture&
		-_textureList : std::map<std::string, sf::Texture>
		+loadIfNotLoaded(const std::string& texture) : void
		+loadIfNotLoaded(StorageToTextures::Layers layer, Maps mapId) : void
		+loadIfNotLoaded(StorageToTextures::Character character) : void
		+loadTexture(const std::string& texture) : void
		+loadTexture(StorageToTextures::Layers layer, Maps mapId) : void
		+loadTexture(StorageToTextures::Character character) : void
	}

	class TitaneEngine {
		+TitaneEngine()
		-_textures : Titane::StorageToTextures
		-_events : Titane::UserEvents
		+Initialise() : bool
		-_window : sf::RenderWindow
		-_scenes : std::vector<std::unique_ptr<Titane::IScene>>
		+changeScene(IScene::Scenes scene) : void
		+display() : void
		+loop() : void
		+manageEvent() : void
		+passDataToCurScene(const DataScene &) : void
		+tick(float elapsedTime) : void
	}

	class TriggerManager {
		+TriggerManager(MapManager& mapManager, EntityMovement& player)
		-_player : EntityMovement&
		-_mapManager : MapManager&
		-_random : std::random_device
		-savagePokemon(TitaneEngine* engine) : void
		-teleportation() : void
		+tick(TitaneEngine* engine) : void
	}

	class UserEvents {
		+ZenithEvent<void(const bool)
		+void() : Zenith::ZenithConditionalEvent<Zenith::TypeList<const bool [ LastEvent ]>,
		+update(sf::RenderWindow& window) : void
	}

	enum AttackEffectStruct::attackCategory {
		NONE
		PHYSICAL
		SPECIAL
	}

	enum AttackEffectStruct::attackEffect {
		AIM_DOWN
		AIM_UP
		AIM_UP_2
		ATTACK_NEXT_TURN
		ATTCK_DOWN
		ATTCK_SPE_DOWN
		ATTCK_SPE_UP
		ATTCK_SPE_UP_2
		ATTCK_UP
		ATTCK_UP_2
		BURN
		CHAINED_2
		CHAINED_INFINITE
		CHAINED_MAX_3
		CHAINED_MAX_5
		CHANGE_TYPE
		CLEAR_STATS
		DEF_DOWN
		DEF_DOWN_2
		DEF_SPE_DOWN
		DEF_SPE_UP
		DEF_SPE_UP_2
		DEF_UP
		DEF_UP_2
		DIZZY
		DMG_IS_20_PV
		DMG_IS_40_PV
		DMG_IS_50_PERCENT
		DMG_IS_DOUBLE_DAMAGE_TAKEN
		DMG_IS_LEVEL
		DMG_iS_DOUBLE
		DODGE_DOWN
		DODGE_UP
		DODGE_UP_2
		END_ATTACK_EFFECTS
		END_SAVAGE_ENCOUNTER
		FREEZE
		HEAL_50_PERCENT
		HEAL_DMG_INFLICTED
		HEAL_TOTAL
		HIGH_CRIT
		INVINCIBLE_DURING_CAST
		KO_IF_SPEED_ABOVE
		MORPHING
		NEEDS_ENNEMY_SLEEPING
		NO_EFFECT
		PARA
		PASS_NEXT_TURN
		POISON
		POISON_BAD
		POS_AIM_DOWN
		POS_ATTCK_DOWN
		POS_ATTCK_SPE_DOWN
		POS_BURN
		POS_DEF_DOWN
		POS_DEF_SPE_DOWN
		POS_DIZZY
		POS_DODGE_DOWN
		POS_FEAR
		POS_FREEZE
		POS_PARA
		POS_POISON
		POS_SLEEP
		POS_SPEED_DOWN
		POS_SPE_DOWN
		PREVENT_STATS_MOD
		PRIORITY
		REPLACE_ATTACK_WITH_RANDOM
		SELF_DAMAGE
		SELF_DAMAGE_IF_MISS
		SELF_DIZZY
		SELF_KO
		SELF_SLEEP
		SLEEP
		SPEED_DOWN
		SPEED_UP
		SPEED_UP_2
		SPE_DOWN
		SPE_UP
		SPE_UP_2
		TRAP_CHAIN_MAX_5
	}

	enum ControllerManager::ControllerType {
		EMPTY
		PLAYER
	}

	enum DataSceneEnum {
		DATA_EMPTY
		DATA_SAVAGE_ENCOUNTER
	}

	enum DetailLevel {
		FILE
		FUNCTION
		LINE
		NO_DETAIL
	}

	enum EntityMovement::Direction {
		DOWN
		LEFT
		NONE
		RIGHT
		UP
	}

	enum Events {
		A
		Add
		B
		Backslash
		Backspace
		C
		Comma
		D
		Delete
		Divide
		Down
		E
		End
		Enter
		Equal
		Escape
		F
		F1
		F10
		F11
		F12
		F13
		F14
		F15
		F2
		F3
		F4
		F5
		F6
		F7
		F8
		F9
		G
		H
		Home
		Hyphen
		I
		Insert
		J
		K
		L
		LAlt
		LBracket
		LControl
		LShift
		LSystem
		LastEvent
		LastKeyboardEvent
		Left
		LeftMouse
		M
		Menu
		MiddleMouse
		MouseButton1
		MouseButton2
		Multiply
		N
		Num0
		Num1
		Num2
		Num3
		Num4
		Num5
		Num6
		Num7
		Num8
		Num9
		Numpad0
		Numpad1
		Numpad2
		Numpad3
		Numpad4
		Numpad5
		Numpad6
		Numpad7
		Numpad8
		Numpad9
		O
		P
		PageDown
		PageUp
		Pause
		Period
		Q
		Quote
		R
		RAlt
		RBracket
		RControl
		RShift
		RSystem
		Right
		RightMouse
		S
		Semicolon
		Slash
		Space
		Subtract
		T
		Tab
		Tilde
		U
		Up
		V
		W
		X
		Y
		Z
	}

	enum EvolutionCondition {
		ECHANGE
		END_EVOLUTION_CONDITION
		PIERRE_EAU
		PIERRE_FEU
		PIERRE_FOUDRE
		PIERRE_LUNE
		PIERRE_PLANTE
	}

	enum IScene::Scenes {
		END_SCENES
		FIGHT
		MAIN
		STARTUP
	}

	enum Maps {
		BOURG_JOY
		CONTREFORT
		CREPUSCUL_AIRE
		CRYONIS
		GEJUDO
		GOUFFRE_ENTRENEIGE
		KORALGA
		LIGUE_POKEMON_EXT
		LIGUE_POKEMON_INT
		MAP_END
		PRAIMISSE
		ROUTE1
		ROUTE2
		ROUTE3
		ROUTE4
		ROUTE5
		ROUTE6
		ROUTE7
		ROUTE8
		ROUTE_VICTOIRE
		SEOULOPOLIS
		VOLCAN_KIBRUL
	}

	enum StoragePokemon::Types {
		ACIER
		AUCUN
		COMBAT
		DRAGON
		EAU
		ELECTRIK
		FEU
		GLACE
		INSECTE
		NORMAL
		PLANTE
		POISON
		PSY
		ROCHE
		SOL
		SPECTRE
		TENEBRE
		TYPES_END
		VOL
	}

	enum StorageToTextures::Character {
		CHAR_END
		MAN
		WOMAN
	}

	enum StorageToTextures::Layers {
		BOT
		COL
		LAY_END
		TOP
	}

	enum VerboseLevel {
		ERROR
		INFO
		NO_VERBOSE
		WARNING
	}

	class AttackEffectStruct {
	}

	class AttackHeader {
		+crc : uint32_t
		+nodeNumber : uint8_t
	}

	class AttackNode {
		+category : uint8_t
		+effect1 : uint8_t
		+effect2 : uint8_t
		+id : uint8_t
		+power : uint8_t
		+pp : uint8_t
		+precision : uint8_t
		+type : uint8_t
	}

	class DataSavageEncounter {
	}

	class DataScene {
	}

	class EvolutionHeader {
		+crc : uint32_t
		+nodeNumber : uint8_t
	}

	class EvolutionNode {
		+condition : uint8_t
		+idBase : uint8_t
		+idEvolution : uint8_t
	}

	class MapHeader {
		+mapID : Maps
		+mapHeight : uint16_t
		+crc : uint32_t
		+TPNumber : uint8_t
	}

	class MapLineInfo {
		+width : uint16_t
	}

	class MapNode {
		+bg : uint8_t
		+collision : uint8_t
		+mid : uint8_t
		+top : uint8_t
		+zoneID : uint8_t
	}

	class PokemonHeader {
		+pokemonNumber : uint16_t
		+crc : uint32_t
	}

	class PokemonNode {
		+id : uint16_t
		+attack : uint8_t
		+defence : uint8_t
		+hp : uint8_t
		+specialAttack : uint8_t
		+specialDefence : uint8_t
		+speed : uint8_t
		+type1 : uint8_t
		+type2 : uint8_t
	}

	class TPHolder {
		+mapKey : Maps
		+TPFromX : uint16_t
		+TPFromY : uint16_t
		+TPToX : uint16_t
		+TPToY : uint16_t
	}

	class encounterHeader {
		+crc : uint32_t
	}

	class encounterNode {
		+maxLvl : uint8_t
		+minLvl : uint8_t
		+pokemonId : uint8_t
	}

	class linkPokemonsAttacksHeader {
		+crc : uint32_t
		+nodeNumber : uint32_t
	}

	class linkPokemonsAttacksNode {
		+attackId : uint8_t
		+condition : uint8_t
	}

	class UDataScene {
		+encounter : DataSavageEncounter*
		+i : int
	}
}


namespace Zenith {
	class Utils {
		+{static} split(std::string str, std::function<T ( const std::string& )> func, const std::string& sep) : std::vector<T>
	}

	class ZenithCRC {
		-crc : uint32_t
		+getCRC() : uint32_t {query}
		+feed(const void* buff, size_t buffSize) : void
		+setCRC(uint32_t newCRC) : void
	}

	class ZenithException {
		+ZenithException(std::string message)
		+ZenithException(Args ... args)
		+what() : char* {query}
		-concatenateMessage(std::stringstream& stream, Arg arg) : void {query}
		-concatenateMessage(std::stringstream& stream, Arg arg, Args ... args) : void {query}
	}

	class ZenithGlobalSingleton <template<typename Child>> {
		#ZenithGlobalSingleton()
		#~ZenithGlobalSingleton()
		+{static} instance() : Child&
	}

	class ZenithLocalSingleton <template<class childName>> {
		#ZenithLocalSingleton()
		+~ZenithLocalSingleton()
		-{static} singletonLock : static std::mutex
	}

	class ZenithVector <template<size_t size, typename vecType>> {
		+ZenithVector()
		+ZenithVector(Args ... args)
		+ZenithVector(std::initializer_list<vecType> array)
		-internal_add(firstVec& vec1, secondVec& vec2) : firstVec&
		-internal_cross(firstVec& vec1, secondVec& vec2) : firstVec&
		-internal_sub(firstVec& vec1, secondVec& vec2) : firstVec&
		+getAbsolute() : float
		-internal_abs() : float
		-internal_dot(firstVec& vec1, secondVec& vec2) : float
		+getNormalized() : thisVec
		-<(index<size ), void>::type constructor ( vecType init) : typename std::enable_if
		-<(index<size ), void>::type internal_clamp ( vecType valueMin, vecType valueMax) : typename std::enable_if
		+<(sizeof ... ( args ) <) : typename std::enable_if
		+<(sizeof ... ( Args ) <) : typename std::enable_if
		+operator[](size_t index) : vecType {query}
		+operator[](size_t index) : vecType&
		-constructor() : void
		-constructor() : void
		-constructor(vecType init, Args ... args) : void
		+forEach(std::function<vecType ( unsigned char )> func) : void
		-internal_clamp(vecType valueMin, vecType valueMax, Args ... args) : void
		+round(float roundness) : void
	}

	class ZenithVectorPhysic <template<unsigned char size, typename vecType>> {
		+ZenithVectorPhysic()
		+ZenithVectorPhysic(ZenithVector<size, vecType> acceleration, ZenithVector<size, vecType> speed)
		+ZenithVectorPhysic(ZenithVector<size, vecType> acceleration, vecType mass, ZenithVector<size, vecType> speed)
		+getAcceleration() : ZenithVector<size , vecType>& {query}
		+getSpeed() : ZenithVector<size , vecType>& {query}
		-m_force : ZenithVector<size, vecType>
		-m_speed : ZenithVector<size, vecType>
		+operator+(const ZenithVectorPhysic<size, vecType>& other) : ZenithVectorPhysic<size , vecType>
		-isForceAcceleration : bool
		+bounciness : vecType
		-densityOfOutside : vecType
		-dragCoefficient : vecType
		+mass : vecType
		+calcSpeed(float deltaTime) : void
		+setSpeed(ZenithVector<size, vecType> speed) : void
	}

	class TypeList <template<typename ... aAgs>> {
	}

	namespace Abstracts {
		abstract class ZenithCollider {
			+ZenithCollider()
			+{abstract} ~ZenithCollider()
			+doesCollide(const ZenithCollider& zenithCollider) : int
		}
	}
}





/' Inheritance relationships '/

Titane.IClassicController <|-- Titane.CameraController


Titane.IClassicController <|-- Titane.EmptyController


Titane.IScene <|-- Titane.SceneCombat


Titane.IScene <|-- Titane.SceneMain


Titane.IScene <|-- Titane.SceneStartup





/' Aggregation relationships '/

Titane.Camera *-- Titane.EntityMovement


Titane.CameraController "2" *-- Titane.EntityMovement


Titane.CameraController *-- Titane.EntityMovement::Direction


Titane.CollisionManager o-- Titane.EntityMovement


Titane.CollisionManager *-- Titane.MapManager


Titane.ControllerManager *-- Titane.CameraController


Titane.ControllerManager *-- Titane.EmptyController


Titane.ControllerManager o-- Titane.IClassicController


Titane.ControllerManager *-- Titane.UserEvents


Titane.EditableAttacks *-- Titane.AttackNode


Titane.EditableAttacks *-- Titane.StorageAttacks


Titane.EditableEncounters *-- Titane.StorageEncounter


Titane.EditableEncounters *-- Titane.encounterNode


Titane.EditableEvolutions *-- Titane.EvolutionNode


Titane.EditableEvolutions *-- Titane.StorageEvolutions


Titane.EditableListPokemonsAttacks *-- Titane.StorageLinkPokemonsAttacks


Titane.EditableListPokemonsAttacks *-- Titane.linkPokemonsAttacksNode


Titane.EditableMap *-- Titane.StorageMap


Titane.EditableMap *-- Titane.TPHolder


Titane.EditablePokemons *-- Titane.Pokemon


Titane.EditablePokemons *-- Titane.StoragePokemon


Titane.Encounters *-- Titane.StorageEncounter


Titane.Entity *-- Titane.ControllerManager


Titane.Entity *-- Titane.EntityMovement


Titane.Entity *-- Titane.PlayerRenderer


Titane.Entity *-- Titane.Pokemon


Titane.Entity *-- Titane.TriggerManager


Titane.Evolutions *-- Titane.StorageEvolutions


Titane.IScene *-- Titane.UserEvents


Titane.ListPokemonsAttacks *-- Titane.StorageLinkPokemonsAttacks


Titane.Map *-- Titane.StorageMap


Titane.MapHeader *-- Titane.Maps


Titane.MapManager *-- Titane.Maps


Titane.PlayerRenderer *-- Titane.EntityMovement


Titane.Pokemon "2" *-- Titane.StoragePokemon


Titane.Pokemon "2" *-- Titane.StoragePokemon::Types


Titane.Pokemons *-- Titane.StoragePokemon


Titane.RendererManager *-- Titane.MapRenderer


Titane.SceneCombat *-- Titane.DataSavageEncounter


Titane.SceneCombat *-- Titane.Fader


Titane.SceneMain *-- Titane.Camera


Titane.SceneMain *-- Titane.CollisionManager


Titane.SceneMain *-- Titane.Entity


Titane.SceneMain *-- Titane.MapManager


Titane.SceneMain *-- Titane.RendererManager


Titane.SceneStartup *-- Titane.Fader


Titane.StorageAttacks *-- Titane.AttackHeader


Titane.StorageAttacks o-- Titane.AttackNode


Titane.StorageEncounter *-- Titane.encounterHeader


Titane.StorageEvolutions *-- Titane.EvolutionHeader


Titane.StorageEvolutions o-- Titane.EvolutionNode


Titane.StorageLinkPokemonsAttacks o-- Titane.linkPokemonsAttacksNode


Titane.StorageMap o-- Titane.MapLineInfo


Titane.StorageMap o-- Titane.MapNode


Titane.StorageMap o-- Titane.TPHolder


Titane.StoragePokemon o-- Titane.PokemonNode


Titane.TPHolder *-- Titane.Maps


Titane.TextureManager *-- Titane.StorageToTextures


Titane.TitaneEngine *-- Titane.IScene


Titane.TitaneEngine *-- Titane.StorageToTextures


Titane.TitaneEngine *-- Titane.UserEvents


Titane.TriggerManager *-- Titane.EntityMovement


Titane.TriggerManager *-- Titane.MapManager


Titane.UDataScene o-- Titane.DataSavageEncounter


Zenith.ZenithVectorPhysic "2" *-- Zenith.ZenithVector






/' Nested objects '/

Titane.AttackEffectStruct +-- Titane.AttackEffectStruct::attackCategory


Titane.AttackEffectStruct +-- Titane.AttackEffectStruct::attackEffect


Titane.ControllerManager +-- Titane.ControllerManager::ControllerType


Titane.EntityMovement +-- Titane.EntityMovement::Direction


Titane.IScene +-- Titane.IScene::Scenes


Titane.StoragePokemon +-- Titane.StoragePokemon::Types


Titane.StorageToTextures +-- Titane.StorageToTextures::Character


Titane.StorageToTextures +-- Titane.StorageToTextures::Layers




@enduml
