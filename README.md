# Pokemon Titane

[![pipeline status](https://gitlab.com/Azkalaak/my_rpg_reloaded/badges/master/pipeline.svg)](https://gitlab.com/Azkalaak/my_rpg_reloaded/-/commits/master)
[![coverage report](https://gitlab.com/Azkalaak/my_rpg_reloaded/badges/master/coverage.svg)](https://gitlab.com/Azkalaak/my_rpg_reloaded/-/commits/master)
***

## Preparation to compilation instruction:

- Install a cpp compiler (visual studio 2022 on windows, gcc / clang on unix)
- Install conan via pip3 (install python3)
- Install Cmake and add it into the path
- create a `build` directory inside the root folder
- Run the following command:
  ```cmake -B./build```

Now the compilation in prepared, simply run the command ```cmake --build build``` and voila

### In case the cmake command failed:

- a conan error is displayed means you likely need to manually install some dependencies (mandatory on some OS)
- a conan error is displayed and the previous point is invalid means you probably need to remove the bincrafters repository manually
- a build error (there is no \<format\> or a cpp20 error) install last version of cpp20, or re-run the first cmake
  command and add to it `-DNO_FULL_CXX_20=ON`. __Some features WILL be disabled into the code with this flag!__

***

## WARNING

The repository holding the dependencies for the project changed recently, any old bincrafters server used in conan is
now obsolete and cannot work. please delete the bincrafters server if you proviously were using it, else, it will fail

Conan being what it is, there may be problems using it because of updates, or outdated packages.

Use other compiler than standard ones for your OS at your own risks! Project has been tested on windows 11 with clang (MinGW build), and on last fedora version using clang and g++.
***

## Execution

If you do want to run the program, please note that:
You will need to compile first pokemon titane, then pokemon titane excel editor, run the excel editor then run the game.

The excel editor currently adds the teleport points into the game. You will not be able to leave the main map if you do not run it.

***