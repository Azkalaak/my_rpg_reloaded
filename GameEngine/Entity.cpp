//
// Created by azkal on 08/02/2022.
//

#include "Entity.hpp"

using namespace Titane;

void Entity::activate()
{
	if (id == 0)
		_controls.changeController(ControllerManager::PLAYER);
}

void Entity::deactivate()
{
	_controls.changeController(ControllerManager::EMPTY);
}


void Entity::tick()
{
	_controls.tick();
}

void Entity::triggers(TitaneEngine *engine)
{
	_triggers.tick(engine);
}

void Entity::render() noexcept
{
	_playerRenderer.RenderPlayer();
}
