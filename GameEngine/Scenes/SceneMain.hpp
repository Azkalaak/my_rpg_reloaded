//
// Created by azkal on 03/03/2022.
//

#ifndef POKEMONTITANE_SCENEMAIN_HPP
#define POKEMONTITANE_SCENEMAIN_HPP

#include "IScene.hpp"
#include "Map/MapManager.hpp"
#include "Collisions/CollisionManager.hpp"
#include "Renderer/RendererManager.hpp"
#include "Camera.hpp"

namespace Titane {
	class SceneMain : public IScene {
	private:
		Titane::MapManager _mapManager;
		Titane::RendererManager _renderManager;
		Titane::CollisionManager _collisionManager;
		std::list<Entity> _entities;
		Camera _camera;

	public:
		SceneMain(TitaneEngine *engine, UserEvents &event, sf::RenderWindow &window);

		void activate() override;

		void deactivate() override;

		void tick(float) override;

		void display(sf::RenderWindow &window) override;
	};
}

#endif //POKEMONTITANE_SCENEMAIN_HPP
