//
// Created by azkal on 03/03/2022.
//

#include "IScene.hpp"

using namespace Titane;

IScene::IScene(TitaneEngine *engine, Titane::UserEvents &event) : _engine(engine), _event(event)
{

}

IScene::~IScene() = default;

void IScene::updateEvent(sf::RenderWindow &window)
{
	_event.update(window);
}

void IScene::passData(const DataScene &)
{

}
