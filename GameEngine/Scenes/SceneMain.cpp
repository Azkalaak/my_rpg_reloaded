//
// Created by azkal on 03/03/2022.
//

#include "SceneMain.hpp"

using namespace Titane;

SceneMain::SceneMain(TitaneEngine *engine, UserEvents &event, sf::RenderWindow &window) : IScene(engine, event),
																						  _collisionManager(_mapManager)
{
	// ==================
	// TEMPORARY PART
	// WILL BE REPLACED BY EITHER THE LOADING OF A SAVE OR A DEFAULT CONFIGURATION
	// ==================
	_entities.emplace_back(window, _event, _mapManager);
	_entities.begin()->transform.position = {{630, 832}};

	_mapManager.setMap(BOURG_JOY);
	_collisionManager.registerCharacter(_entities.begin()->transform);
	_renderManager.init(_mapManager);
}

void SceneMain::activate()
{
	for (auto &it : _entities)
		it.activate();
}

void SceneMain::deactivate()
{
	for (auto &it : _entities)
		it.deactivate();
}

void SceneMain::tick(float)
{
	for (auto &it : _entities)
	{
		it.tick();
		it.triggers(_engine);
	}

	if (_entities.begin()->transform.position != _camera.getPosition().position)
		_camera.updatePosition(_entities.begin()->transform);
	_collisionManager.tick();
}

void SceneMain::display(sf::RenderWindow &window)
{
	auto view = window.getView();

	view.setCenter(static_cast<float>(_camera.getPosition().position[1]), static_cast<float>(_camera.getPosition().position[0]));
	window.setView(view);
	_renderManager.Render(window, _mapManager, _entities);
}
