//
// Created by azkal on 05/03/2022.
//

#include "SceneCombat.hpp"
#include "AssetsManagers/TextureManager.hpp"
#include "TitaneEngine.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

SceneCombat::SceneCombat(TitaneEngine *engine, UserEvents &events) : IScene(engine, events)
{
	TitaneTextureManager.loadTexture("GameData/textures/fight/back.png");
	TitaneTextureManager.loadIfNotLoaded("GameData/textures/pokemons.png");

	auto tmpColour = sf::Color::White;
	_fader.setColour(tmpColour);
	_rect.setFillColor(tmpColour);

	auto &texture = TitaneTextureManager.getTexture("GameData/textures/fight/back.png");
	auto size = texture.getSize();
	_sprite.setOrigin(static_cast<float>(size.x) / 2, static_cast<float>(size.y) / 2);
	_sprite.setTexture(texture);

	_font.loadFromFile("GameData/Fonts/font.ttf");
	_text.setFont(_font);
	tmpColour.r = 72;
	tmpColour.g = 72;
	tmpColour.b = 72;
	tmpColour.a = 255;
	_text.setFillColor(tmpColour);
	_text.setOutlineColor(tmpColour);
	_text.setOutlineThickness(0);
	_text.setLetterSpacing(0.5f);
	_text.setStyle(sf::Text::Style::Bold);
}

SceneCombat::~SceneCombat() noexcept = default;

void SceneCombat::activate()
{
	Titane::TitaneLogger.reportInfo("Combat scene");
	auto tmpColour = sf::Color::White;
	_fader.setColour(tmpColour);
	_rect.setFillColor(tmpColour);
	eventId = _event.callback += [this](const bool arr[]) {
		if (!arr[Escape] && _evtButtonLast[0])
			_engine->changeScene(MAIN);
		_evtButtonLast[0] = arr[Escape];
	};
	_state = 0;
}

void SceneCombat::deactivate()
{
	_event.callback -= eventId;
	_evtButtonLast[0] = false;
	eventId = 0;
}

void SceneCombat::tick(float)
{
	switch (_state) {
		case 0:
			 if (_fader.fadeOut(4))
			 	_state = 1;
			break;
	}
	if (_encounterSavageData.player[actualPkm].actualHp < _encounterSavageData.player[actualPkm].getMaxHp() / 4)
		_color[0] = sf::Color::Red;
	else if (_encounterSavageData.player[actualPkm].actualHp < _encounterSavageData.player[actualPkm].getMaxHp() / 2)
		_color[0] = sf::Color::Yellow;
	else
		_color[0] = sf::Color::Green;
	if (_encounterSavageData.enemy.actualHp < _encounterSavageData.enemy.getMaxHp() / 4)
		_color[1] = sf::Color::Red;
	else if (_encounterSavageData.enemy.actualHp < _encounterSavageData.enemy.getMaxHp() / 2)
		_color[1] = sf::Color::Yellow;
	else
		_color[1] = sf::Color::Green;
}

void SceneCombat::displayTextures(sf::RenderWindow &window, const sf::Vector2f &center)
{
	// BACKGROUND
	auto &texture = TitaneTextureManager.getTexture("GameData/textures/fight/back.png");
	auto size = texture.getSize();
	_sprite.setTexture(texture, true);
	_sprite.setOrigin(static_cast<float>(size.x) / 2, static_cast<float>(size.y) / 2);
	_sprite.setPosition(center.x, center.y - 100);
	_sprite.setScale(1, 1);
	window.draw(_sprite);

	// ENNEMY POKEMON (using a rect because sfml sprite resize is garbage)
	_rect.setOrigin(32, 32);
	_rect.setSize(sf::Vector2f(64, 64));
	_rect.setFillColor(sf::Color::White);
	_rect.setTexture(&TitaneTextureManager.getTexture("GameData/textures/pokemons.png"), true);
	_rect.setTextureRect(sf::IntRect(128, 0 + 64 * _encounterSavageData.enemy.id, 64, 64));
	_rect.setPosition(center.x + 220, center.y - 120);
	_rect.setScale(4, 4);
	window.draw(_rect);

	// ALLY POKEMON (actually just a magikarp because why not)
	_rect.setTextureRect(sf::IntRect(256, 0 + 64 * _encounterSavageData.player[actualPkm].id , 64, 64));
	_rect.setPosition(center.x - 180, center.y - 8);
	_rect.setScale(4, 4);
	window.draw(_rect);
}

void SceneCombat::displayText(sf::RenderWindow &window, const sf::Vector2f &center)
{
	_text.setCharacterSize(22);
	// enemy name
	_text.setStyle(sf::Text::Style::Bold);
	_text.setString(_encounterSavageData.enemy.name);
	_text.setOrigin(_text.getLocalBounds().width, 0);
	_text.setPosition(center.x - 280, center.y - 233);
	window.draw(_text);

	// enemy level
	_text.setStyle(sf::Text::Style::Regular);
	_text.setString(std::to_string(_encounterSavageData.enemy.level));
	_text.setOrigin(0, 0);
	_text.setPosition(center.x - 230, center.y - 233);
	window.draw(_text);

	// ally name
	_text.setStyle(sf::Text::Style::Bold);
	_text.setString(_encounterSavageData.player[actualPkm].name);
	_text.setOrigin(_text.getLocalBounds().width, 0);
	_text.setPosition(center.x + 373, center.y + 32);
	window.draw(_text);

	// Ally level
	_text.setStyle(sf::Text::Style::Regular);
	_text.setString(std::to_string(_encounterSavageData.player[actualPkm].level));
	_text.setOrigin(0, 0);
	_text.setPosition(center.x + 420, center.y + 32);
	window.draw(_text);

	// ally max hp
	_text.setString(std::to_string(_encounterSavageData.player[actualPkm].getMaxHp()));
	_text.setOrigin(_text.getLocalBounds().width, 0);
	_text.setPosition(center.x + 450, center.y + 79);
	_text.setCharacterSize(16);
	window.draw(_text);

	// ally actual hp
	_text.setString(std::to_string(_encounterSavageData.player[actualPkm].actualHp));
	_text.setOrigin(_text.getLocalBounds().width, 0);
	_text.setPosition(center.x + 360, center.y + 79);
	_text.setCharacterSize(16);
	window.draw(_text);
}

void SceneCombat::displayRect(sf::RenderWindow &window, const sf::Vector2f &center)
{
	// Hp rect player
	_rect.setFillColor(_color[0]);
	_rect.setTexture(nullptr, true);
	_rect.setOrigin(0, 0);
	_rect.setScale(1, 1);
	float percentage = static_cast<float>(_encounterSavageData.player[actualPkm].actualHp) /
					   static_cast<float>(_encounterSavageData.player[actualPkm].getMaxHp());
	_rect.setSize(sf::Vector2f(200 * percentage, 7));
	_rect.setPosition(center.x + 258, center.y + 67);
	window.draw(_rect);

	// hp rect enemy
	_rect.setFillColor(_color[1]);
	percentage = static_cast<float>(_encounterSavageData.enemy.actualHp) /
				 static_cast<float>(_encounterSavageData.enemy.getMaxHp());
	_rect.setSize(sf::Vector2f(200 * percentage, 9));
	_rect.setPosition(center.x - 395, center.y - 194);
	window.draw(_rect);

	// Xp player
	_rect.setFillColor(sf::Color::Cyan);
	percentage = static_cast<float>(_encounterSavageData.player[actualPkm].experience) /
				 static_cast<float>(_encounterSavageData.player[actualPkm].getMaxXp());
	_rect.setSize(sf::Vector2f(265 * percentage, 5));
	_rect.setPosition(center.x + 193, center.y + 105);
	window.draw(_rect);
}

void SceneCombat::display(sf::RenderWindow &window)
{
	sf::Vector2f center = window.getView().getCenter();
	center.y = ceilf(center.y);
	center.x = ceilf(center.x);
	displayTextures(window, center);
	displayText(window, center);
	displayRect(window, center);

	// FADE
	_rect.setScale(1, 1);
	_rect.setFillColor(_fader.getColour());
	_rect.setTexture(nullptr, true);
	_rect.setOrigin(window.getView().getCenter());
	_rect.setSize(window.getView().getSize());
	_rect.setPosition(window.getView().getCenter());
	window.draw(_rect);
}

void SceneCombat::passData(const DataScene &data)
{
	if (data.type == DATA_SAVAGE_ENCOUNTER) {
		_encounterSavageData = *data.data.encounter;
		_encounterType = DATA_SAVAGE_ENCOUNTER;

		// temorary part
		_encounterSavageData.player[0] = PokemonsFromStorage.getPokemon(129);
		_encounterSavageData.player[0].level = 19;
		_encounterSavageData.player[0].actualHp = _encounterSavageData.player[0].getMaxHp();
		_encounterSavageData.player[0].experience = 2000;
		_encounterSavageData.enemy.actualHp = _encounterSavageData.enemy.getMaxHp() / 2 - 2;
	}
}
