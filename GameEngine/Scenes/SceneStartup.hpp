//
// Created by azkal on 04/03/2022.
//

#ifndef POKEMONTITANE_SCENESTARTUP_HPP
#define POKEMONTITANE_SCENESTARTUP_HPP

#include <chrono>
#include "IScene.hpp"
#include "Utils/Fader.hpp"

namespace Titane {
	class SceneStartup : public IScene {
	private:
		int _fadeStatus {0};

		sf::Clock _clock;

		sf::Shader _shader;

		sf::Sprite _sprite;

		sf::Text _text;

		sf::Font _font;

		sf::RectangleShape _fade;

		Fader _fader;

		size_t _eventId {0};

		std::latch *_latch {nullptr};

		bool _hasBeenPressed = false;

		void eventHandler(const bool arr[]) noexcept;

		void stateDraw(sf::RenderWindow &window) noexcept;

	public:
		explicit SceneStartup(TitaneEngine *engine, UserEvents &events);

		void activate() override;

		void deactivate() override;

		void tick(float elapsedTime) override;

		void display(sf::RenderWindow &window) override;

		void passData(const DataScene &data) override;
	};
}

#endif //POKEMONTITANE_SCENESTARTUP_HPP
