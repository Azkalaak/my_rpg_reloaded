//
// Created by azkal on 04/03/2022.
//

#include "SceneStartup.hpp"
#include "Logs/Logger.hpp"
#include "TitaneEngine.hpp"

using namespace Titane;

SceneStartup::SceneStartup(TitaneEngine *engine, UserEvents &events) : IScene(engine, events)
{
	// will need to initialise the sprites and the menu
	TitaneTextureManager.loadTexture("GameData/textures/splashscreen/splashscreen.png");
	TitaneTextureManager.loadTexture("GameData/textures/splashscreen/titleFront.png");
	TitaneTextureManager.loadTexture("GameData/textures/splashscreen/titleBack.png");
	auto &texture = TitaneTextureManager.getTexture("GameData/textures/splashscreen/splashscreen.png");
	_sprite.setTexture(texture);
	auto size = texture.getSize();
	_sprite.setOrigin(static_cast<float>(size.x) / 2, static_cast<float>(size.y) / 2);
	auto tmp = sf::Color::Black;
	_fader.setColour(tmp);
	_shader.loadFromFile("GameData/shaders/mainMenu2.frag", sf::Shader::Fragment);
	_shader.setUniform("iResolution", sf::Vector2f(960, 640));
	_font.loadFromFile("GameData/Fonts/font.ttf");
	_text.setFont(_font);
	_text.setCharacterSize(30);
	_text.setString("Appuyer sur\n\n  Entrer");
	_text.setPosition(50, 400); // text is the same as old one
	_text.setStyle(sf::Text::Style::Bold);
	_text.setOutlineThickness(1);
	_text.setFillColor(sf::Color::Black);
	_clock.restart();
}

void SceneStartup::activate()
{
	Titane::TitaneLogger.reportInfo("Splashscreen scene");
	auto tmp = sf::Color::Black;
	_fader.setColour(tmp);
	tmp.a = 0;
	_text.setFillColor(tmp);
	_text.setOutlineColor(tmp);
	_fadeStatus = 0;
	_sprite.setTexture(TitaneTextureManager.getTexture("GameData/textures/splashscreen/splashscreen.png"));
	_clock.restart();
}

void SceneStartup::deactivate()
{
	_event.callback -= _eventId;
	_eventId = 0;
}

void SceneStartup::eventHandler(const bool arr[]) noexcept
{
	static bool isPressed {false};

	if (arr[Enter] || arr[Right])
		isPressed = true;
	if (!arr[Enter] && !arr[Right] && isPressed) {
		isPressed = false;
		_hasBeenPressed = true;
	}
}

void SceneStartup::tick(float)
{
	switch (_fadeStatus) {
		case 0:
			if (_fader.fadeOut(2))
				_fadeStatus = 1;
			break;
		case 1:
			if (_fader.fadeIn(2)) {
				if (_latch != nullptr && !_latch->try_wait()) {
					_fadeStatus = 0;
					break;
				}
				_latch = nullptr;
				_fadeStatus = 2;
				_clock.restart();
				_eventId = _event.callback += [this](const bool events[]){ eventHandler(events); };
			}
			break;
		case 2:
			if (_fader.fadeOut(2)) {
				_fadeStatus = 3;
				auto tmp = sf::Color::Red;
				tmp.a = 0;
				_fader.setColour(tmp);

			}
			if (_hasBeenPressed) {
				_hasBeenPressed = false;
				_fadeStatus = 5;
			}
			break;
		case 3:
			if (_fader.fadeIn(3))
				_fadeStatus = 4;
			if (_hasBeenPressed) {
				_hasBeenPressed = false;
				_fadeStatus = 5;
			}
			break;
		case 4:
			if (_fader.fadeOut(3))
				_fadeStatus = 3;
			if (_hasBeenPressed) {
				_hasBeenPressed = false;
				_fadeStatus = 5;
			}
			break;
		default:
			TitaneLogger.reportInfo("Splashscreen end!");
			_engine->changeScene(MAIN);
			break;
	}
}

void SceneStartup::stateDraw(sf::RenderWindow &window) noexcept
{
	switch (_fadeStatus) {
		case 2:
			_fade.setSize(window.getView().getSize());
			_fade.setOrigin(window.getView().getCenter());
			_fade.setPosition(window.getView().getCenter());
			_fade.setFillColor(_fader.getColour());
			[[fallthrough]];
		case 3:
		case 4:
			// bottom
			_sprite.setTexture(TitaneTextureManager.getTexture("GameData/textures/splashscreen/titleBack.png"));
			_sprite.setPosition(window.getView().getCenter());
			window.draw(_sprite);

			// Shadeeeeeer
			_shader.setUniform("iTime", _clock.getElapsedTime().asSeconds());
			window.draw(_sprite, &_shader);

			_text.setFillColor(_fader.getColour());
			//Top
			_sprite.setTexture(TitaneTextureManager.getTexture("GameData/textures/splashscreen/titleFront.png"));
			_text.setPosition(window.getView().getCenter().x - 430, window.getView().getCenter().y + 80 );
			break;
		default:
			_fade.setSize(window.getView().getSize());
			_fade.setOrigin(window.getView().getCenter());
			_fade.setPosition(window.getView().getCenter());
			_fade.setFillColor(_fader.getColour());
			_text.setPosition(window.getView().getCenter().x - 430, window.getView().getCenter().y + 80 );
			break;
	}
	_sprite.setPosition(window.getView().getCenter());
}

void SceneStartup::display(sf::RenderWindow &window)
{
	stateDraw(window);
	window.draw(_sprite);
	window.draw(_text);
	window.draw(_fade);
}

void SceneStartup::passData(const DataScene &data)
{
	if (data.type != DATA_LATCH)
		return;
	_latch = data.data.latch->_latch;
}
