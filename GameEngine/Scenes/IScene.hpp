//
// Created by azkal on 03/03/2022.
//

#ifndef POKEMONTITANE_ISCENE_HPP
#define POKEMONTITANE_ISCENE_HPP

#include "Events/UserEvents.hpp"
#include "DataClass/UDataScene.hpp"

namespace Titane {
	class TitaneEngine;
	class IScene {
	protected:
// is stored as a pointer, however in functions can be used as reference
		TitaneEngine *_engine {nullptr};

		Titane::UserEvents &_event;

	public:
		IScene(TitaneEngine *engine, Titane::UserEvents &event);

		virtual ~IScene();

		virtual void activate() = 0;

		virtual void deactivate() = 0;

		virtual void tick(float elapsedTime) = 0;

		virtual void display(sf::RenderWindow &window) = 0;

		virtual void passData(const DataScene &data);

		void updateEvent(sf::RenderWindow &window);

		enum Scenes { STARTUP, MAIN, FIGHT, END_SCENES };
	};
}

#endif //POKEMONTITANE_ISCENE_HPP
