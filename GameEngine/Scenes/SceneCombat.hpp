//
// Created by azkal on 05/03/2022.
//

#ifndef POKEMONTITANE_SCENECOMBAT_HPP
#define POKEMONTITANE_SCENECOMBAT_HPP

#include "Utils/Fader.hpp"
#include "IScene.hpp"

namespace Titane {
	class SceneCombat : public IScene {
	private:
		DataSavageEncounter _encounterSavageData;

		DataSceneEnum _encounterType {DATA_EMPTY};

		Fader _fader;

		int _state {0};

		uint8_t actualPkm = 0;

		sf::Sprite _sprite;

		sf::RectangleShape _rect;

		size_t eventId {0};

		bool _evtButtonLast[5] {};

		sf::Font _font;

		sf::Text _text;

		// 0 is always player
		// 1 is always enemy;
		sf::Color _color[2] {sf::Color::Green, sf::Color::Green};

		void displayTextures(sf::RenderWindow &window, const sf::Vector2f &center);

		void displayText(sf::RenderWindow &window, const sf::Vector2f &center);

		void displayRect(sf::RenderWindow &window, const sf::Vector2f &center);

	public:
		SceneCombat(TitaneEngine *engine, UserEvents &events);

		~SceneCombat() noexcept override;

		void activate() override;

		void deactivate() override;

		void tick(float elapsedTime) override;

		void display(sf::RenderWindow &window) override;

		void passData(const DataScene &data) override;
	};
}

#endif //POKEMONTITANE_SCENECOMBAT_HPP
