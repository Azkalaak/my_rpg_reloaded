//
// Created by azkal on 05/04/2021.
//

#ifndef POKEMONTITANE_USEREVENTS_HPP
#define POKEMONTITANE_USEREVENTS_HPP

#include "Zenith/ZenithEvent.hpp"
#include "SFML/Graphics.hpp"
#include "Zenith/ZenithConditionalEvent.hpp"
#include "Zenith/ZenithVector.hpp"

namespace Titane {

	enum Events {
		A = 0,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		Num0,
		Num1,
		Num2,
		Num3,
		Num4,
		Num5,
		Num6,
		Num7,
		Num8,
		Num9,
		Escape,
		LControl,
		LShift,
		LAlt,
		LSystem,
		RControl,
		RShift,
		RAlt,
		RSystem,
		Menu,
		LBracket,
		RBracket,
		Semicolon,
		Comma,
		Period,
		Quote,
		Slash,
		Backslash,
		Tilde,
		Equal,
		Hyphen,
		Space,
		Enter,
		Backspace,
		Tab,
		PageUp,
		PageDown,
		End,
		Home,
		Insert,
		Delete,
		Add,
		Subtract,
		Multiply,
		Divide,
		Left,
		Right,
		Up,
		Down,
		Numpad0,
		Numpad1,
		Numpad2,
		Numpad3,
		Numpad4,
		Numpad5,
		Numpad6,
		Numpad7,
		Numpad8,
		Numpad9,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		F13,
		F14,
		F15,
		Pause,
		LastKeyboardEvent,
		LeftMouse,       ///< The left mouse button
		RightMouse,      ///< The right mouse button
		MiddleMouse,     ///< The middle (wheel) mouse button
		MouseButton1,   ///< The first extra mouse button
		MouseButton2,
		LastEvent
	}; // "rewriting" the SFML events into a custom enum so we don't fully depend on it

	class UserEvents {
	private:
		bool eventList[LastEvent] = {static_cast<Events>(0)};

	public:
		Zenith::ZenithEvent<void(const bool [LastEvent])> callback;

		Zenith::ZenithConditionalEvent<Zenith::TypeList<const bool[LastEvent]>, void(void)> conditionalCallback;

		void update(sf::RenderWindow &window);
	};
}

#endif //POKEMONTITANE_USEREVENTS_HPP
