//
// Created by azkal on 05/04/2021.
//

#include "UserEvents.hpp"
#include "SFML/Window.hpp"

void Titane::UserEvents::update(sf::RenderWindow &window)
{
	bool fire = false;
	sf::View view;

	sf::Event event{};
	while (window.pollEvent(event)) {
		switch (event.type) {
			case sf::Event::Closed:
				window.close(); // only event acting directly on the window because why not
				break;
			case sf::Event::KeyPressed:
				if (!eventList[event.key.code])
					fire = true;
				eventList[event.key.code] = true;
				break;
			case sf::Event::KeyReleased:
				fire = true;
				eventList[event.key.code] = false;
				break;
			case sf::Event::MouseWheelMoved:
			case sf::Event::MouseWheelScrolled:
				break;
			case sf::Event::MouseButtonPressed:
				if (!eventList[int(event.mouseButton.button) + LastKeyboardEvent + 1])
					fire = true;
				eventList[int(event.mouseButton.button) + LastKeyboardEvent + 1] = true;
				break;
			case sf::Event::MouseButtonReleased:
				fire = true;
				eventList[int(event.mouseButton.button) + LastKeyboardEvent + 1] = false;
				break;
			case sf::Event::Resized:
				if (event.size.height < 860 || event.size.width < 940) {
					if (event.size.height < 640)
						event.size.height = 640;
					if (event.size.width < 960)
						event.size.width = 960;
					window.setSize(sf::Vector2u(event.size.width, event.size.height));
				}
				window.setView(sf::View(sf::FloatRect(0, 0, static_cast<float>(event.size.width), static_cast<float>(event.size.height))));
				break;
			case sf::Event::MouseMoved: // we currently ignore those
			case sf::Event::JoystickButtonPressed:
			case sf::Event::JoystickButtonReleased:
			case sf::Event::JoystickMoved:

			case sf::Event::JoystickConnected: // we don't care
			case sf::Event::TextEntered:
			case sf::Event::LostFocus:
			case sf::Event::GainedFocus:
			case sf::Event::JoystickDisconnected:
			case sf::Event::TouchBegan:
			case sf::Event::TouchEnded:
			case sf::Event::TouchMoved:
			case sf::Event::SensorChanged:
			case sf::Event::MouseEntered:
			case sf::Event::MouseLeft:
			case sf::Event::Count:
				break;
		}
	}

	if (fire) {
		callback.fire(eventList);
		conditionalCallback.fireIf(eventList);
	}
}
