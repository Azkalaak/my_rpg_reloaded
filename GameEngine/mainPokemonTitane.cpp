//
// Created by azkal on 05/04/2021.
//
#include "TitaneEngine.hpp"

int main()
{
	Titane::TitaneEngine engine;

	engine.Initialise();
	engine.loop();

	return 0;
}
