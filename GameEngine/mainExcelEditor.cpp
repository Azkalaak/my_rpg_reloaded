//
// Created by azkal on 21/01/2022.
//

#include <array>
#include <filesystem>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Storage/StorageMap.hpp"
#include "Map/EditableMap.hpp"
#include "Logs/Logger.hpp"
#include "Zenith/ZenithUtils.hpp"
#include "Pokemons/EditablePokemons.hpp"
#include "Storage/StorageAttacks.hpp"
#include "Attacks/EditableAttacks.hpp"
#include "Pokemons/EditableEvolutions.hpp"
#include "Pokemons/EditableListPokemonsAttacks.hpp"
#include "Pokemons/EditableEncounters.hpp"

static void mapAddTp()
{
	const char *arr[Titane::MAP_END] = {
			"./GameData/Maps/MAP01",
			"./GameData/Maps/MAP02",
			"./GameData/Maps/MAP03",
			"./GameData/Maps/MAP04",
			"./GameData/Maps/MAP05",
			"./GameData/Maps/MAP06",
			"./GameData/Maps/MAP07",
			"./GameData/Maps/MAP08",
			"./GameData/Maps/MAP09",
			"./GameData/Maps/MAP10",
			"./GameData/Maps/MAP11",
			"./GameData/Maps/MAP12",
			"./GameData/Maps/MAP13",
			"./GameData/Maps/MAP14",
			"./GameData/Maps/MAP15",
			"./GameData/Maps/MAP16",
			"./GameData/Maps/MAP17",
			"./GameData/Maps/MAP18",
			"./GameData/Maps/MAP19",
			"./GameData/Maps/MAP20",
			"./GameData/Maps/MAP21"
	};

	std::ifstream file("./GameData/excels/tpList.csv");
	if (!file.is_open())
		return;
	// ====================
	// OPEN AND MODIFY EACH  MAPS DEPENDING ON THE EXCEL FED IN
	// ====================
	std::string line;
	std::getline(file, line); // LINE WITH HEADER THINGS, WE IGNORE IT
	Titane::EditableMap map(arr[0]);
	int i = 0;

	while (!file.eof())
	{
		std::getline(file, line);
		if (line.empty())
			continue;

		auto vec = Zenith::Utils::split<int>(line, [](const std::string& x){return std::stoi(x);}, ";");

		std::string mapPath = arr[vec[0]];
		Zenith::ZenithVector2i tpFrom = {{vec[1], vec[2]}};
		Zenith::ZenithVector2i tpTo = {{vec[3], vec[4]}};
		auto mapTo = static_cast<Titane::Maps>(vec[5]);

		map.changeMap(mapPath.c_str());

		map.addTP(tpFrom, tpTo, mapTo);
		map.saveMap();
		++i;
	}
	Titane::TitaneLogger.reportRaw("  -- ", i, " TP Points added to maps");
	file.close();
}

static Titane::StoragePokemon::Types typeNameToId(const std::string &str)
{
	int j = 0;
	for (auto &i : Titane::StoragePokemon::TypesNames) {
		if (i == str)
			return static_cast<Titane::StoragePokemon::Types>(j);
	}
	Titane::TitaneLogger.reportRaw("  -- ", "TYPE NOT RECOGNIZED: ", str);
	return Titane::StoragePokemon::AUCUN;
};

static void pokemonBinarize()
{
	std::ifstream file("./GameData/excels/pokemonsList.csv");
	if (!file.is_open())
		return;
	std::string line;
	std::getline(file, line);
	int i = 0;

	Titane::EditablePokemons pkm;

	while (!file.eof())
	{
		std::getline(file, line);
		if (line.empty())
			continue;
		auto vec = Zenith::Utils::split<std::string>(line, [](const std::string &x) {return x;}, ";");

		if (vec.size() < 9)
			continue;
		if (vec.size() == 9)
			vec.emplace_back("AUCUN");
		pkm.addPokemon(
				vec[1],
				std::stoi(vec[0]),
				std::stoi(vec[2]),
				std::stoi(vec[3]),
				std::stoi(vec[4]),
				std::stoi(vec[5]),
				std::stoi(vec[6]),
				std::stoi(vec[7]),
				typeNameToId(vec[8]),
				typeNameToId(vec[9])
				);
		++i;
	}

	pkm.save();
	pkm.verify();
	file.close();
	Titane::TitaneLogger.reportRaw("  -- ", i, " Pokemon stored!");
}

static int EvolutionStrToCondition(const std::string &str)
{
	for (int i = 0; i < Titane::END_EVOLUTION_CONDITION; ++i)
		if (str == Titane::StorageEvolutions::ConditionName[i])
			return i;
	Titane::TitaneLogger.reportRaw("  -- ", "Unknown condition: ", str);
	return Titane::END_EVOLUTION_CONDITION;
}

static void pokemonEvolutionBinarize()
{
	std::ifstream file("./GameData/excels/PokemonEvolutionList.csv");
	if (!file.is_open())
		return;
	std::string line;
	std::getline(file, line);
	int i = 0;

	Titane::EditableEvolutions evolutions;

	while (!file.eof()) {
		std::getline(file, line);
		if (line.empty())
			continue;
		auto vec = Zenith::Utils::split<std::string>(line, [](const std::string &x) {return x;}, ";");

		if (vec.size() < 3)
			continue;
		if (vec.size() == 3)
			evolutions.addEvolution(
						std::stoi(vec[0]),
						std::stoi(vec[1]),
						Titane::ECHANGE
					);
		else if (vec.size() == 4){
			if (vec[2] == "LVL")
				evolutions.addEvolution(
						std::stoi(vec[0]),
						std::stoi(vec[1]),
						std::stoi(vec[3])
				);
			else
				evolutions.addEvolution(
						std::stoi(vec[0]),
						std::stoi(vec[1]),
						EvolutionStrToCondition(vec[3])
				);
		}
		++i;
	}
	evolutions.save();
	evolutions.verify();
	file.close();
	Titane::TitaneLogger.reportRaw("  -- ", i, " Evolutions stored!");
}

static int categoryToBinary(const std::string &str)
{
	if (str == "Physique")
		return Titane::AttackEffectStruct::PHYSICAL;
	else if (str == "Speciale")
		return Titane::AttackEffectStruct::SPECIAL;
	else
		return Titane::AttackEffectStruct::NONE;
}

static int attackEffectToBinary(const std::string &str)
{
	for (int i = 0; i < Titane::AttackEffectStruct::END_ATTACK_EFFECTS; ++i)
	{
		if (Titane::StorageAttacks::AttackEffectsName[i] && Titane::StorageAttacks::AttackEffectsName[i] == str)
			return i;
	}
	return -1;
}

static void attackBinarize()
{
	std::ifstream file("./GameData/excels/Attacks.csv");
	if (!file.is_open())
		return;
	std::string line;
	std::getline(file, line);
	int i = 0;
	Titane::EditableAttacks attacks;


	while (!file.eof()) {
		std::getline(file, line);
		if (line.empty())
			continue;
		auto vec = Zenith::Utils::split<std::string>(line, [](const std::string &str) {return str;}, ";");

		if (vec.size() < 8)
			continue;
		if (attackEffectToBinary(vec[7]) == -1 && (vec.size() != 9 || attackEffectToBinary(vec[8]) == -1)) {
			continue;
		}

		attacks.addAttack(
				vec[1],
				std::stoi(vec[0]),
				typeNameToId(vec[2]),
				categoryToBinary(vec[3]),
				vec[4] == "NO" ? 0 : std::stoi(vec[4]),
				vec[5] == "NO" ? 0 : std::stoi(vec[5]),
				std::stoi(vec[6]),
				attackEffectToBinary(vec[7]),
				vec.size() != 9 ? 255 : attackEffectToBinary(vec[8])
				);
		++i;
	}
	attacks.save();
	attacks.verify();
	file.close();
	Titane::TitaneLogger.reportRaw("  -- ", i, " Attack stored!");
}

static void linkPokemonsAttacksBinarize()
{
	std::ifstream file("./GameData/excels/AttackPokemons.csv");
	if (!file.is_open())
		return;
	std::string line;
	std::getline(file, line);
	Titane::EditableAttacks attacks;
	int goodCounter = 0;
	int badCounter = 0;

	Titane::EditableListPokemonsAttacks editable;

	attacks.load();

	while (!file.eof()) {
		std::getline(file, line);
		if (line.empty())
			continue;
		auto vec = Zenith::Utils::split<std::string>(line, [](const std::string &x) { return x; }, ";");

		if (vec.size() != 4 || !attacks.doesAttackExist(vec[1])) {
			++badCounter;
			continue;
		}
		if (vec[2] == "LVL")
			editable.addAttack(
					std::stoi(vec[0]),
					attacks.getAttackId(vec[1]),
					std::stoi(vec[3])
					);
		++goodCounter;
	}
	editable.save();
	editable.verify();
	file.close();
	Titane::TitaneLogger.reportRaw("  -- ", goodCounter, " Attacks were linked to pokemons");
	Titane::TitaneLogger.reportRaw("  -- ", badCounter, " Attacks could not be linked. (either invalid attack or incomplete excel file)");
}

static void encounterBinarize()
{
	std::ifstream file("./GameData/excels/Encounter.csv");
	if (!file.is_open())
		return;
	std::string line;
	std::getline(file, line);
	int i = 0;
	Titane::EditableEncounters encounters;


	while (!file.eof()) {
		std::getline(file, line);
		if (line.empty())
			continue;
		auto vec = Zenith::Utils::split<std::string>(line, [](const std::string &str) { return str; }, ";");

		encounters.addEncounter(
				std::stoi(vec[0]),
				std::stoi(vec[1]),
				std::stoi(vec[2]),
				std::stoi(vec[3])
				);
		++i;
	}
	encounters.save();
	encounters.verify();
	file.close();
	Titane::TitaneLogger.reportRaw("  -- ", i, " encounters has been stored");
}

static void deleteExcelFolder()
{
	std::filesystem::remove_all("./GameData/excels");
	Titane::TitaneLogger.reportRaw("  -- ", "Removed excel directory");
}

int main()
{
	mapAddTp();
	pokemonBinarize();
	pokemonEvolutionBinarize();
	attackBinarize();
	linkPokemonsAttacksBinarize();
	encounterBinarize();

	// ===============
	// NEED TO DELETE EXCEL FOLDER AFTER RUN
	// ===============
	deleteExcelFolder();

}
