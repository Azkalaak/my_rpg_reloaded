//
// Created by azkal on 09/12/2021.
//

#ifndef POKEMONTITANE_LOGGER_HPP
#define POKEMONTITANE_LOGGER_HPP


#include <string>
#include <sstream>
#include <iostream>
#include <chrono>
#include "Zenith/ZenithGlobalSingleton.hpp"

namespace Titane {

	enum VerboseLevel {
		INFO = 3, WARNING = 2, ERROR = 1, NO_VERBOSE = 0
	};

	enum DetailLevel {
		LINE = 3, FUNCTION = 2, FILE = 1, NO_DETAIL = 0
	};

	class Logger {
	public:
		virtual ~Logger();
	protected:
		Logger();

	private:

		char verboseLevel = INFO;

#ifdef DEBUG
		char detailLevel = LINE;
		static constexpr bool doesLog = true;
#else
		char detailLevel = NO_DETAIL;
		static constexpr bool doesLog = false;

#endif

		template<typename ...Args>
		static void sendMessage(std::ostream &stream, const Args&... args)
		{
			(stream << ... << args);
			stream << '\n';
		}


	public:

		Logger(const Logger &) = delete;

		Logger(Logger &&) = delete;

		Logger &operator=(const Logger &) = delete;

		Logger &operator=(Logger &&) = delete;

		template<typename ...Args>
		void reportInfo(const char *file, const char *function, int line, Args...args) const
		{
			if constexpr(!doesLog)
				return;
			std::string frontInfo;
			std::stringstream outMessage;

			if (detailLevel == NO_DETAIL)
				return;
#ifndef NO_FULL_CXX_20
			frontInfo =  std::format("<INFO> <{:%d/%m/%Y %H:%M:%OS}>:",
											   std::chrono::utc_clock::now());
#else
			frontInfo = "<INFO>  ";
#endif
			if (detailLevel >= FILE) {
				outMessage << "\n\t " << file;
			}
			if (detailLevel >= FUNCTION) {
				outMessage << "\n\t(" << function << ")";
			}
			if (detailLevel >= LINE) {
				outMessage << " [l:" << line << "]";
			}

			if (verboseLevel == INFO) {
				sendMessage(std::cout, frontInfo, args..., outMessage.rdbuf());
			}
		}

		template<typename ...Args>
		void reportWarn(const char *file, const char *function, int line, Args...args) const
		{
			if constexpr(!doesLog)
				return;
			std::string frontInfo;
			std::stringstream outMessage;

			if (detailLevel == NO_DETAIL)
				return;
#ifndef NO_FULL_CXX_20
			frontInfo = std::format("<WARN> <{:%d/%m/%Y %H:%M:%OS}>:",
											   std::chrono::utc_clock::now());
#else
			frontInfo = "<WARN>  ";
#endif


			if (detailLevel >= FILE) {
				outMessage << "\n\t " << file;
			}
			if (detailLevel >= FUNCTION) {
				outMessage << "\n\t(" << function << ")";
			}
			if (detailLevel >= LINE) {
				outMessage << " [l:" << line << "]";
			}

			if (verboseLevel >= WARNING) {
				sendMessage(std::cerr, frontInfo, args..., outMessage.rdbuf());
			}
		}

		template<typename ...Args>
		void reportErr(const char *file, const char *function, int line, Args...args) const
		{
			if constexpr(!doesLog)
				return;
			std::string frontInfo;
			std::stringstream outMessage;

			if (detailLevel == NO_DETAIL)
				return;
#ifndef NO_FULL_CXX_20
			frontInfo = std::format("<ERR>  <{:%d/%m/%Y %H:%M:%OS}>:",
											   std::chrono::utc_clock::now());
#else
			frontInfo = "<ERR>   ";
#endif

			if (detailLevel >= FILE) {
				outMessage << "\n\t " << file;
			}
			if (detailLevel >= FUNCTION) {
				outMessage << "\n\t(" << function << ")";
			}
			if (detailLevel >= LINE) {
				outMessage << " [l:" << line << "]";
			}

			if (verboseLevel >= ERROR) {
				sendMessage(std::cerr, frontInfo, args..., outMessage.rdbuf());
			}
		}

		template<typename ...Args>
		void reportRaw(Args...args) const
		{
			if constexpr(!doesLog)
				return;
			if (detailLevel == NO_DETAIL)
				return;
			if (verboseLevel >= INFO)
				sendMessage(std::cout, args...);
		}

		void changeVerbose(VerboseLevel level) noexcept;

#undef reportInfo
#undef reportWarn
#undef reportErr
#define reportInfo(...) reportInfo(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
#define reportWarn(...) reportWarn(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
#define reportErr(...)  reportErr (__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

	};

	using TitaneLogger = Zenith::ZenithGlobalSingleton<Logger>;
}

#define TitaneLogger TitaneLogger::instance()

#endif //POKEMONTITANE_LOGGER_HPP
