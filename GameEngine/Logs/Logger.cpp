//
// Created by azkal on 09/12/2021.
//

#include <chrono>

#ifndef NO_FULL_CXX_20
#include <format>
#endif

#include "Logger.hpp"

using namespace Titane;

void Logger::changeVerbose(VerboseLevel level) noexcept
{
	verboseLevel = level;
}

Logger::~Logger() = default;

Logger::Logger() = default;
