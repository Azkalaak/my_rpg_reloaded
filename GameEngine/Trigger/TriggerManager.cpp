//
// Created by azkal on 25/01/2022.
//

#include "TriggerManager.hpp"
#include "TitaneEngine.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

void TriggerManager::teleportation() noexcept
{
	int x = (_player.position[1]) / 32;
	int y = (_player.position[0] + 10) / 32;

	switch (_player.direction) {
		case EntityMovement::UP:
			--y;
			break;
		case EntityMovement::DOWN:
			++y;
			break;
		case EntityMovement::RIGHT:
			++x;
			break;
		case EntityMovement::LEFT:
			--x;
			break;
		case EntityMovement::NONE:
			return;
	}

	auto &tmp = _mapManager.getMap(_mapManager.getMap()).isTp({{x, y}});

	if (tmp.mapKey == MAP_END)
		return;

	_player.position = {{tmp.TPToY * 32 - 10, tmp.TPToX * 32}};
	_mapManager.setMap(tmp.mapKey);
}

void TriggerManager::savagePokemon(TitaneEngine *engine) noexcept
{
	if (_player.idle || _player.position[1] % 32 || (_player.position[0] + 10) % 32)
		return;

	int x = (_player.position[1]) / 32;
	int y = (_player.position[0] + 10) / 32;

	auto actualMapId = _mapManager.getMap();

	if(_mapManager.getMap(actualMapId).isGrass({{x, y}}) == 0 &&
	actualMapId != ROUTE_VICTOIRE && actualMapId != ROUTE6)
		return;

	unsigned int rndNumber = _random() % 280;

	if (rndNumber >= 13)
		return;

	auto &encounter = EncountersFromStorage.getEncounter(actualMapId);

	Pokemon pkm(PokemonsFromStorage.getPokemon(encounter.pokemonId));

	pkm.level = static_cast<char>(_random() % (encounter.maxLvl - encounter.minLvl) + encounter.minLvl);

	Titane::TitaneLogger.reportInfo("Pokemon encountered!", " It is a ", pkm.name, " with a level of ", static_cast<int>(pkm.level));
	engine->changeScene(IScene::FIGHT);
	// need to pass the savage pokemon and the player pokemons
	DataSavageEncounter tmpEncounter;
	tmpEncounter.enemy = pkm;
	DataScene data {.type = DATA_SAVAGE_ENCOUNTER};
	data.data.encounter = &tmpEncounter;
	engine->passDataToCurScene(data);
}

void TriggerManager::tick(TitaneEngine *engine)
{
	if (_player.idle || _player.position[1] % 32 || (_player.position[0] + 10) % 32)
		return;

	teleportation();
	savagePokemon(engine);
}
