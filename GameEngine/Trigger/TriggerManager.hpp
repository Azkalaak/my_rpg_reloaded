//
// Created by azkal on 25/01/2022.
//

#ifndef POKEMONTITANE_TRIGGERMANAGER_HPP
#define POKEMONTITANE_TRIGGERMANAGER_HPP

#include <random>
#include "DataClass/EntityMovement.hpp"
#include "Map/MapManager.hpp"
#include "Pokemons/Encounters.hpp"
#include "Pokemons/Pokemons.hpp"

namespace Titane {
	class TitaneEngine;
	class TriggerManager {
	private:
		EntityMovement &_player;

		MapManager &_mapManager;

		std::random_device _random;

		void teleportation() noexcept;

		void savagePokemon(TitaneEngine *engine) noexcept;

	public:
		explicit TriggerManager(MapManager &mapManager, EntityMovement &player) : _player(player), _mapManager(mapManager) {}

		void tick(TitaneEngine *engine);
	};
}

#endif //POKEMONTITANE_TRIGGERMANAGER_HPP
