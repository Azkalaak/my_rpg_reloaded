//
// Created by azkal on 20/01/2022.
//

#ifndef POKEMONTITANE_TITANEENGINE_HPP
#define POKEMONTITANE_TITANEENGINE_HPP

#include <latch>
#include <thread>

#include "Storage/StorageToTextures.hpp"
#include "Events/UserEvents.hpp"
#include "DataClass/EntityMovement.hpp"
#include "Renderer/RendererManager.hpp"
#include "Controller/CameraController.hpp"
#include "Controller/ControllerManager.hpp"
#include "Collisions/CollisionManager.hpp"
#include "Trigger/TriggerManager.hpp"
#include "Entity.hpp"
#include "Camera.hpp"
#include "Scenes/SceneMain.hpp"

namespace Titane {
	class TitaneEngine {
	private:

		// ========================
		// SFML CLASS
		// ========================
		sf::RenderWindow _window;

		// ========================
		// TITANE CLASS
		// ========================

		Titane::StorageToTextures _textures;
		Titane::UserEvents _events;

		IScene::Scenes actualScene {IScene::END_SCENES};
		std::vector<std::unique_ptr<Titane::IScene>> _scenes;

		std::latch _latch {IScene::END_SCENES - 1};

	public:

		TitaneEngine();

		bool Initialise();

		// loop until video closes
		void loop();

		// if using external loop (for debug or testing purpose)
		// elapsed time here is in seconds
		void tick(float elapsedTime);

		// if using external loop (for debug or testing purpose)
		void manageEvent();

		// if using external loop (for debug or testing purpose)
		void display();

		void changeScene(IScene::Scenes scene);

		void passDataToCurScene(const DataScene &);
	};
}

#endif //POKEMONTITANE_TITANEENGINE_HPP
