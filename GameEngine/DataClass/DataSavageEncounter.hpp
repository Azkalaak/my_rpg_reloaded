//
// Created by azkal on 05/03/2022.
//

#ifndef POKEMONTITANE_DATASAVAGEENCOUNTER_HPP
#define POKEMONTITANE_DATASAVAGEENCOUNTER_HPP

#include "Pokemon.hpp"

namespace Titane {
	struct DataSavageEncounter {
		Pokemon enemy {};
		Pokemon player[6] {};
	};
}

#endif //POKEMONTITANE_DATASAVAGEENCOUNTER_HPP
