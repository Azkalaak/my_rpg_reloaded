//
// Created by azkalaak on 13/03/2022.
//

#ifndef POKEMONTITANE_DATALATCH_HPP
#define POKEMONTITANE_DATALATCH_HPP
#include <latch>

namespace Titane {
	struct DataLatch {
		std::latch *_latch;
	};
}
#endif //POKEMONTITANE_DATALATCH_HPP
