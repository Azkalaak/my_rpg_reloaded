//
// Created by azkal on 27/01/2022.
//

#ifndef POKEMONTITANE_POKEMON_HPP
#define POKEMONTITANE_POKEMON_HPP

#include <string>
#include "Storage/StoragePokemon.hpp"
#include "Storage/StorageAttacks.hpp"

namespace Titane {
	class Pokemon {
	public:
		[[nodiscard]] int getMaxHp() const noexcept
		{
			return  (level * (2 * baseHp + iv[0] + ev[0] / 4)) / 100 + level + 10;
		}

		[[nodiscard]] int getMaxXp() const noexcept
		{
			// fast leveling as the game is too small for other formulas
			return (4 * level * level * level) / 5;
		}

		std::string name;
		int iv[6] {};
		int ev[6] {};
		int actualHp {0};
		int experience {0};
		AttackEffectStruct::attackEffect status {AttackEffectStruct::NO_EFFECT};

		int id {0};
		int baseHp {0};
		int attack {0};
		int defence {0};
		int specialAttack {0};
		int specialDefence {0};
		int speed {0};
		StoragePokemon::Types type1 {StoragePokemon::AUCUN};
		StoragePokemon::Types type2 {StoragePokemon::AUCUN};

// Useful only if the pokemon is part of a team / combat
		char level {1};

		Pokemon() = default;
		explicit Pokemon(const PokemonNode &nodePokemon)
		{
			name = reinterpret_cast<const char *>(nodePokemon.name);
			id = static_cast<int>(nodePokemon.id);
			baseHp = static_cast<int>(nodePokemon.hp);
			attack = static_cast<int>(nodePokemon.attack);
			defence = static_cast<int>(nodePokemon.defence);
			specialAttack = static_cast<int>(nodePokemon.specialAttack);
			specialDefence = static_cast<int>(nodePokemon.specialDefence);
			speed = static_cast<int>(nodePokemon.speed);
			type1 = static_cast<StoragePokemon::Types>(nodePokemon.type1);
			type2 = static_cast<StoragePokemon::Types>(nodePokemon.type2);
		}

		Pokemon(const std::string &_name, int _id, int _hp, int _attack, int _defence, int _specialAttack,
				 int _specialDefence, int _speed, StoragePokemon::Types _type1, StoragePokemon::Types _type2)
		{
			name = _name;
			id = _id;
			baseHp = _hp;
			attack = _attack;
			defence = _defence;
			specialAttack = _specialAttack;
			specialDefence = _specialDefence;
			speed = _speed;
			type1 = _type1;
			type2 = _type2;
		}

		bool operator==(const Pokemon &pkm) const noexcept
		{
			return id == pkm.id;
		}

		bool operator<(const Pokemon &pkm) const noexcept
		{
			return id < pkm.id;
		}
		bool operator>(const Pokemon &pkm) const noexcept
		{
			return id > pkm.id;
		}
	};
}

#endif //POKEMONTITANE_POKEMON_HPP
