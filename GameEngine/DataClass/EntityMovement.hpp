//
// Created by azkal on 19/01/2022.
//

#ifndef POKEMONTITANE_ENTITYMOVEMENT_HPP
#define POKEMONTITANE_ENTITYMOVEMENT_HPP

#include "Zenith/ZenithVector.hpp"

namespace Titane {

	class EntityMovement {
	public:
		enum Direction {
			UP, DOWN, LEFT, RIGHT, NONE
		};

		Zenith::ZenithVector2i position; // position of the top left part of player

		Zenith::ZenithVector2i speedVector; // vector of direction. used to move views and colliders

		int speed = 1; // Speed in pixels per move, used for collision purpose

		Direction direction = DOWN;

		bool idle = true;

		bool operator==(const EntityMovement &character2) const noexcept
		{
			return position == character2.position;
		}
	};


}


#endif //POKEMONTITANE_ENTITYMOVEMENT_HPP
