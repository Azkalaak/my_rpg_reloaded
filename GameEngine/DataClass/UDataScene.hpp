//
// Created by azkal on 05/03/2022.
//

#ifndef POKEMONTITANE_UDATASCENE_HPP
#define POKEMONTITANE_UDATASCENE_HPP

#include "DataSavageEncounter.hpp"
#include "DataLatch.hpp"

namespace Titane {
	enum DataSceneEnum {
		DATA_SAVAGE_ENCOUNTER,
		DATA_LATCH,
		DATA_EMPTY
	};

	union UDataScene {
		int i = 0;
		DataSavageEncounter *encounter;
		DataLatch *latch;
	};

	struct DataScene {
		DataSceneEnum type {DATA_EMPTY};
		UDataScene data {0};
	};
}

#endif //POKEMONTITANE_UDATASCENE_HPP
