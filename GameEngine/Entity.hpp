//
// Created by azkal on 08/02/2022.
//

#ifndef POKEMONTITANE_ENTITY_HPP
#define POKEMONTITANE_ENTITY_HPP

#include <array>
#include "DataClass/Pokemon.hpp"
#include "DataClass/EntityMovement.hpp"
#include "Controller/ControllerManager.hpp"
#include "Renderer/PlayerRenderer.hpp"
#include "Trigger/TriggerManager.hpp"
#include "Collisions/CollisionManager.hpp"

namespace Titane {
	class Entity {
	private:

		// id of 0 is the player
		int id = 0;

		// a pokemon with ID 0 is considered as non existant
		std::array<Pokemon, 6> _team;


		ControllerManager _controls;

		TriggerManager _triggers;

		PlayerRenderer _playerRenderer;

	public:

		EntityMovement transform;

		Entity(sf::RenderWindow &windows, UserEvents &events, MapManager &map) : _controls(transform, events), _triggers(map, transform), _playerRenderer(windows, transform)
		{
			static int newId = -1;
			++newId;
			id = newId;
		}

		void activate();

		void deactivate();

		void tick();

		void triggers(TitaneEngine *engine);

		void render() noexcept;
	};
}

#endif //POKEMONTITANE_ENTITY_HPP
