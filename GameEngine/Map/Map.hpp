//
// Created by azkal on 06/04/2021.
//

#ifndef POKEMONTITANE_MAP_HPP
#define POKEMONTITANE_MAP_HPP

#include <string>
#include "Zenith/ZenithVector.hpp"
#include "Storage/StorageMap.hpp"

namespace Titane {

	class Map {
	private:

		StorageMap map;

		const std::string path;
		// need to store the map in a binary form
		// need to store map collisions too, perhaps inside the same map?
		// need to store the TP, using vector2 maybe? (tp can tp you in a map at given coordinates)

	public:
		Map(const char *str);

		~Map() noexcept;

		template<typename fnc>
		void ForEach(const Zenith::ZenithVector2i &beginPoint, const Zenith::ZenithVector2i &endPoint,
					 const std::function<fnc> &func) const noexcept
		{
			for (int i = beginPoint[0] < 0 ? 0 : beginPoint[0]; i < map.header.mapHeight && i < endPoint[0]; ++i)
				for (int j = beginPoint[1] < 0 ? 0 : beginPoint[1]; j < map.infos[i].width && j < endPoint[1]; ++j)
					func(map.map[i][j], {{i, j}});
		}

		void dumpAsText() const;

		// CHeck position given on map
		[[nodiscard]] bool isCollision(Zenith::ZenithVector2i position) const noexcept;

		[[nodiscard]] const TPHolder &isTp(Zenith::ZenithVector2i position) const noexcept;

		[[nodiscard]] int isGrass(Zenith::ZenithVector2i position) const noexcept;

		void verify();

		[[nodiscard]] uint16_t height() const noexcept
		{ return map.header.mapHeight; }

		[[nodiscard]] uint16_t width() const noexcept
		{return map.infos[map.header.mapHeight - 1].width;}
	};



	/*
	 *  DATA STRUCTURE OF A MAP FILE:
	 *
	 * 	Header
	 *
	 * 	Line
	 *  Next line Size (read it as last node of first line)
	 *  ...
	 *
	 * Map TP
	 * Map PNJ
	 * Map Pokemons
	 *
	 *
	 *  CRC
	 *
	 * */
}

#endif //POKEMONTITANE_MAP_HPP
