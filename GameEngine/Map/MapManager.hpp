//
// Created by azkal on 06/04/2021.
//

#ifndef POKEMONTITANE_MAPMANAGER_HPP
#define POKEMONTITANE_MAPMANAGER_HPP

#include <array>
#include "Map.hpp"

namespace Titane {
	class MapManager {
	private:

		Maps actualMap = MAP_END;

		std::array<Titane::Map, 21> allMaps{
				"./GameData/Maps/MAP01",
				"./GameData/Maps/MAP02",
				"./GameData/Maps/MAP03",
				"./GameData/Maps/MAP04",
				"./GameData/Maps/MAP05",
				"./GameData/Maps/MAP06",
				"./GameData/Maps/MAP07",
				"./GameData/Maps/MAP08",
				"./GameData/Maps/MAP09",
				"./GameData/Maps/MAP10",
				"./GameData/Maps/MAP11",
				"./GameData/Maps/MAP12",
				"./GameData/Maps/MAP13",
				"./GameData/Maps/MAP14",
				"./GameData/Maps/MAP15",
				"./GameData/Maps/MAP16",
				"./GameData/Maps/MAP17",
				"./GameData/Maps/MAP18",
				"./GameData/Maps/MAP19",
				"./GameData/Maps/MAP20",
				"./GameData/Maps/MAP21"
		};
	public:

		MapManager();

		void setMap(Maps id) noexcept
		{ actualMap = id; }

		[[nodiscard]] Maps getMap() const noexcept
		{ return actualMap; }

		[[nodiscard]] const Map &getMap(Maps id) const noexcept
		{ return allMaps[id]; }

		void dumpMap() const noexcept
		{ allMaps[actualMap].dumpAsText(); }

		void draw(const Zenith::ZenithVector2i &topLeft, const Zenith::ZenithVector2i &bottomRight,
				  const std::function<void(const MapNode &, Zenith::ZenithVector2i)> &func, Maps mapId = MAP_END) const;
	};
}

#endif //POKEMONTITANE_MAPMANAGER_HPP
