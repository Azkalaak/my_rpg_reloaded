//
// Created by azkal on 21/01/2022.
//

#include "EditableMap.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

EditableMap::EditableMap(const char *str)
{
	if (!map.load(str))
		throw Zenith::ZenithException("Invalid file: ", str);

	for (int i = 0; i < map.header.TPNumber; ++i)
		_tpHolders.emplace_back(map.tp[i]);
	path = str;
}

EditableMap::~EditableMap() noexcept = default;

void EditableMap::changeMap(const char *str)
{
	path = "";
	map.unload();
	if (!map.load(str))
		throw Zenith::ZenithException("Invalid file: ", path);
	_tpHolders.clear();
	for (int i = 0; i < map.header.TPNumber; ++i)
		_tpHolders.emplace_back(map.tp[i]);
	path = str;
}

void EditableMap::addTP(const TPHolder &tpHolder)
{
	_tpHolders.emplace_back(tpHolder);
}

void EditableMap::addTP(const Zenith::ZenithVector2i &TPFrom, const Zenith::ZenithVector2i &TPTo, Maps mapTo)
{
	addTP({static_cast<uint16_t>(TPFrom[0]), static_cast<uint16_t>(TPFrom[1]), static_cast<uint16_t>(TPTo[0]), static_cast<uint16_t>(TPTo[1]), mapTo});
}

void EditableMap::removeTP(const TPHolder &tpHolder)
{
	_tpHolders.remove(tpHolder);
}

void EditableMap::removeTP(const Zenith::ZenithVector2i &TPFrom, Maps mapTo)
{
	removeTP({static_cast<uint16_t>(TPFrom[0]), static_cast<uint16_t>(TPFrom[1]), 0, 0, mapTo});
}

void EditableMap::saveMap()
{
	_tpHolders.sort();
	_tpHolders.unique();

	auto *tmpTPHolder = new TPHolder[_tpHolders.size()];
	int i = 0;

	for (auto &j : _tpHolders) {
		tmpTPHolder[i] = j;
		++i;
	}
	delete[] map.tp;
	map.tp = tmpTPHolder;
	map.header.TPNumber = static_cast<uint8_t>(_tpHolders.size());
	if (!map.write(path))
		throw Zenith::ZenithException("Could not write file to system >", path, "<");
}

void EditableMap::verify()
{
	if (map.calcCrc() != map.header.crc)
		throw Zenith::ZenithException("Corrupted map file: >", path, "<");
}
