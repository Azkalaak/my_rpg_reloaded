//
// Created by azkal on 06/04/2021.
//

#include "MapManager.hpp"
#include <iostream>

Titane::MapManager::MapManager()
{
	for (size_t i = 0; i < MAP_END; ++i)
		allMaps[i].verify();
}

void Titane::MapManager::draw(const Zenith::ZenithVector2i &topLeft, const Zenith::ZenithVector2i &bottomRight,
							  const std::function<void(const MapNode &, Zenith::ZenithVector2i)> &func, Maps mapId) const
{
	// [0] is the row
	// [1] is the column
	if (mapId == MAP_END)
		mapId = actualMap;
	allMaps[mapId].ForEach(topLeft, bottomRight, func);
}
