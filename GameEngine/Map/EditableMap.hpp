//
// Created by azkal on 21/01/2022.
//

#ifndef POKEMONTITANE_EDITABLEMAP_HPP
#define POKEMONTITANE_EDITABLEMAP_HPP

#include <list>
#include "Storage/StorageMap.hpp"

namespace Titane {
	class EditableMap {
	private:
		StorageMap map;

		std::string path;

		std::list<TPHolder> _tpHolders;

	public:
		explicit EditableMap(const char *str);

		~EditableMap() noexcept;

		void changeMap(const char *str);

		void addTP(const TPHolder &tpHolder);

		void addTP(const Zenith::ZenithVector2i &TPFrom, const Zenith::ZenithVector2i &TPTo, Maps mapTo);

		void removeTP(const TPHolder &tpHolder);

		void removeTP(const Zenith::ZenithVector2i &TPFrom, Maps mapTo);

		void saveMap();

		void verify();
	};
}

#endif //POKEMONTITANE_EDITABLEMAP_HPP
