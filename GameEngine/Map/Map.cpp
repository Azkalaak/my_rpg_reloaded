//
// Created by azkal on 06/04/2021.
//

#include <fstream>
#include <iostream>

#include "Map.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;


Map::Map(const char *str) : path(std::string(str))
{
	if (!map.load(path))
		throw Zenith::ZenithException("Invalid file: ", path);
}

Map::~Map() noexcept
{

}

void Map::dumpAsText() const
{
	for (int i = 0; i < map.header.mapHeight; ++i) {
		for (int j = 0; j < map.infos[i].width; ++j) {
			std::cout << static_cast<char>(map.map[i][j].bg + 32);
		}
		std::cout << "\n";
	}
	std::cout << std::endl;
}

void Map::verify()
{
	if (map.calcCrc() != map.header.crc)
		throw Zenith::ZenithException("Corrupted map file: >", path, "<");
}

bool Map::isCollision(Zenith::ZenithVector2i position) const noexcept
{
	return map.map[position[0]][position[1]].collision;
}

const TPHolder &Map::isTp(Zenith::ZenithVector2i position) const noexcept
{
	static TPHolder empty {0, 0, 0, 0, Maps::MAP_END};

	for (int i = 0; i < map.header.TPNumber; ++i) {
		if (map.tp[i].TPFromX == position[0] && map.tp[i].TPFromY == position[1])
			return map.tp[i];
	}
	return empty;
}

int Map::isGrass(Zenith::ZenithVector2i position) const noexcept
{
	return map.map[position[1]][position[0]].zoneID;
}
