//
// Created by azkal on 04/03/2022.
//

#ifndef POKEMONTITANE_FADER_HPP
#define POKEMONTITANE_FADER_HPP

#include <SFML/Graphics.hpp>

namespace Titane {
	class Fader {
	private:
		uint8_t _currAlpha = 0;

		sf::Color _colour;

	public:
		// set the alpha channel in the same time
		void setColour(const sf::Color &newColour) noexcept;

		// alpha goes up (no transparency)
		bool fadeIn(uint8_t increment = 1) noexcept;

		// alpha goes down (transparency)
		bool fadeOut(uint8_t increment = 1) noexcept;

		[[nodiscard]] const sf::Color &getColour() const noexcept;

	};
}

#endif //POKEMONTITANE_FADER_HPP
