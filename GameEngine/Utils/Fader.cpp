//
// Created by azkal on 04/03/2022.
//

#include "Fader.hpp"

using namespace Titane;

void Fader::setColour(const sf::Color &newColour) noexcept
{
	_colour = newColour;
	_currAlpha = _colour.a;
}

bool Fader::fadeIn(uint8_t increment) noexcept
{
	if (_currAlpha + increment > 255)
		_currAlpha = 255;
	else
		_currAlpha += increment;
	_colour.a = _currAlpha;
	if (_currAlpha == 255)
		return true;
	return false;
}

bool Fader::fadeOut(uint8_t increment) noexcept
{
	if (_currAlpha - increment < 0)
		_currAlpha = 0;
	else
		_currAlpha -= increment;
	_colour.a = _currAlpha;
	if (_currAlpha == 0)
		return true;
	return false;
}

const sf::Color &Fader::getColour() const noexcept
{
	return _colour;
}
