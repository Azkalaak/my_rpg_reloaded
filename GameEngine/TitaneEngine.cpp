//
// Created by azkal on 20/01/2022.
//

#include <iostream>
#include <chrono>

#include "TitaneEngine.hpp"
#include "Logs/Logger.hpp"
#include "Zenith/ZenithException.hpp"
#include "Scenes/SceneStartup.hpp"
#include "Scenes/SceneCombat.hpp"

using namespace Titane;

TitaneEngine::TitaneEngine() : _window(sf::VideoMode(960, 640), "Pokemon Titane")
{
	_scenes.emplace_back(std::make_unique<SceneStartup>(this, _events));
	std::thread sceneMainThread([this] {
		_scenes.emplace_back(std::make_unique<SceneMain>(this, _events, _window));
		_latch.count_down();
		_scenes.emplace_back(std::make_unique<SceneCombat>(this, _events));
		_latch.count_down();
	});

	sceneMainThread.detach();
	DataScene dataScene;
	DataLatch tmpLatch {._latch = &_latch};

	dataScene.type = DATA_LATCH;
	dataScene.data.latch = &tmpLatch;

	_scenes[IScene::STARTUP]->passData(dataScene);

	actualScene = IScene::STARTUP;
	_scenes[actualScene]->activate();

	_window.setVerticalSyncEnabled(false);
	_window.setFramerateLimit(90);
}

bool TitaneEngine::Initialise()
{
	try {
		if (_textures.verification())
			Titane::TitaneLogger.reportInfo("All map textures found!");
		else
			throw Zenith::ZenithException("Erreur: Textures manquantes");
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		return false;
	}

	return true;
}

void TitaneEngine::loop()
{
	long long elapsedTime = 0;
	constexpr uint32_t TPS = 80;
	constexpr float timeToWait = 1000.0f / static_cast<float>(TPS);

	float tmpTime = 0.0f;
	int tmpTpsCounter = 0;
	float tmpSecondTpsCounter = 0.0f;


	while (_window.isOpen()) {
		tmpTime += static_cast<float>(elapsedTime) / 1000;
		tmpSecondTpsCounter += static_cast<float>(elapsedTime) / 1000;
		auto begin = std::chrono::high_resolution_clock::now();

		if (tmpTime >= timeToWait) {
			tick((tmpTime - (tmpTime - timeToWait)) / 1000);
			tmpTime -= timeToWait;
			++tmpTpsCounter;
		}

		if (tmpSecondTpsCounter >= 1000) {
			if (tmpTpsCounter < 80)
				TitaneLogger.reportWarn("Tps is currently at: ", tmpTpsCounter, " Instead of 80.");
			tmpSecondTpsCounter -= 1000;
			tmpTpsCounter = 0;
		}

		display(); // this will go either inside a coroutine or a thread
		manageEvent(); // same here

		auto end = std::chrono::high_resolution_clock::now();
		elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
	}
}

void TitaneEngine::manageEvent()
{
	_events.update(_window);
}

void TitaneEngine::display()
{
	_window.clear(sf::Color::Black);
	_scenes[actualScene]->display(_window);
	_window.display();
}

void TitaneEngine::tick(float elapsedTime)
{
	_scenes[actualScene]->tick(elapsedTime);
}

void TitaneEngine::changeScene(IScene::Scenes scene)
{
	if (actualScene == IScene::END_SCENES)
		return;
	_scenes[actualScene]->deactivate();
	actualScene = scene;
	_scenes[actualScene]->activate();
	sf::View view(sf::FloatRect(0, 0, static_cast<float>(_window.getSize().x), static_cast<float>(_window.getSize().y)));
	_window.setView(view);
}

void TitaneEngine::passDataToCurScene(const DataScene &data)
{
	if (actualScene == IScene::END_SCENES)
		return;
	_scenes[actualScene]->passData(data);
}
