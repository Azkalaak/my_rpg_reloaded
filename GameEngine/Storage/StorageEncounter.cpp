//
// Created by azkalaak on 21/02/2022.
//

#include <fstream>
#include <filesystem>
#include "StorageEncounter.hpp"
#include "Zenith/ZenithCRC.hpp"

using namespace Titane;

bool StorageEncounter::load(const std::string &path) noexcept
{
	std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
	bool returnValue = file.is_open();

// we read the header
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(&header), sizeof(encounterHeader)).eof();

// Lecture of the number of attacks per pokemon
	if (returnValue) {
		returnValue = !file.read(reinterpret_cast<char *>(numberPkm), MAP_END * static_cast<uint16_t>(sizeof(uint8_t))).eof();
	}
// Lecture of all attacks

	if (returnValue) {
		for (int i = 0; i < MAP_END && returnValue; ++i) {
			if (numberPkm[i] == 0)
				continue;
			nodes[i] = new encounterNode[numberPkm[i]];
			returnValue = !file.read(reinterpret_cast<char *>(nodes[i]), numberPkm[i] * static_cast<uint16_t>(sizeof(encounterNode))).eof();
		}
	}

	file.close();
	return returnValue;
}

bool StorageEncounter::write(const std::string &path) const noexcept
{
	std::filesystem::create_directories("./GameData/PokemonData");
	std::ofstream file(path, std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open())
		return false;

	encounterHeader newHeader = header;
	newHeader.crc = calcCrc();

	file.write(reinterpret_cast<const char *>(&newHeader), sizeof newHeader);

	file.write(reinterpret_cast<const char *>(numberPkm), MAP_END * static_cast<uint16_t>(sizeof(uint8_t)));

	for (int i = 0; i < MAP_END; ++i)
		file.write(reinterpret_cast<const char *>(nodes[i]), static_cast<uint32_t>(sizeof(encounterNode)) * numberPkm[i]);

	file.close();
	return true;
}

void StorageEncounter::unload() noexcept
{
	int i = 0;

	header = {};

	for (auto & node : nodes) {
		numberPkm[i] = 0;
		delete[] node;
		node = nullptr;
		++i;
	}
}

StorageEncounter::~StorageEncounter() noexcept
{
	unload();
}

uint32_t StorageEncounter::calcCrc() const noexcept
{
	Zenith::ZenithCRC crcCalc;

	encounterHeader headerTmp = header;

// feeding the crc calculator with the header
	headerTmp.crc = 0;
	crcCalc.feed(&headerTmp, sizeof headerTmp);


	crcCalc.feed(numberPkm, MAP_END * sizeof(uint8_t));

	for (int i = 0; i < MAP_END; ++i)
		crcCalc.feed(nodes[i], numberPkm[i] * sizeof(encounterNode));

	return crcCalc.getCRC();
}
