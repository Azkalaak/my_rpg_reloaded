//
// Created by azkalaak on 19/02/2022.
//

#include <fstream>
#include <filesystem>
#include "StorageLinkPokemonsAttacks.hpp"
#include "Zenith/ZenithCRC.hpp"

using namespace Titane;

bool StorageLinkPokemonsAttacks::write(const std::string &path) const noexcept
{
	std::filesystem::create_directories("./GameData/PokemonData");
	std::ofstream file(path, std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open())
		return false;

	linkPokemonsAttacksHeader newHeader = header;
	newHeader.crc = calcCrc();

	file.write(reinterpret_cast<const char *>(&newHeader), sizeof newHeader);

	file.write(reinterpret_cast<const char *>(pokemonAttackNumber), newHeader.nodeNumber * static_cast<uint16_t>(sizeof(uint8_t)));

	for (size_t i = 0; i < newHeader.nodeNumber; ++i)
		file.write(reinterpret_cast<const char *>(nodes[i]), static_cast<uint32_t>(sizeof(linkPokemonsAttacksNode)) * pokemonAttackNumber[i]);

	file.close();
	return true;
}

bool StorageLinkPokemonsAttacks::load(const std::string &path) noexcept
{
	std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
	bool returnValue = file.is_open();

// we read the header
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(&header), sizeof(linkPokemonsAttacksHeader)).eof();

// Lecture of the number of attacks per pokemon
	if (returnValue) {
		pokemonAttackNumber = new uint8_t [header.nodeNumber];
		returnValue = !file.read(reinterpret_cast<char *>(pokemonAttackNumber), header.nodeNumber * static_cast<uint16_t>(sizeof(uint8_t))).eof();
	}
// Lecture of all attacks

	if (returnValue) {
		nodes = new linkPokemonsAttacksNode*[header.nodeNumber];
		for (size_t i = 0; i < header.nodeNumber && returnValue; ++i) {
			nodes[i] = new linkPokemonsAttacksNode[pokemonAttackNumber[i]];
			returnValue = !file.read(reinterpret_cast<char *>(nodes[i]), pokemonAttackNumber[i] * static_cast<uint16_t>(sizeof(linkPokemonsAttacksNode))).eof();
		}
	}

	file.close();
	return returnValue;
}

uint32_t StorageLinkPokemonsAttacks::calcCrc() const noexcept
{
	Zenith::ZenithCRC crcCalc;

	linkPokemonsAttacksHeader headerTmp = header;

// feeding the crc calculator with the header
	headerTmp.crc = 0;
	crcCalc.feed(&headerTmp, sizeof headerTmp);


	crcCalc.feed(pokemonAttackNumber, headerTmp.nodeNumber * sizeof(uint8_t));

	for (size_t i = 0; i < headerTmp.nodeNumber; ++i)
		crcCalc.feed(nodes[i], pokemonAttackNumber[i] * sizeof(linkPokemonsAttacksNode));

	return crcCalc.getCRC();
}

StorageLinkPokemonsAttacks::~StorageLinkPokemonsAttacks() noexcept
{
	unload();
}

void StorageLinkPokemonsAttacks::unload() noexcept
{
	for (size_t i = 0; i < header.nodeNumber; ++i)
		delete[] nodes[i];
	delete[] nodes;
	nodes = nullptr;
	delete[] pokemonAttackNumber;
	pokemonAttackNumber = nullptr;
	header = {};
}
