//
// Created by azkal on 27/01/2022.
//

#include <fstream>
#include <filesystem>
#include "StoragePokemon.hpp"
#include "Zenith/ZenithCRC.hpp"

using namespace Titane;

bool StoragePokemon::load(const std::string &path) noexcept
{
	std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
	bool returnValue = file.is_open();

// we read the header
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(&header), sizeof(PokemonHeader)).eof();

// Lecture of all pokemons at once
	if (returnValue) {
		pokemons = new PokemonNode[header.pokemonNumber];
		returnValue = !file.read(reinterpret_cast<char *>(pokemons), header.pokemonNumber * static_cast<uint16_t>(sizeof(PokemonNode))).eof();
	}
// End of lecture
	file.close();
	return returnValue;
}

bool StoragePokemon::write(const std::string &path) const noexcept
{
	std::filesystem::create_directories("./GameData/PokemonData");
	std::ofstream file(path, std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open())
		return false;

	PokemonHeader newHeader = header;
	newHeader.crc = calcCrc();

	file.write(reinterpret_cast<const char *>(&newHeader), sizeof newHeader);
	file.write(reinterpret_cast<const char *>(pokemons), static_cast<uint32_t>(sizeof(PokemonNode)) * header.pokemonNumber);
	file.close();
	return true;
}

uint32_t StoragePokemon::calcCrc() const noexcept
{
	Zenith::ZenithCRC crcCalc;

	PokemonHeader headerTmp = header;

// feeding the crc calculator with the header
	headerTmp.crc = 0;
	crcCalc.feed(&headerTmp, sizeof headerTmp);

// Then feeding with pokemon nodes
	crcCalc.feed(pokemons, sizeof(PokemonNode) * headerTmp.pokemonNumber);

// we return the calculated CRC
	return crcCalc.getCRC();
}

void StoragePokemon::unload() noexcept
{
	delete[] pokemons;
	pokemons = nullptr;
}

StoragePokemon::~StoragePokemon() noexcept
{
	delete[] pokemons;
}
