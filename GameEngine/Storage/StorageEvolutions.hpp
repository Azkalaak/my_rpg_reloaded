//
// Created by azkal on 07/02/2022.
//

#ifndef POKEMONTITANE_STORAGEEVOLUTIONS_HPP
#define POKEMONTITANE_STORAGEEVOLUTIONS_HPP

#include <cstdint>
#include <string>

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif

namespace Titane {

	enum EvolutionCondition {
		PIERRE_FOUDRE	= 101,
		PIERRE_LUNE		= 102,
		PIERRE_FEU		= 103,
		PIERRE_PLANTE	= 104,
		PIERRE_EAU		= 105,
		ECHANGE			= 106,
		END_EVOLUTION_CONDITION,
	};

#pragma pack(push, 1)

	struct EvolutionHeader {
		uint8_t nodeNumber = 0;
		uint32_t crc = 0;
	} PACKED;

	struct EvolutionNode {
		uint8_t idBase = 0;
		uint8_t idEvolution = 0;
		uint8_t condition = 0;
	} PACKED;

#pragma pack(pop)

	class StorageEvolutions {
	public:

		EvolutionHeader header;

		EvolutionNode *nodes = nullptr;

		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

		[[nodiscard]] bool write(const std::string &path) const noexcept;

		[[nodiscard]] uint32_t calcCrc() const noexcept;

		~StorageEvolutions() noexcept;

		constexpr const static char *ConditionName[END_EVOLUTION_CONDITION] {
			"FOUDRE",
			"LUNE",
			"FEU",
			"PLANTE",
			"EAU",
			"ECHANGE"
		};
	};
}

#endif //POKEMONTITANE_STORAGEEVOLUTIONS_HPP
