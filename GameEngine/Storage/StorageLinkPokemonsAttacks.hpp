//
// Created by azkalaak on 19/02/2022.
//

#ifndef POKEMONTITANE_STORAGELINKPOKEMONSATTACKS_HPP
#define POKEMONTITANE_STORAGELINKPOKEMONSATTACKS_HPP

#include <cstdint>
#include <string>

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif


namespace Titane {

#pragma pack(push, 1)

	struct linkPokemonsAttacksHeader {
		uint32_t nodeNumber = 0;
		uint32_t crc = 0;
	} PACKED;

	struct linkPokemonsAttacksNode {
		uint8_t attackId = 0;
		uint8_t condition = 0; // if condition is above 100 then it means it is not a level-type one
	} PACKED;

#pragma pack(pop)

	class StorageLinkPokemonsAttacks {
	public:

		linkPokemonsAttacksHeader header = {};
		uint8_t *pokemonAttackNumber = nullptr;
		linkPokemonsAttacksNode **nodes = nullptr; // The key here is the pokemon ID

		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

		[[nodiscard]] bool write(const std::string &path) const noexcept;

		[[nodiscard]] uint32_t calcCrc() const noexcept;

		~StorageLinkPokemonsAttacks() noexcept;
	};
}

#endif //POKEMONTITANE_STORAGELINKPOKEMONSATTACKS_HPP
