//
// Created by azkalaak on 21. 4. 9..
//

#ifndef POKEMONTITANE_STORAGEMAP_HPP
#define POKEMONTITANE_STORAGEMAP_HPP

#include <cstdint>
#include "Zenith/ZenithVector.hpp"

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif

namespace Titane {


	enum Maps {
		BOURG_JOY			= 0,
		PRAIMISSE			= 1,
		CREPUSCUL_AIRE		= 2,
		CRYONIS				= 3,
		CONTREFORT			= 4,
		SEOULOPOLIS			= 5,
		KORALGA				= 6,
		GEJUDO				= 7,
		ROUTE1				= 8,
		ROUTE2				= 9,
		ROUTE3				= 10,
		ROUTE4				= 11,
		ROUTE5				= 12,
		ROUTE6				= 13,
		ROUTE7				= 14,
		ROUTE8				= 15,
		ROUTE_VICTOIRE		= 16,
		GOUFFRE_ENTRENEIGE	= 17,
		VOLCAN_KIBRUL		= 18,
		LIGUE_POKEMON_INT	= 19,
		LIGUE_POKEMON_EXT	= 20,
		MAP_END                // always keep last as it is the count of the maps
	};

#pragma pack(push, 1)

// the TP places. it will hold all the tp data on each maps
	struct TPHolder {
// where do we TP from?
		uint16_t TPFromX = 0;
		uint16_t TPFromY = 0;
// where do we TP to?
		uint16_t TPToX = 0;
		uint16_t TPToY = 0;
// which map is it?
		Maps mapKey = MAP_END;
	} PACKED;

	struct MapNode {
// "Zone id" (grouping tiles)
		uint8_t zoneID = 0;
// background
		uint8_t bg = 0;
// middle layer
		uint8_t mid = 0;
// top layer
		uint8_t top = 0;
// does it collide with the player?
		uint8_t collision = true;
	} PACKED;

// we keep it as a full structure in case we want to add anything
	struct MapLineInfo {
		uint16_t width = 0;
	} PACKED;

	struct MapHeader {
// DURING CRC CALCULATION, THIS NEEDS TO BE RESETTED AT 0
		uint32_t crc = 0;
// number of lines of the map
		uint16_t mapHeight = 0;
// this needs to correspond to the loaded ID, else we throw
		Maps mapID = MAP_END;
// number of struct TP following the current structure
		uint8_t TPNumber = 0;
		uint8_t mapName[50] {};
	} PACKED;
#pragma pack(pop)

/*
 * DATA STRUCTURE OF A MAP:
 * Header (containing all usefull informations about the file
 *
 * MAP
 * 	each map line must be ordered in the following way:
 * 	LineInfo - Line
 *
 * The lineInfo must contains the useful informations about the map (size of the line)
 * The line is composed of nodes.
 *
 * TP
 *
 *
 * */


	class StorageMap {
	public:
/*       DATA PART BEGIN    */
		MapHeader header{};
		MapLineInfo *infos = nullptr;
		MapNode **map = nullptr;
		TPHolder *tp = nullptr;
// we later add the pnj and pkm system, we have no use of it there

/*        DATA PART LAY_END     */

// we read the entire map from the given file. only verification done here is CRC and line
		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

// write the entire map to a file. calculation of the CRC is done there.
		[[nodiscard]] bool write(const std::string &path) const noexcept;

// the CRC calculation is done there. the CRC is NEVER written into the stored header
		[[nodiscard]] uint32_t calcCrc() const noexcept;

// Classic destructor. destroys what has been allocated
		~StorageMap() noexcept;
	};

	bool operator==(const TPHolder &tp1, const TPHolder &tp2);
	bool operator!=(const TPHolder &tp1, const TPHolder &tp2);
	bool operator>(const TPHolder &tp1, const TPHolder &tp2);
	bool operator<(const TPHolder &tp1, const TPHolder &tp2);
	bool operator>=(const TPHolder &tp1, const TPHolder &tp2);
	bool operator<=(const TPHolder &tp1, const TPHolder &tp2);
}


#endif //POKEMONTITANE_STORAGEMAP_HPP
