//
// Created by azkal on 27/01/2022.
//

#ifndef POKEMONTITANE_STORAGEPOKEMON_HPP
#define POKEMONTITANE_STORAGEPOKEMON_HPP

#include <cstdint>
#include <string>

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif



namespace Titane {



#pragma pack(push, 1)

	struct PokemonHeader {
// The number of pokemon to read
		uint16_t pokemonNumber = 0;
// The CRC number, need to be 0 during CRC calculation
		uint32_t crc = 0;
	};

	struct PokemonNode {
// The Pokemon ID
		uint16_t id = 0;
// The pokemon Name
		int8_t name[20] = {};
// The pokemon HP;
		uint8_t hp = 0;
// The pokemon attack
		uint8_t attack = 0;
// The pokemon defense
		uint8_t defence = 0;
// The pokemon special attack
		uint8_t specialAttack = 0;
// The pokemon special defence
		uint8_t specialDefence = 0;
// The pokemon speed
		uint8_t speed = 0;
// The pokemon first type
		uint8_t type1 = 0;
// The pokemon second type
		uint8_t type2 = 0;
	} PACKED;

#pragma pack(pop)

	class StoragePokemon {
	public:
		PokemonHeader header{};
		PokemonNode *pokemons = nullptr;

		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

		[[nodiscard]] bool write(const std::string &path) const noexcept;

		[[nodiscard]] uint32_t calcCrc() const noexcept;

		~StoragePokemon() noexcept;

		enum Types {
			AUCUN		= 0,
			ACIER		= 1,
			COMBAT		= 2,
			DRAGON		= 3,
			EAU			= 4,
			ELECTRIK	= 5,
			FEU			= 6,
			GLACE		= 7,
			INSECTE		= 8,
			NORMAL		= 9,
			PLANTE		= 10,
			POISON		= 11,
			PSY			= 12,
			ROCHE		= 13,
			SOL			= 14,
			SPECTRE		= 15,
			TENEBRE		= 16,
			VOL			= 17,
			TYPES_END
		};

		constexpr const static char *TypesNames[TYPES_END]  {
				"AUCUN",
				"ACIER",
				"COMBAT",
				"DRAGON",
				"EAU",
				"ELECTRIK",
				"FEU",
				"GLACE",
				"INSECTE",
				"NORMAL",
				"PLANTE",
				"POISON",
				"PSY",
				"ROCHE",
				"SOL",
				"SPECTRE",
				"TENEBRE",
				"VOL"
		};

	};
}

#endif //POKEMONTITANE_STORAGEPOKEMON_HPP
