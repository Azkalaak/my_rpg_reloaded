//
// Created by azkalaak on 21/02/2022.
//

#ifndef POKEMONTITANE_STORAGEENCOUNTER_HPP
#define POKEMONTITANE_STORAGEENCOUNTER_HPP

#include <cstdint>
#include "StorageMap.hpp"

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif

namespace Titane {

#pragma pack(push, 1)

	struct encounterHeader {
		uint32_t crc = 0;
	} PACKED;

	struct encounterNode {
		uint8_t pokemonId = 0;
		uint8_t minLvl = 0;
		uint8_t  maxLvl = 0;
	};

#pragma pack(pop)

	class StorageEncounter {
	public:
		encounterHeader header;

		uint8_t numberPkm[MAP_END] {}; // problem here: not all maps have pokemons

		encounterNode *nodes[MAP_END] {nullptr}; // problem here: not all maps have pokemons

		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

		[[nodiscard]] bool write(const std::string &path) const noexcept;

		[[nodiscard]] uint32_t calcCrc() const noexcept;

		~StorageEncounter() noexcept;
	};
}

#endif //POKEMONTITANE_STORAGEENCOUNTER_HPP
