//
// Created by azkal on 02/02/2022.
//

#ifndef POKEMONTITANE_STORAGEATTACKS_HPP
#define POKEMONTITANE_STORAGEATTACKS_HPP

#ifdef _MSC_VER
#define PACKED
#else
#define PACKED __attribute__((packed, aligned(1)))
#endif


namespace Titane {

	struct AttackEffectStruct {
		enum attackCategory {
			PHYSICAL,
			SPECIAL,
			NONE
		};

		enum attackEffect {
			POISON				= 0,
			POISON_BAD			= 1,
			SLEEP				= 2,
			PARA				= 3,
			FREEZE				= 4,
			BURN				= 5,
			DIZZY				= 6,
			POS_POISON			= 7,
			POS_SLEEP			= 8,
			POS_PARA			= 9,
			POS_FREEZE			= 10,
			POS_BURN			= 11,
			POS_DIZZY			= 12,
			SELF_SLEEP			= 13,
			SELF_DIZZY			= 14,
			// ===========
			// END OF STATUS EFFECTS
			// ===========
			ATTCK_UP			= 15,
			ATTCK_SPE_UP		= 16,
			DEF_UP				= 17,
			DEF_SPE_UP			= 18,
			SPEED_UP			= 19,
			DODGE_UP			= 20,
			AIM_UP				= 21,
			SPE_UP				= 22,
			ATTCK_UP_2			= 23,
			ATTCK_SPE_UP_2		= 24,
			DEF_UP_2			= 25,
			DEF_SPE_UP_2		= 26,
			SPEED_UP_2			= 27,
			DODGE_UP_2			= 28,
			AIM_UP_2			= 29,
			SPE_UP_2			= 30,
			ATTCK_DOWN			= 31,
			ATTCK_SPE_DOWN		= 32,
			DEF_DOWN			= 33,
			DEF_SPE_DOWN		= 34,
			SPEED_DOWN			= 35,
			DODGE_DOWN			= 36,
			AIM_DOWN			= 37,
			SPE_DOWN			= 38,
			DEF_DOWN_2			= 39,
			POS_ATTCK_DOWN		= 40,
			POS_ATTCK_SPE_DOWN	= 41,
			POS_DEF_DOWN		= 42,
			POS_DEF_SPE_DOWN	= 43,
			POS_SPEED_DOWN		= 44,
			POS_DODGE_DOWN		= 45,
			POS_AIM_DOWN		= 46,
			POS_SPE_DOWN		= 47,
			// ===========
			// END OF STATS MODIFICATIONS
			// ===========
			CLEAR_STATS,
			PREVENT_STATS_MOD,

			POS_FEAR,
			HIGH_CRIT,
			SELF_DAMAGE,
			SELF_DAMAGE_IF_MISS,
			SELF_KO,
			INVINCIBLE_DURING_CAST,

			CHAINED_2,
			CHAINED_MAX_3,
			CHAINED_MAX_5,
			CHAINED_INFINITE,
			TRAP_CHAIN_MAX_5,

			ATTACK_NEXT_TURN,
			PASS_NEXT_TURN,
			KO_IF_SPEED_ABOVE,

			DMG_IS_DOUBLE_DAMAGE_TAKEN,
			DMG_IS_LEVEL,
			DMG_iS_DOUBLE,
			DMG_IS_20_PV,
			DMG_IS_40_PV,
			DMG_IS_50_PERCENT,

			HEAL_DMG_INFLICTED,
			HEAL_50_PERCENT,
			HEAL_TOTAL,

			MORPHING,
			CHANGE_TYPE,
			REPLACE_ATTACK_WITH_RANDOM,
			END_SAVAGE_ENCOUNTER,
			PRIORITY,
			NEEDS_ENNEMY_SLEEPING,

			NO_EFFECT,

			END_ATTACK_EFFECTS
		};
	};

#pragma pack(push, 1)

	struct AttackHeader {
		uint8_t nodeNumber = 0;
		uint32_t crc = 0;
	} PACKED;

	struct AttackNode {
		uint8_t id = 0;
		uint8_t name[16] {};
		uint8_t type = 0;
		uint8_t category = 0;
		uint8_t power = 0;
		uint8_t precision = 0;
		uint8_t pp = 0;
		uint8_t effect1 = AttackEffectStruct::END_ATTACK_EFFECTS;
		uint8_t effect2 = AttackEffectStruct::END_ATTACK_EFFECTS;
	} PACKED;

#pragma pack(pop)

	class StorageAttacks {
	public:

		AttackHeader header;

		AttackNode *nodes = nullptr;

		[[nodiscard]] bool load(const std::string &path) noexcept;

		void unload() noexcept;

		[[nodiscard]] bool write(const std::string &path) const noexcept;

		[[nodiscard]] uint32_t calcCrc() const noexcept;

		~StorageAttacks() noexcept;

		constexpr const static char *AttackEffectsName[AttackEffectStruct::END_ATTACK_EFFECTS]  {
				"POISON",
				"POISON_GRAVE",
				"DODO",
				"PARA",
				"GELE",
				"BRUL",
				"CONFUS",
				"POS_POISON",
				"POS_DODO",
				"POS_PARA",
				"POS_GEL",
				"POS_BRUL",
				"POS_CONFUS",
				"SELF_DODO",
				"SELF_CONFUS",

				"ATT_UP",
				"ATT_SPE_UP",
				"DEF_UP",
				"DEF_SPE_UP",
				"VIT_UP",
				"ESQU_UP",
				"PREC_UP",
				"SPE_UP",
				"ATT_UP_2",
				"ATT_SPE_UP_2",
				"DEF_UP_2",
				"DEF_SPE_UP_2",
				"VIT_UP_2",
				"ESQU_UP_2",
				"PREC_UP_2",
				"SPE_UP_2",
				"ATT_DOWN",
				"ATT_SPE_DOWN",
				"DEF_DOWN",
				"DEF_SPE_DOWN",
				"VIT_DOWN",
				"ESQU_DOWN",
				"PREC_DOWN",
				"SPE_DOWN",
				"DEF_DOWN_2",
				"POS_ATT_DOWN",
				"POS_ATT_SPE_DOWN",
				"POS_DEF_DOWN",
				"POS_DEF_SPE_DOWN",
				"POS_VIT_DOWN",
				"POS_ESQU_DOWN",
				"POS_PREC_DOWN",
				"POS_SPE_DOWN",

				"CLEAR_STAT",
				"NO_STAT",

				"APEURER",
				"CRIT",
				"BLESSE",
				"PV_ECHEC",
				"SELF_KO",
				"NOHIT",

				"DOUBLE_COUP",
				"CHAINE_3",
				"CHAINE_5",
				"CHAINE",
				"BACK_CHAINE",

				"DEUX_TOUR",
				"IMMO",
				"KO_SPEED",

				"DOUBLE_ATTCK",
				"DOUBLE_LVL",
				"MID_DEF",
				"20_PV",
				"40_PV",
				"DMG_50",

				"SOIN_DEGAT",
				"SOIN_50",
				"SOIN_TOTAL",

				"MORPH",
				"CHANGE_TYPE",
				"ATTCK_LEARN",
				"FIN_COMBT",
				"PRIO",
				"SI_DODO",

				"NO",
		};

	};

}

#endif //POKEMONTITANE_STORAGEATTACKS_HPP
