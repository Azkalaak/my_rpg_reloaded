//
// Created by azkal on 09/12/2021.
//

#ifndef POKEMONTITANE_STORAGETOTEXTURES_HPP
#define POKEMONTITANE_STORAGETOTEXTURES_HPP

#include "Storage/StorageMap.hpp"

namespace Titane {
	class StorageToTextures {
	public:
		enum Layers {
			BOT, COL, TOP, LAY_END
		};
		enum Character {
			MAN, WOMAN, CHAR_END
		};
	private:

		const char *_characterPath[CHAR_END] = {
				"./GameData/textures/character/man.png",
				"./GameData/textures/character/woman.png"
		};

		const char *_mapPath[LAY_END][Titane::Maps::MAP_END] = {{
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot_crep.png",
																		"./GameData/textures/tiling/bot_snow.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot_gej.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot_snow.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot_cave.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot.png",
																		"./GameData/textures/tiling/bot_cave.png",
																		"./GameData/textures/tiling/bot_snow.png",
																		"./GameData/textures/tiling/bot_cave.png",
																		"./GameData/textures/tiling/bot_ligue_in.png",
																		"./GameData/textures/tiling/bot.png"
																},
																{
																		"./GameData/textures/tiling/col_village.png",
																		"./GameData/textures/tiling/col_city.png",
																		"./GameData/textures/tiling/col_crep.png",
																		"./GameData/textures/tiling/col_cryo.png",
																		"./GameData/textures/tiling/col_contre.png",
																		"./GameData/textures/tiling/col_seoul.png",
																		"./GameData/textures/tiling/col_koral.png",
																		"./GameData/textures/tiling/col_gej.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_snow.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_caves.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_road.png",
																		"./GameData/textures/tiling/col_caves.png",
																		"./GameData/textures/tiling/col_snow.png",
																		"./GameData/textures/tiling/col_kibrul.png",
																		"./GameData/textures/tiling/col_ligue_in.png",
																		"./GameData/textures/tiling/col_ligue_out.png",
																},
																{
																		"./GameData/textures/tiling/top_village.png",
																		"./GameData/textures/tiling/top_city.png",
																		"./GameData/textures/tiling/top_crep.png",
																		"./GameData/textures/tiling/top_cryo.png",
																		"./GameData/textures/tiling/top_contre.png",
																		"./GameData/textures/tiling/top_seoul.png",
																		"./GameData/textures/tiling/top_koral.png",
																		"./GameData/textures/tiling/top_gej.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_snow.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_caves.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_road.png",
																		"./GameData/textures/tiling/top_caves.png",
																		"./GameData/textures/tiling/top_snow.png",
																		"./GameData/textures/tiling/top_caves.png",
																		"./GameData/textures/tiling/top_ligue_in.png",
																		"./GameData/textures/tiling/top_ligue_out.png",
																}};


	public:

		// Checks if all assets are there
		bool verification();

		[[nodiscard]] const char *getMapTexture(Layers layer, Maps key) const noexcept;

		[[nodiscard]] const char *getCharacterTexture(Character charac) const noexcept;

		[[nodiscard]] const char *getTexture(Character charac) const noexcept
		{ return getCharacterTexture(charac); }

		[[nodiscard]] const char *getTexture(Layers layer, Maps key) const noexcept
		{ return getMapTexture(layer, key); }
	};
}

#endif //POKEMONTITANE_STORAGETOTEXTURES_HPP
