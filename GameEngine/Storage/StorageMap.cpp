//
// Created by azkalaak on 21. 4. 9..
//

#include <fstream>
#include <iostream>
#include <filesystem>

#include "StorageMap.hpp"
#include "Zenith/ZenithCRC.hpp"

using namespace Titane;

bool StorageMap::load(const std::string &path) noexcept
{
	std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
	bool returnValue = file.is_open();

// Lecture of the header
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(&header),
								 sizeof(MapHeader)).eof();

// Allocation and lecture of the map information
	infos = new MapLineInfo[header.mapHeight]{};
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(infos),
								 static_cast<uint16_t>(sizeof(MapLineInfo)) * header.mapHeight).eof();

// Allocation and lecture of the map nodes
	map = new MapNode *[header.mapHeight]{};
	for (uint16_t i = 0; i < header.mapHeight; ++i) {
		map[i] = new MapNode[infos[i].width]{};
		if (returnValue)
			returnValue = !file.read(reinterpret_cast<char *>(map[i]),
									 static_cast<uint16_t>(sizeof(MapNode)) * infos[i].width).eof();
		else
			break;
	}

// Allocation and lecture of the TP points
	tp = new TPHolder[header.TPNumber]{};
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(tp),
								 static_cast<uint32_t>(sizeof(TPHolder)) * header.TPNumber).eof();
	file.close();
	return returnValue;
}

bool StorageMap::write(const std::string &path) const noexcept
{
	std::filesystem::create_directories("./GameData/PokemonData");
	std::ofstream file(path, std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open())
		return false;

// Calculation of the CRC and header writing
	MapHeader newHeader = header;
	newHeader.crc = calcCrc();



	file.write(reinterpret_cast<const char *>(&newHeader), sizeof newHeader);

// Writing of the map informations
	file.write(reinterpret_cast<const char *>(infos), static_cast<uint16_t>(sizeof(MapLineInfo)) * header.mapHeight);

// Writing of the map nodes
	for (uint16_t i = 0; i < header.mapHeight; ++i) {
		file.write(reinterpret_cast<const char *>(map[i]), static_cast<uint16_t>(sizeof(MapNode)) * infos[i].width);
	}

//writing of the TP points
	file.write(reinterpret_cast<const char *>(tp), static_cast<uint32_t> (sizeof(TPHolder)) * header.TPNumber);

	file.close();

	return true;
}

uint32_t StorageMap::calcCrc() const noexcept
{
	Zenith::ZenithCRC crcCalc;
	MapHeader headerTmp = header;

// we feed the header
	headerTmp.crc = 0;
	crcCalc.feed(&headerTmp, sizeof header);

// we feed the map info
	crcCalc.feed(infos, sizeof(MapLineInfo) * header.mapHeight);

// we feed the map nodes
	for (int i = 0; i < header.mapHeight; ++i)
		crcCalc.feed(map[i], sizeof(MapNode) * infos[i].width);

// we feed the tp points
	crcCalc.feed(tp, header.TPNumber * sizeof(TPHolder));

	return crcCalc.getCRC();
}

StorageMap::~StorageMap() noexcept
{
	if (!map)
		return;
	for (int i = 0; i < header.mapHeight; ++i)
		delete[] map[i];
	delete[] map;
	delete[] infos;
	delete[] tp;
}

void StorageMap::unload() noexcept
{
	for (int i = 0; i < header.mapHeight; ++i)
		delete[] map[i];
	delete[] map;
	delete[] infos;
	delete[] tp;
	map = nullptr;
	infos = nullptr;
	tp = nullptr;
}

bool Titane::operator==(const TPHolder &tp1, const TPHolder &tp2)
{
	return tp1.mapKey == tp2.mapKey && tp1.TPFromX == tp2.TPFromX && tp1.TPFromY == tp2.TPFromY;
}

bool Titane::operator!=(const TPHolder &tp1, const TPHolder &tp2)
{
	return !(tp1 == tp2);
}

bool Titane::operator<(const TPHolder &tp1, const TPHolder &tp2)
{
	return tp1.TPFromX < tp2.TPFromX || (tp1.TPFromX == tp2.TPFromX && tp1.TPFromY < tp2.TPFromY);
}

bool Titane::operator>(const TPHolder &tp1, const TPHolder &tp2)
{
	return tp1.TPFromX > tp2.TPFromX || (tp1.TPFromX == tp2.TPFromX && tp1.TPFromY > tp2.TPFromY);
}

bool Titane::operator<=(const TPHolder &tp1, const TPHolder &tp2)
{
	return tp1 < tp2 || tp1 == tp2;
}

bool Titane::operator>=(const TPHolder &tp1, const TPHolder &tp2)
{
	return tp1 > tp2 || tp1 == tp2;
}
