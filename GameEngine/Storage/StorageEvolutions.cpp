//
// Created by azkal on 07/02/2022.
//

#include "StorageEvolutions.hpp"
#include <fstream>
#include <filesystem>

#include "Zenith/ZenithCRC.hpp"

using namespace Titane;

bool StorageEvolutions::load(const std::string &path) noexcept
{
	std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
	bool returnValue = file.is_open();

// we read the header
	if (returnValue)
		returnValue = !file.read(reinterpret_cast<char *>(&header), sizeof(EvolutionHeader)).eof();

	// Lecture of all attacks at once
	if (returnValue) {
		nodes = new EvolutionNode[header.nodeNumber];
		returnValue = !file.read(reinterpret_cast<char *>(nodes), header.nodeNumber * static_cast<uint16_t>(sizeof(EvolutionNode))).eof();
	}

	file.close();
	return returnValue;
}

bool StorageEvolutions::write(const std::string &path) const noexcept
{
	std::filesystem::create_directories("./GameData/PokemonData");
	std::ofstream file(path, std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);

	if (!file.is_open())
		return false;

	EvolutionHeader newHeader = header;
	newHeader.crc = calcCrc();



	file.write(reinterpret_cast<const char *>(&newHeader), sizeof newHeader);
	file.write(reinterpret_cast<const char *>(nodes), static_cast<uint32_t>(sizeof(EvolutionNode)) * header.nodeNumber);
	file.close();
	return true;
}

uint32_t StorageEvolutions::calcCrc() const noexcept
{
	Zenith::ZenithCRC crcCalc;

	EvolutionHeader headerTmp = header;

// feeding the crc calculator with the header
	headerTmp.crc = 0;
	crcCalc.feed(&headerTmp, sizeof headerTmp);

// Then feeding with pokemon nodes
	crcCalc.feed(nodes, sizeof(EvolutionNode) * headerTmp.nodeNumber);

// we return the calculated CRC
	return crcCalc.getCRC();
}

void StorageEvolutions::unload() noexcept
{
	delete[] nodes;
	nodes = nullptr;
}

StorageEvolutions::~StorageEvolutions() noexcept
{
	delete[] nodes;
}
