//
// Created by azkal on 09/12/2021.
//

#include <fstream>

#include "StorageToTextures.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

bool StorageToTextures::verification()
{
	bool retValue = true;
	std::ifstream file;

	for (auto &i: _mapPath) {
		for (auto &j: i) {
			file.open(j);
			if (!file.is_open()) {
				retValue = false;
				TitaneLogger.reportErr("File [", j, "] could not be found");
			} else
				file.close();
		}
	}
	for (auto &i: _characterPath) {
		file.open(i);
		if (!file.is_open()) {
			retValue = false;
			TitaneLogger.reportErr("File [", i, "] could not be found");
		} else
			file.close();
	}

	return retValue;
}

const char *StorageToTextures::getMapTexture(Layers layer, Maps key) const noexcept
{
	return _mapPath[layer][key];
}

const char *StorageToTextures::getCharacterTexture(Character character) const noexcept
{
	return _characterPath[character];
}
