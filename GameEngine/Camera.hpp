//
// Created by azkal on 08/02/2022.
//

#ifndef POKEMONTITANE_CAMERA_HPP
#define POKEMONTITANE_CAMERA_HPP

#include "DataClass/EntityMovement.hpp"

namespace Titane {
	class Camera {
	private:
		EntityMovement position;

	public:
		Camera() = default;

		void updatePosition(const EntityMovement &newPosition) { position = newPosition; }

		const EntityMovement &getPosition() { return position; }
	};
}

#endif //POKEMONTITANE_CAMERA_HPP
