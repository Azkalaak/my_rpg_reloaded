//
// Created by azkal on 10/12/2021.
//

#include "TextureManager.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

bool TextureManager::Verification()
{
	return _storageToTextures.verification();
}

void TextureManager::loadTexture(const std::string &texture)
{
	_textureList[texture].loadFromFile(texture);
}

const sf::Texture &TextureManager::getTexture(const std::string &texture)
{
#ifndef APPLE_NO_FULL_CXX_20
	if (!_textureList.contains(texture))
#else
	if (_textureList.count(texture) == 0)
#endif
		loadTexture(texture);
	return _textureList[texture];
}

void TextureManager::loadTexture(StorageToTextures::Layers layer, Maps mapId)
{
	loadTexture(_storageToTextures.getTexture(layer, mapId));
}

void TextureManager::loadTexture(StorageToTextures::Character character)
{
	loadTexture(_storageToTextures.getTexture(character));
}

const sf::Texture &TextureManager::getTexture(StorageToTextures::Layers layer, Maps mapId)
{
	return getTexture(_storageToTextures.getTexture(layer, mapId));
}

const sf::Texture &TextureManager::getTexture(StorageToTextures::Character character)
{
	return getTexture(_storageToTextures.getTexture(character));
}

void TextureManager::loadIfNotLoaded(const std::string &texture)
{
#ifndef APPLE_NO_FULL_CXX_20
	if (!_textureList.contains(texture))
#else
	if (_textureList.count(texture) == 0)
#endif
		loadTexture(texture);
}

void TextureManager::loadIfNotLoaded(StorageToTextures::Layers layer, Maps mapId)
{
	loadIfNotLoaded(_storageToTextures.getTexture(layer, mapId));
}

void TextureManager::loadIfNotLoaded(StorageToTextures::Character character)
{
	loadIfNotLoaded(_storageToTextures.getTexture(character));
}

TextureManager::~TextureManager() = default;
