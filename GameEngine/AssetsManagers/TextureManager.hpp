//
// Created by azkal on 10/12/2021.
//

#ifndef POKEMONTITANE_TEXTUREMANAGER_HPP
#define POKEMONTITANE_TEXTUREMANAGER_HPP

#include <map>
#include "SFML/Graphics.hpp"
#include "Storage/StorageToTextures.hpp"
#include "Zenith/ZenithGlobalSingleton.hpp"

namespace Titane {
	class TextureManager {
	private:
		std::map<std::string, sf::Texture> _textureList;

		StorageToTextures _storageToTextures;
	protected:
		TextureManager() = default;
		virtual ~TextureManager();
	public:

		bool Verification();

		void loadTexture(const std::string &texture);

		void loadTexture(StorageToTextures::Layers layer, Maps mapId);

		void loadTexture(StorageToTextures::Character character);

		void loadIfNotLoaded(const std::string &texture);

		void loadIfNotLoaded(StorageToTextures::Layers layer, Maps mapId);

		void loadIfNotLoaded(StorageToTextures::Character character);

		const sf::Texture &getTexture(const std::string &texture);

		const sf::Texture &getTexture(StorageToTextures::Layers layer, Maps mapId);

		const sf::Texture &getTexture(StorageToTextures::Character character);
	};

	using TitaneTextureManager = Zenith::ZenithGlobalSingleton<Titane::TextureManager>;
}
#define TitaneTextureManager TitaneTextureManager::instance()

#endif //POKEMONTITANE_TEXTUREMANAGER_HPP
