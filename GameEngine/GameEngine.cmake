# ==========================================================================================================
# 											Shared sources
# ==========================================================================================================

set(
	TITANE_SHARED_SOURCES
		Logs/Logger.cpp
		Storage/StorageAttacks.cpp
		Storage/StorageEncounter.cpp
		Storage/StorageEvolutions.cpp
		Storage/StorageLinkPokemonsAttacks.cpp
		Storage/StorageMap.cpp
		Storage/StoragePokemon.cpp
)

# ==========================================================================================================
# 											Excel Editor sources
# ==========================================================================================================

set(
	TITANE_EXCEL_EDITOR_SOURCES
		Attacks/EditableAttacks.cpp
		Map/EditableMap.cpp
		Pokemons/EditableEncounters.cpp
		Pokemons/EditableEvolutions.cpp
		Pokemons/EditableListPokemonsAttacks.cpp
		Pokemons/EditablePokemons.cpp
)

# ==========================================================================================================
# 											Pokemon Titane sources
# ==========================================================================================================

set(
	TITANE_POKEMON_SOURCES
		Camera.cpp
		Collisions/CollisionManager.cpp
		Controller/CameraController.cpp
		Controller/ControllerManager.cpp
		Controller/EmptyController.cpp
		Controller/IClassicController.cpp
		Entity.cpp
		Events/UserEvents.cpp
		Logs/Logger.cpp
		Map/Map.cpp
		Map/MapManager.cpp
		Pokemons/Encounters.cpp
		Pokemons/Evolutions.cpp
		Pokemons/ListPokemonsAttacks.cpp
		Pokemons/Pokemons.cpp
		Renderer/MapRenderer.cpp
		Renderer/PlayerRenderer.cpp
		Renderer/RendererManager.cpp
		Scenes/IScene.cpp
		Scenes/SceneCombat.cpp
		Scenes/SceneMain.cpp
		Scenes/SceneStartup.cpp
		TitaneEngine.cpp
		AssetsManagers/TextureManager.cpp
		Trigger/TriggerManager.cpp
		Storage/StorageToTextures.cpp
		Utils/Fader.cpp
)

# ==========================================================================================================
# 											Path put to sources
# ==========================================================================================================

list(TRANSFORM TITANE_SHARED_SOURCES PREPEND "GameEngine/")
list(TRANSFORM TITANE_EXCEL_EDITOR_SOURCES PREPEND "GameEngine/")
list(TRANSFORM TITANE_POKEMON_SOURCES PREPEND "GameEngine/")

# ==========================================================================================================
# 											Sources linked to target
# ==========================================================================================================

target_sources(
		PokemonTitane
		PRIVATE
			${TITANE_SHARED_SOURCES}
			${TITANE_POKEMON_SOURCES}
)

target_sources(
		PokemonTitaneExcelEditor
		PRIVATE
			${TITANE_SHARED_SOURCES}
			${TITANE_EXCEL_EDITOR_SOURCES}
)