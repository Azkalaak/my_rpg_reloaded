//
// Created by azkal on 27/01/2022.
//

#ifndef POKEMONTITANE_EDITABLEPOKEMONS_HPP
#define POKEMONTITANE_EDITABLEPOKEMONS_HPP

#include <list>
#include "DataClass/Pokemon.hpp"

namespace Titane {
	class EditablePokemons {
	private:
		std::list<Pokemon> _pokemons;

		StoragePokemon _storage;

		const std::string _path = "./GameData/PokemonData/pokemons";
	public:

		void load();

		void addPokemon(const Pokemon &pokemon);

		void addPokemon(const std::string &name, int id, int hp, int attack, int defence, int specialAttack,
						int specialDefence, int speed, StoragePokemon::Types type1, StoragePokemon::Types type2 = StoragePokemon::AUCUN);

		void save();

		void verify();
	};
}

#endif //POKEMONTITANE_EDITABLEPOKEMONS_HPP
