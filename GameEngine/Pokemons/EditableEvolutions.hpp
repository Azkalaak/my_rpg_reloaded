//
// Created by azkal on 07/02/2022.
//

#ifndef POKEMONTITANE_EDITABLEEVOLUTIONS_HPP
#define POKEMONTITANE_EDITABLEEVOLUTIONS_HPP

#include <list>
#include "Storage/StorageEvolutions.hpp"

namespace Titane {
	class EditableEvolutions {
	private:
		std::list<EvolutionNode> _nodes;

		StorageEvolutions _storage;

		const std::string _path = "./GameData/PokemonData/evolutions";
	public:
		void load();

		void addEvolution(const EvolutionNode &evolution);

		void addEvolution(int idFrom, int idTo, int condition);

		void save();

		void verify();
	};

	bool operator<(const EvolutionNode &node1, const EvolutionNode &node2) noexcept;
	bool operator>(const EvolutionNode &node1, const EvolutionNode &node2) noexcept;
	bool operator==(const EvolutionNode &node1, const EvolutionNode &node2) noexcept;
	bool operator!=(const EvolutionNode &node1, const EvolutionNode &node2) noexcept;

}

#endif //POKEMONTITANE_EDITABLEEVOLUTIONS_HPP
