//
// Created by azkalaak on 22/02/2022.
//

#ifndef POKEMONTITANE_POKEMONS_HPP
#define POKEMONTITANE_POKEMONS_HPP

#include "Storage/StoragePokemon.hpp"
#include "DataClass/Pokemon.hpp"
#include "Zenith/ZenithGlobalSingleton.hpp"

namespace Titane {
	class Pokemons {
	private:
		StoragePokemon _storage;

		const std::string _path = "./GameData/PokemonData/pokemons";

	protected:
		Pokemons();
	public:
		virtual ~Pokemons();

		void reload();

		[[nodiscard]] Pokemon getPokemon(int pokemonId) const;

	};
	using PokemonsFromStorage = Zenith::ZenithGlobalSingleton<Pokemons>;
}

#define PokemonsFromStorage PokemonsFromStorage::instance()

#endif //POKEMONTITANE_POKEMONS_HPP
