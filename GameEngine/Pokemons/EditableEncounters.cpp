//
// Created by azkalaak on 21/02/2022.
//

#include "EditableEncounters.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

bool Titane::operator<(const encounterNode &node1, const encounterNode &node2) noexcept
{
	return node1.pokemonId < node2.pokemonId;
}

bool Titane::operator>(const encounterNode &node1, const encounterNode &node2) noexcept
{
	return node1.pokemonId > node2.pokemonId;
}

bool Titane::operator==(const encounterNode &node1, const encounterNode &node2) noexcept
{
	return node1.pokemonId == node2.pokemonId;
}

bool Titane::operator!=(const encounterNode &node1, const encounterNode &node2) noexcept
{
	return node1.pokemonId != node2.pokemonId;
}

void EditableEncounters::load()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Encounter file file invalid! please generate / regenerate it");

	for (int i = 0; i < MAP_END; ++i)
		for (int j = 0; j < _storage.numberPkm[i]; ++j)
			_nodes[i].emplace_back(_storage.nodes[i][j]);
}

void EditableEncounters::addEncounter(int mapId, const encounterNode &node)
{
	_nodes[mapId].emplace_back(node);
}

void EditableEncounters::addEncounter(int mapId, int pokemonId, int minLevel, int maxLevel)
{
	encounterNode node {
		.pokemonId = static_cast<uint8_t>(pokemonId),
		.minLvl = static_cast<uint8_t>(minLevel),
		.maxLvl = static_cast<uint8_t>(maxLevel)
	};

	addEncounter(mapId, node);
}

void EditableEncounters::save()
{
	_storage.unload();
	for (auto &it : _nodes) {
		it.second.sort();
		it.second.unique();
	}
	for (int i = 0; i < MAP_END; ++i) {
#ifndef APPLE_NO_FULL_CXX_20
		if (!_nodes.contains(i)) {
#else
		if (_nodes.count(i) == 0) {
#endif
			++i;
			continue;
		}
		_storage.numberPkm[i] = static_cast<uint8_t>(_nodes[i].size());
		_storage.nodes[i] = new encounterNode[_storage.numberPkm[i]];
		int j = 0;
		for (auto &node : _nodes[i]) {
			_storage.nodes[i][j] = node;
			++j;
		}
	}
	if (!_storage.write(_path))
		throw Zenith::ZenithException("Error while writing Encounter attacks file to system");

}

void EditableEncounters::verify()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Corrupted Encounter attack file!");
}
