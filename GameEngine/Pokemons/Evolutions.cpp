//
// Created by azkalaak on 23/02/2022.
//

#include "Evolutions.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

Evolutions::~Evolutions() = default;

Evolutions::Evolutions()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Evolution file invalid! please generate / regenerate it");
}

void Evolutions::reload()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Evolution file invalid! please generate / regenerate it");
}

bool Evolutions::doesEvolve(int pkmId, int lvl) const
{
	// WARNING, HERE THE MAX POKEMON NUMBER IS HARD CODED!!!!!
	// IT MIGHT NEED TO CHANGE IN THE FUTUR
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	for (int i = 0; i < _storage.header.nodeNumber; ++i)
		if (_storage.nodes[i].idBase == pkmId && _storage.nodes[i].condition == lvl)
			return true;
	return false;
}

int Evolutions::getEvolution(int pkmId, int lvl) const
{
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	for (int i = 0; i < _storage.header.nodeNumber; ++i)
		if (_storage.nodes[i].idBase == pkmId && _storage.nodes[i].condition == lvl)
			return _storage.nodes[i].idEvolution;
	return 0; // since it is not posible to have a pokemon of ID 0 this is the error output
}
