//
// Created by azkalaak on 23/02/2022.
//

#ifndef POKEMONTITANE_ENCOUNTERS_HPP
#define POKEMONTITANE_ENCOUNTERS_HPP

#include <random>
#include "Storage/StorageEncounter.hpp"
#include "Zenith/ZenithGlobalSingleton.hpp"

namespace Titane {
	class Encounters {
	private:
		std::random_device _random;

		StorageEncounter _storage;

		const std::string _path = "./GameData/PokemonData/encounter";

	protected:
		Encounters();
	public:

		virtual ~Encounters();

		void reload();

		[[nodiscard]] const encounterNode &getEncounter(Maps mapId);
	};

	using EncountersFromStorage = Zenith::ZenithGlobalSingleton<Encounters>;
}

#define EncountersFromStorage EncountersFromStorage::instance()

#endif //POKEMONTITANE_ENCOUNTERS_HPP
