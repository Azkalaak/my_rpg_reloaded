//
// Created by azkal on 27/01/2022.
//

#include <filesystem>
#include "EditablePokemons.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

static PokemonNode &operator<<(PokemonNode& stPkm, const Pokemon &pkm)
{
	for (size_t i = 0; i < pkm.name.size() && i < sizeof stPkm.name; ++i)
		stPkm.name[i] = pkm.name[i];
	stPkm.id = static_cast<uint16_t>(pkm.id);
	stPkm.hp = static_cast<uint8_t>(pkm.baseHp);
	stPkm.attack = static_cast<uint8_t>(pkm.attack);
	stPkm.defence = static_cast<uint8_t>(pkm.defence);
	stPkm.specialDefence = static_cast<uint8_t>(pkm.specialDefence);
	stPkm.specialAttack = static_cast<uint8_t>(pkm.specialAttack);
	stPkm.speed = static_cast<uint8_t>(pkm.speed);
	stPkm.type1 = pkm.type1;
	stPkm.type2 = pkm.type2;

	return stPkm;
}

void EditablePokemons::load()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Pokemon file invalid! please generate / regenerate it");

	for (int i = 0; i < _storage.header.pokemonNumber; ++i)
	{
		_pokemons.emplace_back(_storage.pokemons[i]);
	}
}

void EditablePokemons::addPokemon(const Pokemon &pokemon)
{
	_pokemons.emplace_back(pokemon);
}

void EditablePokemons::addPokemon(const std::string &name, int id, int hp, int attack, int defence, int specialAttack,
								  int specialDefence, int speed, StoragePokemon::Types type1, StoragePokemon::Types type2)
{
	_pokemons.emplace_back(name, id, hp, attack, defence, specialAttack, specialDefence, speed, type1, type2);
}

void EditablePokemons::save()
{
	_pokemons.sort();
	_pokemons.unique();

	auto *tmpPokemonHolder = new PokemonNode[_pokemons.size()];
	int i = 0;

	for (auto &j : _pokemons) {
		tmpPokemonHolder[i] << j;
		++i;
	}
	delete[] _storage.pokemons;
	_storage.pokemons = tmpPokemonHolder;
	_storage.header.pokemonNumber = static_cast<uint16_t>(_pokemons.size());

	if (!_storage.write(_path))
		throw Zenith::ZenithException("Error while writing pokemon file to system");
}

void EditablePokemons::verify()
{
	_storage.unload();

	if (!_storage.load(_path))
		throw Zenith::ZenithException("Corrupted pokemon file!");
}
