//
// Created by azkal on 07/02/2022.
//

#include "EditableEvolutions.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

bool Titane::operator<(const EvolutionNode &node1, const EvolutionNode &node2) noexcept
{
	return node1.idEvolution < node2.idEvolution;
}

bool Titane::operator>(const EvolutionNode &node1, const EvolutionNode &node2) noexcept
{
	return node1.idEvolution > node2.idEvolution;
}

bool Titane::operator==(const EvolutionNode &node1, const EvolutionNode &node2) noexcept
{
	return node1.idEvolution == node2.idEvolution;
}

bool Titane::operator!=(const EvolutionNode &node1, const EvolutionNode &node2) noexcept
{
	return node1.idEvolution != node2.idEvolution;
}

void EditableEvolutions::load()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Pokemon file invalid! please generate / regenerate it");

	for (int i = 0; i < _storage.header.nodeNumber; ++i)
	{
		_nodes.emplace_back(_storage.nodes[i]);
	}
}

void EditableEvolutions::addEvolution(const EvolutionNode &evolution)
{
	_nodes.emplace_back(evolution);
}

void
EditableEvolutions::addEvolution(int idFrom, int idTo, int condition)
{
	EvolutionNode newNode = {
			.idBase = static_cast<uint8_t>(idFrom),
			.idEvolution = static_cast<uint8_t>(idTo),
			.condition = static_cast<uint8_t>(condition),
	};
	addEvolution(newNode);
}

void EditableEvolutions::save()
{
	_nodes.sort();
	_nodes.unique();

	auto *tmpNodes = new EvolutionNode[_nodes.size()];
	int i = 0;

	for (auto &j : _nodes) {
		tmpNodes[i] = j;
		++i;
	}
	delete[] _storage.nodes;
	_storage.nodes = tmpNodes;
	_storage.header.nodeNumber = static_cast<uint8_t>(_nodes.size());

	if (!_storage.write(_path))
		throw Zenith::ZenithException("Error while writing evolution file to system");
}

void EditableEvolutions::verify()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Corrupted evolution file!");
}
