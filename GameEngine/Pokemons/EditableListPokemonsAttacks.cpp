//
// Created by azkalaak on 19/02/2022.
//

#include "EditableListPokemonsAttacks.hpp"
#include "Zenith/ZenithException.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

bool Titane::operator<(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept
{
	return node1.attackId < node2.attackId;
}

bool Titane::operator>(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept
{
	return node1.attackId > node2.attackId;
}

bool Titane::operator==(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept
{
	return node1.attackId == node2.attackId;
}

bool Titane::operator!=(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept
{
	return node1.attackId != node2.attackId;
}

void EditableListPokemonsAttacks::load()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Link pokemon file file invalid! please generate / regenerate it");

	for (int i = 0; i < static_cast<int>(_storage.header.nodeNumber); ++i)
		for (int j = 0; j < _storage.pokemonAttackNumber[i]; ++j)
			_nodes[i].emplace_back(_storage.nodes[i][j]);
}

void EditableListPokemonsAttacks::addAttack(int pokemonId, const linkPokemonsAttacksNode &attack)
{
	_nodes[pokemonId].emplace_back(attack);
}

void EditableListPokemonsAttacks::addAttack(int pokemonId, int attackId, int condition)
{
	linkPokemonsAttacksNode node {
		.attackId = static_cast<uint8_t>(attackId),
		.condition = static_cast<uint8_t>(condition),
	};
	addAttack(pokemonId, node);
}

void EditableListPokemonsAttacks::save()
{
	_storage.unload();

	for (auto &it : _nodes) {
		it.second.sort();
		it.second.unique();
	}
	_storage.header.nodeNumber = static_cast<uint32_t>(_nodes.size());
	_storage.pokemonAttackNumber = new uint8_t[_storage.header.nodeNumber];
	_storage.nodes = new linkPokemonsAttacksNode*[_storage.header.nodeNumber] {nullptr};
	int i = 0;
	for (auto &it: _nodes) {
		_storage.pokemonAttackNumber[i] = static_cast<uint8_t>(it.second.size());
		_storage.nodes[i] = new linkPokemonsAttacksNode[_storage.pokemonAttackNumber[i]];
		int j = 0;
		for (auto &node: it.second) {
			_storage.nodes[i][j] = node;
			++j;
		}
		++i;
	}
	if (!_storage.write(_path))
		throw Zenith::ZenithException("Error while writing List pokemon attacks file to system");
}

void EditableListPokemonsAttacks::verify()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Corrupted link pokemon attack file!");
}
