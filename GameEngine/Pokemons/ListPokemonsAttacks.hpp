//
// Created by azkalaak on 23/02/2022.
//

#ifndef POKEMONTITANE_LISTPOKEMONSATTACKS_HPP
#define POKEMONTITANE_LISTPOKEMONSATTACKS_HPP

#include <list>
#include "Storage/StorageLinkPokemonsAttacks.hpp"

namespace Titane {
	class ListPokemonsAttacks {
	private:
		StorageLinkPokemonsAttacks _storage;

		const std::string _path = "./GameData/PokemonData/linkPokemonsAttacks";

	public:

		ListPokemonsAttacks();

		void reload();

		[[nodiscard]] bool doesLearnAttack(int pkmId, int lvl) const;

		[[nodiscard]] int getLearnedAttack(int pkmId, int lvl) const;

		[[nodiscard]] std::list<int> getAllLearnedAttack(int pkmId, int lvl) const;

		[[nodiscard]] std::list<int> getAllLearnedAttackBeforeLevel(int pkmId, int lvl) const;

	};
}

#endif //POKEMONTITANE_LISTPOKEMONSATTACKS_HPP
