//
// Created by azkalaak on 19/02/2022.
//

#ifndef POKEMONTITANE_EDITABLELISTPOKEMONSATTACKS_HPP
#define POKEMONTITANE_EDITABLELISTPOKEMONSATTACKS_HPP

#include <vector>
#include <list>
#include <map>
#include "Storage/StorageLinkPokemonsAttacks.hpp"

namespace Titane {
	class EditableListPokemonsAttacks {
	private:

		StorageLinkPokemonsAttacks _storage;

		std::map<int, std::list<linkPokemonsAttacksNode>> _nodes;

		const std::string _path = "./GameData/PokemonData/linkPokemonsAttacks";
	public:
		void load();

		void addAttack(int pokemonId, const linkPokemonsAttacksNode &attack);

		void addAttack(int pokemonId, int attackId, int condition);

		void save();

		void verify();
	};

	bool operator<(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept;
	bool operator>(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept;
	bool operator==(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept;
	bool operator!=(const linkPokemonsAttacksNode &node1, const linkPokemonsAttacksNode &node2) noexcept;
}

#endif //POKEMONTITANE_EDITABLELISTPOKEMONSATTACKS_HPP
