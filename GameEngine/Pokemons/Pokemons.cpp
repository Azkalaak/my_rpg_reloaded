//
// Created by azkalaak on 22/02/2022.
//

#include "Pokemons.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

Pokemons::~Pokemons() = default;

Pokemons::Pokemons()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Pokemon file invalid! please generate / regenerate it");
}

void Pokemons::reload()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Pokemon file invalid! please generate / regenerate it");
}

Pokemon Pokemons::getPokemon(int pokemonId) const
{
	if (pokemonId <= 0 || pokemonId > _storage.header.pokemonNumber)
		throw Zenith::ZenithException("Invalid pokemon Id: ", pokemonId);

	Pokemon pkm(_storage.pokemons[pokemonId - 1]); // we substract one as the pokemons are not stored with their ID as key
	return pkm;
}
