//
// Created by azkalaak on 23/02/2022.
//

#ifndef POKEMONTITANE_EVOLUTIONS_HPP
#define POKEMONTITANE_EVOLUTIONS_HPP

#include "DataClass/Pokemon.hpp"
#include "Storage/StorageEvolutions.hpp"
#include "Zenith/ZenithGlobalSingleton.hpp"

namespace Titane {
	class Evolutions {
	private:
		StorageEvolutions _storage;

		const std::string _path = "./GameData/PokemonData/evolutions";

	protected:
		Evolutions();
	public:

		virtual ~Evolutions();

		void reload();

		[[nodiscard]] bool doesEvolve(int pkmId, int lvl) const;

		[[nodiscard]] int getEvolution(int pkmId, int lvl) const;
	};

	using EvolutionsFromStorage = Zenith::ZenithGlobalSingleton<Evolutions>;
}

#define EvolutionsFromStorage EvolutionsFromStorage::instance()

#endif //POKEMONTITANE_EVOLUTIONS_HPP
