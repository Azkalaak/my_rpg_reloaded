//
// Created by azkalaak on 23/02/2022.
//

#include <string>
#include "Encounters.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

Encounters::~Encounters() = default;

Encounters::Encounters()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Encounter file invalid! please generate / regenerate it");
}

void Encounters::reload()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Encounter file invalid! please generate / regenerate it");
}

const encounterNode &Encounters::getEncounter(Maps mapId)
{
	if (mapId > MAP_END)
		throw Zenith::ZenithException("Invalid map ID: ", mapId);
	if (_storage.numberPkm[mapId] == 0)
		throw Zenith::ZenithException("Map has no pokemon!");
	return _storage.nodes[mapId][_random() % _storage.numberPkm[mapId]];
}
