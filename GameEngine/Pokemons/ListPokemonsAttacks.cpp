//
// Created by azkalaak on 23/02/2022.
//

#include "ListPokemonsAttacks.hpp"
#include "Zenith/ZenithException.hpp"

using namespace Titane;

ListPokemonsAttacks::ListPokemonsAttacks()
{
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Link attack pokemon file invalid! please generate / regenerate it");
}

void ListPokemonsAttacks::reload()
{
	_storage.unload();
	if (!_storage.load(_path))
		throw Zenith::ZenithException("Link attack pokemon file invalid! please generate / regenerate it");
}

bool ListPokemonsAttacks::doesLearnAttack(int pkmId, int lvl) const
{
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	if (_storage.pokemonAttackNumber[pkmId] == 0)
		throw Zenith::ZenithException("Pokemon cannot learn attacks!", pkmId);
	for (int j = 0; j < _storage.pokemonAttackNumber[pkmId]; ++j) {
		if (_storage.nodes[pkmId][j].condition == lvl)
			return true;
	}
	return false;
}

int ListPokemonsAttacks::getLearnedAttack(int pkmId, int lvl) const
{
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	if (_storage.pokemonAttackNumber[pkmId] == 0)
		throw Zenith::ZenithException("Pokemon cannot learn attacks!", pkmId);
	for (int j = 0; j < _storage.pokemonAttackNumber[pkmId]; ++j) {
		if (_storage.nodes[pkmId][j].condition == lvl)
			return _storage.nodes[pkmId][j].attackId;
	}
	return -1;
}

std::list<int> ListPokemonsAttacks::getAllLearnedAttack(int pkmId, int lvl) const
{
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	if (_storage.pokemonAttackNumber[pkmId] == 0)
		throw Zenith::ZenithException("Pokemon cannot learn attacks!", pkmId);

	std::list<int> attacks;
	for (int j = 0; j < _storage.pokemonAttackNumber[pkmId]; ++j) {
		if (_storage.nodes[pkmId][j].condition == lvl)
			attacks.emplace_back(_storage.nodes[pkmId][j].attackId);
	}
	return attacks;
}

std::list<int> ListPokemonsAttacks::getAllLearnedAttackBeforeLevel(int pkmId, int lvl) const
{
	if (lvl < 1 || lvl > 100 || pkmId < 0 || pkmId > 151)
		throw Zenith::ZenithException("Invalid pokemon ID and level given: pkm ", pkmId, " : lvl ", lvl);
	if (_storage.pokemonAttackNumber[pkmId] == 0)
		throw Zenith::ZenithException("Pokemon cannot learn attacks!", pkmId);

	std::list<int> attacks;
	for (int j = 0; j < _storage.pokemonAttackNumber[pkmId]; ++j) {
		if (_storage.nodes[pkmId][j].condition <= lvl)
			attacks.emplace_back(_storage.nodes[pkmId][j].attackId);
	}
	return attacks;
}
