//
// Created by azkalaak on 21/02/2022.
//

#ifndef POKEMONTITANE_EDITABLEENCOUNTERS_HPP
#define POKEMONTITANE_EDITABLEENCOUNTERS_HPP

#include <map>
#include <list>
#include "Storage/StorageEncounter.hpp"

namespace Titane {
	class EditableEncounters {
	private:
		StorageEncounter _storage;
		std::map<int, std::list<encounterNode>> _nodes;

		const std::string _path = "./GameData/PokemonData/encounter";

	public:
		void load();

		void addEncounter(int mapId, const encounterNode &node);

		void addEncounter(int mapId, int pokemonId, int minLevel, int maxLevel);

		void save();

		void verify();
	};

	bool operator<(const encounterNode &node1, const encounterNode &node2) noexcept;
	bool operator>(const encounterNode &node1, const encounterNode &node2) noexcept;
	bool operator==(const encounterNode &node1, const encounterNode &node2) noexcept;
	bool operator!=(const encounterNode &node1, const encounterNode &node2) noexcept;

}

#endif //POKEMONTITANE_EDITABLEENCOUNTERS_HPP
