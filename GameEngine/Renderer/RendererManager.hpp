//
// Created by azkal on 09/12/2021.
//

#ifndef POKEMONTITANE_RENDERERMANAGER_HPP
#define POKEMONTITANE_RENDERERMANAGER_HPP

#include <SFML/Graphics.hpp>
#include "MapRenderer.hpp"
#include "AssetsManagers/TextureManager.hpp"
#include "PlayerRenderer.hpp"
#include "Entity.hpp"

namespace Titane {
	class RendererManager {
	private:
		MapRenderer _mapRenderer;

	public:

		void init(const MapManager &map);

		void Render(sf::RenderWindow &window, const MapManager &map, std::list<Entity> &);

	};
}

#endif //POKEMONTITANE_RENDERERMANAGER_HPP
