//
// Created by azkal on 09/12/2021.
//

#ifndef POKEMONTITANE_MAPRENDERER_HPP
#define POKEMONTITANE_MAPRENDERER_HPP

#include <SFML/Graphics.hpp>
#include "Storage/StorageToTextures.hpp"
#include "Map/MapManager.hpp"
#include "DataClass/EntityMovement.hpp"

namespace Titane {
	class MapRenderer {
	private:
		sf::IntRect _intRect {0, 0, 32, 32};
		sf::Vector2f _vec;
		sf::Sprite _sprite;

		sf::RenderTexture _fullMapTest[MAP_END][StorageToTextures::Layers::LAY_END];


	public:

		void init(const MapManager &map);

		void preRender(StorageToTextures::Layers layer, const MapManager &map, Maps actualMap);
		void
		RenderLayerBeta(sf::RenderWindow &window, StorageToTextures::Layers layer, const MapManager &map);

		void
		RenderLayer(sf::RenderWindow &window, StorageToTextures::Layers layer, const MapManager &map,
					Zenith::ZenithVector2i tileTopLeft = {{0, 0}}, Zenith::ZenithVector2i tileBotRight = {{60, 60}});
	};
}

#endif //POKEMONTITANE_MAPRENDERER_HPP
