//
// Created by azkal on 19/01/2022.
//

#ifndef POKEMONTITANE_PLAYERRENDERER_HPP
#define POKEMONTITANE_PLAYERRENDERER_HPP

#include <SFML/Graphics.hpp>
#include "Storage/StorageToTextures.hpp"
#include "DataClass/EntityMovement.hpp"

namespace Titane {
	class PlayerRenderer {
	private:
		sf::RenderWindow &_window;
		sf::Sprite _sprite;
		sf::IntRect _intRect;
		sf::Vector2f _vec;
		const EntityMovement &_player;

		int frame = 0;
		int backward = 1;

		uint8_t deltaFrame = 0;

	public:
		explicit PlayerRenderer(sf::RenderWindow &window, EntityMovement &player) : _window(window), _intRect(0, 0, 32, 42),
																					_player(player)
		{}

		void RenderPlayer();
	};
}

#endif //POKEMONTITANE_PLAYERRENDERER_HPP
