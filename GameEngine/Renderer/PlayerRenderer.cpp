//
// Created by azkal on 19/01/2022.
//

#include "PlayerRenderer.hpp"
#include "AssetsManagers/TextureManager.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

void PlayerRenderer::RenderPlayer()
{
	_vec.x = static_cast<float>(_player.position[1]);
	_vec.y = static_cast<float>(_player.position[0]);

	++deltaFrame;

	if (deltaFrame == 8) {
		deltaFrame = 0;
		frame += backward;
	}
	if (frame == -1 || frame == 3) {
		backward *= -1;
		frame += backward;
	}


	// player _sprite is 42 pixels high and 32 pixels large
	_intRect.top = _player.direction * 42;
	_intRect.left = frame * 32 + 32 * 3 * static_cast<int>(_player.speed == 4);

	if (_player.speedVector == Zenith::ZenithVector2i{{0, 0}})
		_intRect.left = 32;

	_sprite.setPosition(_vec);
	_sprite.setTexture(TitaneTextureManager.getTexture(StorageToTextures::MAN));
	_sprite.setTextureRect(_intRect);
	_window.draw(_sprite);
}
