//
// Created by azkal on 09/12/2021.
//

#include "MapRenderer.hpp"
#include "Zenith/ZenithException.hpp"
#include "Logs/Logger.hpp"
#include "Zenith/ZenithGlobalSingleton.hpp"
#include "AssetsManagers/TextureManager.hpp"

using namespace Titane;


void MapRenderer::init(const MapManager &map)
{
	for (int actualMap = 0; actualMap < MAP_END; ++actualMap) {
		TitaneTextureManager.loadIfNotLoaded(StorageToTextures::BOT, static_cast<Maps>(actualMap));
		TitaneTextureManager.loadIfNotLoaded(StorageToTextures::COL, static_cast<Maps>(actualMap));
		TitaneTextureManager.loadIfNotLoaded(StorageToTextures::TOP, static_cast<Maps>(actualMap));
		preRender(StorageToTextures::BOT, map, static_cast<Maps>(actualMap));
		preRender(StorageToTextures::COL, map, static_cast<Maps>(actualMap));
		preRender(StorageToTextures::TOP, map, static_cast<Maps>(actualMap));
	}
}

// =====================================================
// TODO: CHANGE THE INITIALISATION OF THE TEXTURE LOADER IN A LOADER SO IT IS LOADED ONLY ONCE FOR EACH MAP!
// TODO: I COULD TRY AGAIN TO DISPLAY TEXTURES ON A VERTEX ARRAY HOWEVER IT SEEMS TO BE SUPER INEFFECTIVE
// TODO: AS FOR NOW, THIS METHOD OF DRAWING WILL REPLACE THE PREVIOUS ONE
// TODO: MORE LOADING TIME, HOWEVER LESS RUNTIME USAGE.
// =====================================================
void MapRenderer::preRender(StorageToTextures::Layers layer, const MapManager &map, Maps actualMap)
{
	auto &text = TitaneTextureManager.getTexture(layer, actualMap);
	int32_t tileSize = 32;
	Zenith::ZenithVector2i botTile = {{map.getMap(actualMap).height(), map.getMap(actualMap).width()}};

	_fullMapTest[actualMap][layer].create(static_cast<unsigned int>(botTile[1] * 32),
										  static_cast<unsigned int>(botTile[0] * 32));
	_fullMapTest[actualMap][layer].clear(sf::Color::Transparent);


	map.draw({{0, 0}}, botTile, [this, actualMap, &text, &layer, &tileSize]
			(const Titane::MapNode &node, Zenith::ZenithVector2i position) {
		_vec.x = static_cast<float>(position[1] * tileSize);
		_vec.y = static_cast<float>(position[0] * tileSize);

		_sprite.setPosition(_vec);

		switch (layer) {
			case StorageToTextures::BOT:
				_intRect.left = tileSize * node.bg;
				break;
			case StorageToTextures::COL:
				_intRect.left = tileSize * node.mid;
				break;
			case StorageToTextures::TOP:
				_intRect.left = tileSize * node.top;
				break;
			case StorageToTextures::LAY_END:
				Titane::TitaneLogger.reportErr("Invalid layer given to draw function");
				throw Zenith::ZenithException("Invalid layer given!");
		}
		_sprite.setTexture(text);
		_sprite.setTextureRect(_intRect);
		_fullMapTest[actualMap][layer].draw(_sprite);
	}, actualMap);
	_fullMapTest[actualMap][layer].display();
}

void MapRenderer::RenderLayerBeta(sf::RenderWindow &window, StorageToTextures::Layers layer, const MapManager &map)
{
	auto &tmp = _fullMapTest[map.getMap()][layer];
	sf::Sprite sprite;
	sprite.setTexture(tmp.getTexture(), true);
	window.draw(sprite);
	// we try to fullrender the map? maybe I guess
}

void MapRenderer::RenderLayer(sf::RenderWindow &window, StorageToTextures::Layers layer, const MapManager &map,
							  Zenith::ZenithVector2i tileTopLeft, Zenith::ZenithVector2i tileBotRight)
{
	RenderLayerBeta(window, layer, map);
}
