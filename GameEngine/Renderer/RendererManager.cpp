//
// Created by azkal on 09/12/2021.
//

#include "RendererManager.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

void RendererManager::init(const MapManager &map)
{
	_mapRenderer.init(map);
}

void RendererManager::Render(sf::RenderWindow &window, const MapManager &map, std::list<Entity> &entities)
{
	auto &screenSize = window.getView().getSize();
	auto &screenCenter = window.getView().getCenter();
	Zenith::ZenithVector2i topLeft = {{
											  (static_cast<int>(screenCenter.y) - static_cast<int>(screenSize.y /2)) / 32 - 1,
											  (static_cast<int>(screenCenter.x) - static_cast<int>(screenSize.x /2)) / 32 - 1,
	}};
	Zenith::ZenithVector2i botRight = {{
											  (static_cast<int>(screenCenter.y) + static_cast<int>(screenSize.y /2)) / 32 + 1,
											  (static_cast<int>(screenCenter.x) + static_cast<int>(screenSize.x /2)) / 32 + 1,
									  }};
	_mapRenderer.RenderLayer(window, StorageToTextures::BOT, map, topLeft, botRight);
	_mapRenderer.RenderLayer(window, StorageToTextures::COL, map, topLeft, botRight);
	for (auto &it : entities)
		it.render();
	_mapRenderer.RenderLayer(window, StorageToTextures::TOP, map, topLeft, botRight);
}
