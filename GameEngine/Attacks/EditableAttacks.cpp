//
// Created by azkal on 03/02/2022.
//

#include "EditableAttacks.hpp"

#include "Zenith/ZenithException.hpp"

using namespace Titane;

bool Titane::operator<(const AttackNode &node1, const AttackNode &node2) noexcept
{
	return node1.id < node2.id;
}

bool Titane::operator>(const AttackNode &node1, const AttackNode &node2) noexcept
{
	return node1.id > node2.id;
}

bool Titane::operator==(const AttackNode &node1, const AttackNode &node2) noexcept
{
	return node1.id == node2.id;
}

bool Titane::operator!=(const AttackNode &node1, const AttackNode &node2) noexcept
{
	return node1.id != node2.id;
}

void EditableAttacks::load()
{
	if (!_storage.load("./GameData/PokemonData/attacks"))
		throw Zenith::ZenithException("Attack file invalid! please generate / regenerate it");

	for (int i = 0; i < _storage.header.nodeNumber; ++i)
	{
		_attacks.emplace_back(_storage.nodes[i]);
	}
}

void EditableAttacks::addAttack(const AttackNode &attack)
{
	_attacks.emplace_back(attack);
}

void
EditableAttacks::addAttack(const std::string &name, int id, int type, int category, int power, int precision, int pp,
						   int effect1, int effect2)
{
	AttackNode newNode = {
			.id = static_cast<uint8_t>(id),
			.name = {},
			.type = static_cast<uint8_t>(type),
			.category = static_cast<uint8_t>(category),
			.power = static_cast<uint8_t>(power),
			.precision = static_cast<uint8_t>(precision),
			.pp = static_cast<uint8_t>(pp),
			.effect1 = static_cast<uint8_t>(effect1),
			.effect2 = static_cast<uint8_t>(effect2),
	};
	for (unsigned long long i = 0; i < 16 && i < name.length(); ++i)
		newNode.name[i] = static_cast<uint8_t>(name[i]);
	addAttack(newNode);
}

void EditableAttacks::save()
{
	_attacks.sort();
	_attacks.unique();

	auto *tmpNodes = new AttackNode[_attacks.size()];
	int i = 0;

	for (auto &j : _attacks) {
		tmpNodes[i] = j;
		++i;
	}
	delete[] _storage.nodes;
	_storage.nodes = tmpNodes;
	_storage.header.nodeNumber = static_cast<uint8_t>(_attacks.size());

	if (!_storage.write("./GameData/PokemonData/attacks"))
		throw Zenith::ZenithException("Error while writing attack file to system");
}

void EditableAttacks::verify()
{
	_storage.unload();
	if (!_storage.load("./GameData/PokemonData/attacks"))
		throw Zenith::ZenithException("Corrupted attack file!");
}

bool EditableAttacks::doesAttackExist(const std::string &attackName) const noexcept
{
	for (auto &it : _attacks)
		if (reinterpret_cast<const char *>(it.name) == attackName)
			return true;
	return false;
}

int EditableAttacks::getAttackId(const std::string &attackName) const noexcept
{
	for (auto &it : _attacks)
		if (reinterpret_cast<const char *>(it.name) == attackName)
			return it.id;
	return -1;
}
