//
// Created by azkal on 03/02/2022.
//

#ifndef POKEMONTITANE_EDITABLEATTACKS_HPP
#define POKEMONTITANE_EDITABLEATTACKS_HPP

#include <list>
#include <string>
#include "Storage/StorageAttacks.hpp"

namespace Titane {
	class EditableAttacks {
	private:
		std::list<AttackNode> _attacks;

		StorageAttacks _storage;
	public:

		void load();

		void addAttack(const AttackNode &attack);

		void addAttack(const std::string &name, int id, int type, int category, int power, int precision,
						int pp, int effect1, int effect2);

		[[nodiscard]] bool doesAttackExist(const std::string &attackName) const noexcept;

		[[nodiscard]] int getAttackId(const std::string &attackName) const noexcept;

		void save();

		void verify();
	};

	bool operator<(const AttackNode &node1, const AttackNode &node2) noexcept;
	bool operator>(const AttackNode &node1, const AttackNode &node2) noexcept;
	bool operator==(const AttackNode &node1, const AttackNode &node2) noexcept;
	bool operator!=(const AttackNode &node1, const AttackNode &node2) noexcept;
}

#endif //POKEMONTITANE_EDITABLEATTACKS_HPP
