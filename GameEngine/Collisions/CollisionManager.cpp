//
// Created by azkal on 20/01/2022.
//

#include "CollisionManager.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

void CollisionManager::registerCharacter(EntityMovement &character)
{
	_colliderList.emplace_back(&character);
}

void CollisionManager::unregisterCharacter(EntityMovement &character)
{
	_colliderList.remove(&character);
}

void CollisionManager::tick()
{
	auto &map = _mapManager.getMap(_mapManager.getMap());
	// =============
	// Each character is compared to the current loaded map
	// =============
	for (auto &ref : _colliderList) {
		auto &i = *ref;
		if (i.idle || i.position[1] % 32 || (i.position[0] + 10) % 32)
			continue;
		int x = (i.position[1]) / 32;
		int y = (i.position[0] + 10) / 32;

		switch (i.direction) {
			case EntityMovement::UP:
				--y;
				break;
			case EntityMovement::DOWN:
				++y;
				break;
			case EntityMovement::RIGHT:
				++x;
				break;
			case EntityMovement::LEFT:
				--x;
				break;
			case EntityMovement::NONE:
				continue;
		}

		if (map.isCollision({{y, x}})) {
			i.speedVector = {{0, 0}};
			i.idle = true;
		}

		// =============
		// then to each other character (used for NPC collision)
		// The same loop is used to reduce compute delay and if collision already happened
		// =============
		if (i.idle)
			continue;
		for (auto &ref2 : _colliderList) {
			if (ref == ref2)
				continue;
		}
	}
}
