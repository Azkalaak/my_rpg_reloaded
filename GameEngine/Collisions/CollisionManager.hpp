//
// Created by azkal on 20/01/2022.
//

#ifndef POKEMONTITANE_COLLISIONMANAGER_HPP
#define POKEMONTITANE_COLLISIONMANAGER_HPP

#include <list>
#include "DataClass/EntityMovement.hpp"
#include "Map/MapManager.hpp"

namespace Titane {
	class CollisionManager {
	private:
		std::list<EntityMovement *> _colliderList;

		const MapManager &_mapManager;

	public:

		explicit CollisionManager(const MapManager &mapManager) : _mapManager(mapManager) {}

		void registerCharacter(EntityMovement &character);

		void unregisterCharacter(EntityMovement &character);

		void tick();

	};
}

#endif //POKEMONTITANE_COLLISIONMANAGER_HPP
