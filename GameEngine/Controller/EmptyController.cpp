//
// Created by azkal on 01/03/2022.
//

#include "EmptyController.hpp"

using namespace Titane;

void EmptyController::tick()
{

}

void EmptyController::activate(UserEvents &events)
{
	(void)events;
}

void EmptyController::deactivate(UserEvents &events)
{
	(void)events;
}
