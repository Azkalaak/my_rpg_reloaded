//
// Created by azkal on 20/01/2022.
//

#ifndef POKEMONTITANE_CONTROLLERMANAGER_HPP
#define POKEMONTITANE_CONTROLLERMANAGER_HPP

#include <array>
#include "CameraController.hpp"
#include "EmptyController.hpp"

namespace Titane {
	class ControllerManager {
	public:
		enum ControllerType {
			PLAYER, EMPTY
		};
	private:

		UserEvents &_events;

		CameraController _cameraController;
		EmptyController _emptyController;

		IClassicController *_actualController = &_emptyController;

	public:

		explicit ControllerManager(EntityMovement &player, UserEvents &events) : _events(events), _cameraController(player)
		{}

		void changeController(ControllerType type);

		void tick()
		{ _actualController->tick(); }
	};
}

#endif //POKEMONTITANE_CONTROLLERMANAGER_HPP
