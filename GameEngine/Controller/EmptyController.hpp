//
// Created by azkal on 20/01/2022.
//

#ifndef POKEMONTITANE_EMPTYCONTROLLER_HPP
#define POKEMONTITANE_EMPTYCONTROLLER_HPP

#include "IClassicController.hpp"

namespace Titane {
	class EmptyController : public IClassicController {
	public:
		~EmptyController() override = default;

		void activate(UserEvents &events) override;


		void deactivate(UserEvents &events) override;

		void tick() override;
	};
}

#endif //POKEMONTITANE_EMPTYCONTROLLER_HPP
