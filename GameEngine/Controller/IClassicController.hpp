//
// Created by azkal on 11/12/2021.
//

#ifndef POKEMONTITANE_ICONTROLLER_HPP
#define POKEMONTITANE_ICONTROLLER_HPP

#include "Events/UserEvents.hpp"
#include "DataClass/EntityMovement.hpp"

namespace Titane {
	class IClassicController {
	public:

		virtual ~IClassicController();
		// This function shall subscribe all needed elements to the event manager
		virtual void activate(UserEvents &events) = 0;

		// this function shall unsubscribe all needed elements to the event manager
		virtual void deactivate(UserEvents &events) = 0;

		// runs each frame, used to maintain events like onPressed
		virtual void tick() = 0;
	};
}

#endif //POKEMONTITANE_ICONTROLLER_HPP
