//
// Created by azkal on 11/12/2021.
//

#ifndef POKEMONTITANE_CAMERACONTROLLER_HPP
#define POKEMONTITANE_CAMERACONTROLLER_HPP

#include "IClassicController.hpp"
#include "Zenith/ZenithVector.hpp"
#include "DataClass/EntityMovement.hpp"

namespace Titane {
	class CameraController : public IClassicController {
	private:

//		bool _isMoving = false;

		int _moveCounter = 0;

		int _waitRotation = 0;

		int _speed = 1; // input speed

		EntityMovement::Direction _direction = EntityMovement::NONE;

		size_t _callbackMovement = 0;

		EntityMovement &_player;

		void move() noexcept;

	public:

		explicit CameraController(EntityMovement &player) : _player(player)
		{}

		~CameraController() override = default;

		void activate(UserEvents &events) override;

		void deactivate(UserEvents &events) override;

		void tick() override;

	};
}

#endif //POKEMONTITANE_CAMERACONTROLLER_HPP
