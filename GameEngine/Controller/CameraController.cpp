//
// Created by azkal on 11/12/2021.
//

#include "CameraController.hpp"
#include "Logs/Logger.hpp"

using namespace Titane;

void CameraController::activate(UserEvents &events)
{
	_callbackMovement = events.callback += [this](const bool arr[]) {
		if (arr[Left])
			_speed = 4;
		else
			_speed = 2;
		if (arr[Z])
			_direction = EntityMovement::UP;
		else if (arr[Q])
			_direction = EntityMovement::LEFT;
		else if (arr[S])
			_direction = EntityMovement::DOWN;
		else if (arr[D])
			_direction = EntityMovement::RIGHT;
		else
			_direction = EntityMovement::NONE;
	};
}

void CameraController::deactivate(UserEvents &events)
{
	events.callback -= _callbackMovement;
	_callbackMovement = 0;
	_player.idle = true;
	_direction = EntityMovement::NONE;
	_speed = 2;
}

void CameraController::move() noexcept
{
	if (_player.idle) {
		_player.speed = _speed;
		if (_direction != EntityMovement::NONE)
			_player.direction = _direction;
		_player.idle = false;
		switch (_direction) {
			case EntityMovement::UP:
				_player.speedVector = {{-1 * _player.speed, 0}};
				break;
			case EntityMovement::LEFT:
				_player.speedVector = {{0, -1 * _player.speed}};
				break;
			case EntityMovement::DOWN:
				_player.speedVector = {{1 * _player.speed, 0}};
				break;
			case EntityMovement::RIGHT:
				_player.speedVector = {{0, 1 * _player.speed}};
				break;
			case EntityMovement::NONE:
				_player.speedVector = {{0, 0}};
				_player.idle = true;
				break;
		}
	} else {
		if (_moveCounter < 32) {
			_player.position += _player.speedVector;
			_moveCounter += _player.speed;
		}
		if (_moveCounter == 32 ) {
			_player.idle = true;
			_moveCounter = 0;
		}
	}
}

void CameraController::tick()
{
	if (Zenith::ZenithVector2i{{0, 0}} == _player.speedVector &&
		_player.direction != _direction && _direction != EntityMovement::NONE) {
		_player.direction = _direction;
		_waitRotation = 1;
		return;
	}

	if (_waitRotation >= 10)
		_waitRotation = 0;
	else if (_waitRotation > 0)
		++_waitRotation;
	else
		move();
}
