//
// Created by azkal on 20/01/2022.
//

#include "ControllerManager.hpp"

using namespace Titane;

void ControllerManager::changeController(ControllerType type)
{
	_actualController->deactivate(_events);
	switch (type) {
		case PLAYER:
			_actualController = &_cameraController;
			break;
		case EMPTY:
			_actualController = &_emptyController;
	}
	_actualController->activate(_events);
}
