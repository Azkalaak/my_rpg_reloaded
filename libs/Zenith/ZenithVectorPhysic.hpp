//
// Created by azkal on 24/03/2021.
//

#ifndef FRAMEWORK_ZENITHVECTORPHYSIC_HPP
#define FRAMEWORK_ZENITHVECTORPHYSIC_HPP

#include "ZenithVector.hpp"

namespace Zenith {

	template<unsigned char size, typename vecType>
	class ZenithVectorPhysic {
	private:
		ZenithVector<size, vecType> m_force;
		ZenithVector<size, vecType> m_speed;
		bool isForceAcceleration = true;

		vecType densityOfOutside = 0.5;
		vecType dragCoefficient = 0.3;

	public:

		vecType bounciness = -1;
		vecType mass = 0.1f;

		ZenithVectorPhysic() = default;

		explicit ZenithVectorPhysic(ZenithVector<size, vecType> acceleration,
									ZenithVector<size, vecType> speed = ZenithVector<size, vecType>())
				: m_force(acceleration), m_speed(speed), isForceAcceleration(true)
		{

		}

		explicit ZenithVectorPhysic(ZenithVector<size, vecType> acceleration,
									vecType mass,
									ZenithVector<size, vecType> speed = ZenithVector<size, vecType>())
				: m_force(acceleration), m_speed(speed), isForceAcceleration(false), mass(mass)
		{

		}

		ZenithVectorPhysic<size, vecType> operator+(const ZenithVectorPhysic<size, vecType> &other)
		{
			return ZenithVectorPhysic<size, vecType>(getAcceleration() + other.getAcceleration(),
													 getSpeed(), other.getSpeed());
		}

		void setSpeed(ZenithVector<size, vecType> speed)
		{
			m_speed = speed;
		}

		const ZenithVector<size, vecType> &getAcceleration() const noexcept
		{ return m_force; }

		const ZenithVector<size, vecType> &getSpeed() const noexcept
		{ return m_speed; }

		// calculate the speed depending on the actual force vector (gravity, wind...)
		// it is using euler calculation method as it is easier for me to do it.
		void calcSpeed(float deltaTime = 1)
		{
			ZenithVector<size, vecType> realAcceleration = m_force; // taking in account the mass
			// new speed calculus
			if (!isForceAcceleration)
				realAcceleration /= mass;

			realAcceleration.forEach([this](unsigned char i) {
				auto multiplier = m_force[i] < 0 ? -1 : 1;
				return (float) (0.5 * densityOfOutside * dragCoefficient * m_force[i] * m_force[i]) * multiplier;
			});

			//vy * dt + (0.5 * ay * dt * dt);
			m_speed = m_speed + (realAcceleration * mass) * deltaTime;
		}

	};

	/**********************************************************************************/
	//             THIS PART IS FOR PRE-IMPLEMENTATIONS AND ALIASES.
	/**********************************************************************************/

	// 4 dimensions
	typedef ZenithVectorPhysic<4, float> ZenithVectorPhysic4f;
	typedef ZenithVectorPhysic<4, double> ZenithVectorPhysic4d;
	typedef ZenithVectorPhysic<4, int> ZenithVectorPhysic4i;
	typedef ZenithVectorPhysic<4, unsigned int> ZenithVectorPhysic4u;

	// 3 dimensions
	typedef ZenithVectorPhysic<3, float> ZenithVectorPhysic3f;
	typedef ZenithVectorPhysic<3, double> ZenithVectorPhysic3d;
	typedef ZenithVectorPhysic<3, int> ZenithVectorPhysic3i;
	typedef ZenithVectorPhysic<3, unsigned int> ZenithVectorPhysic3u;

	// 2 dimensions
	typedef ZenithVectorPhysic<2, float> ZenithVectorPhysic2f;
	typedef ZenithVectorPhysic<2, double> ZenithVectorPhysic2d;
	typedef ZenithVectorPhysic<2, int> ZenithVectorPhysic2i;
	typedef ZenithVectorPhysic<2, unsigned int> ZenithVectorPhysic2u;
}

#endif //FRAMEWORK_ZENITHVECTORPHYSIC_HPP
