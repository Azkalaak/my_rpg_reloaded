#ifndef UNTITLED_EPIVECTOR_HPP
#define UNTITLED_EPIVECTOR_HPP

#include <ostream>
#include <functional>
#include <cmath>

namespace Zenith {
// this is the dev talking.
// I HIGHLY suggest that you do some typedef to use this.
// you have been warned.
	template<size_t size, typename vecType>
	class ZenithVector {
	private: // constructor stuff
		typedef ZenithVector<size, vecType> thisVec;

		template<size_t index>
		void constructor()
		{
			if constexpr (index < size) {
				dimensions[index] = 0;
				constructor<index + 1>();
			}
		}

		template<size_t index, vecType value, vecType...Args>
		void constructor()
		{
			dimensions[index] = value;
			constructor<index + 1, Args...>();
		}

		template<size_t index>
		typename std::enable_if<(index < size), void>::type
		constructor(vecType init)
		{
			dimensions[index] = init;
		}

		template<size_t index, typename... Args>
		void constructor(vecType init, Args... args)
		{
			dimensions[index] = init;
			constructor<index + 1>(args...);
		}

	private: // private stuff

		template<size_t sizeVec2, typename firstVec, typename secondVec>
		firstVec &internal_add(firstVec &vec1, secondVec &vec2)
		{
			for (size_t i = 0; i < size; ++i)
				vec1[i] += vec2[i];
			return vec1;
		}

		template<size_t sizeVec2, typename firstVec, typename secondVec>
		firstVec &internal_sub(firstVec &vec1, secondVec &vec2)
		{
			for (size_t i = 0; i < sizeVec2; ++i)
				vec1[i] -= vec2[i];
			return vec1;
		}

		template<typename firstVec, typename secondVec>
		float internal_dot(firstVec &vec1, secondVec &vec2)
		{
			float result = 0;
			for (unsigned char i = 0; i < size; ++i)
				result += vec1[i] * vec2[i];
			return result;
		}

		template<size_t sizeVec2, typename firstVec, typename secondVec>
		firstVec &internal_cross(firstVec &vec1, secondVec &vec2)
		{
			firstVec resVec;
			for (unsigned char i = 0; i < size; ++i)
				vec1[i] -= vec2[i];
			return vec1;
		}

		float internal_abs()
		{
			float result = 0;
			for (size_t i = 0; i < size; ++i)
				result += dimensions[i] * dimensions[i];
			result = std::sqrt(result);
			return result;
		}

		template<size_t index>
		typename std::enable_if<(index < size), void>::type
		internal_clamp(vecType valueMin, vecType valueMax)
		{
			if (valueMin > valueMax)
				internal_clamp<index>(valueMax, valueMin);
			if (dimensions[index] < valueMin)
				dimensions[index] = valueMin;
			else if (dimensions[index] > valueMax)
				dimensions[index] = valueMax;
		}

		template<int index, typename... Args>
		void internal_clamp(vecType valueMin, vecType valueMax, Args... args)
		{
			if (valueMin > valueMax)
				internal_clamp<index>(valueMax, valueMin, args...);
			if (dimensions[index] < valueMin)
				dimensions[index] = valueMin;
			else if (dimensions[index] > valueMax)
				dimensions[index] = valueMax;
			internal_clamp<index + 1>(args...);
		}

		static_assert(size > 1, "A vector cannot hold less than 2 dimensions"); // dimension verification

		vecType dimensions[size] = {0};

	public: // public stuff

		ZenithVector()
		{
			constructor<0>();
		}

		template<typename... Args>
		explicit ZenithVector(Args... args)
		{
			constructor<0>(args...);
		}

		ZenithVector(std::initializer_list<vecType> array)
		{
			size_t i = 0;
			for (auto it = array.begin(); it != array.end(); ++it, ++i) {
				dimensions[i] = *it;
			}
		}

		//initialisation function, may get removed in the futur, dunno
		template<vecType... args>
		typename std::enable_if<(sizeof...(args) <= size), void>::type
		init()
		{
			constructor<0, args...>();
		}

		template<typename... Args>
		typename std::enable_if<(sizeof...(Args) <= size * 2), void>::type
		clamp(Args... args)
		{
			internal_clamp<0>(args...);
		}

		void round(float roundness = 100)
		{
			for (size_t i = 0; i < size; ++i)
				dimensions[i] = std::round(dimensions[i] * roundness) / roundness;
		}

		void forEach(std::function<vecType(unsigned char)> func)
		{
			for (size_t i = 0; i < size; ++i)
				dimensions[i] = func(i);
		}

// const access function, the dimensions are sorted in the order. there is no security even thought it would be easy to implement
		vecType operator[](size_t index) const
		{
			return dimensions[index];
		}

		// reference access function, same as const access one without being const
		vecType &operator[](size_t index)
		{
			return dimensions[index];
		}

		// getter of the angle between this vector and the other one given.
		/*template<typename firstVec, typename secondVec>
		float getAngle(ZenithVector<size, firstVec> &vec1, ZenithVector<size, secondVec> &vec2) {
			return std::acos(internal_dot(vec1, vec2) / vec1.getAbsolute() * vec2.getAbsolute()) * (180 / std::numbers::pi);
		}*/

		// get the normalized vector corresponding to this one
		thisVec getNormalized()
		{
			return *this / internal_abs();
		}

		// get the absolute value of this vector
		float getAbsolute()
		{
			return internal_abs();
		}

	};

// addition operator overload with a scalar
	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> operator+(const ZenithVector<size, vecType> &vec, const type &var)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < size; ++i)
			retValue[i] = vec[i] + var;
		return retValue;
	}

	// addition operator overload with a scalar
	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> &operator+=(ZenithVector<size, vecType> &vec, const type &var)
	{
		for (size_t i = 0; i < size; ++i)
			vec[i] += var;
		return vec;
	}

// addition operator overload with another vector, always returns the bigger one
	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size <= sizeVec2), ZenithVector<sizeVec2, vecType2>>::type
	operator+(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		ZenithVector<sizeVec2, vecType2> retValue;

		for (size_t i = 0; i < sizeVec2; ++i)
			retValue[i] = vec1[i] + vec2[i];
		for (size_t i = sizeVec2 - (sizeVec2 - size); i < sizeVec2; ++i)
			retValue[i] = vec2[i];
		return retValue;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size > sizeVec2), ZenithVector<size, vecType>>::type
	operator+(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		return vec2 + vec1;
	}

	// addition operator overload with another vector
	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size <= sizeVec2), ZenithVector<size, vecType> &>::type
	operator+=(ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		for (size_t i = 0; i < size; ++i)
			vec1[i] += vec2[i];
		return vec1;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size > sizeVec2), ZenithVector<size, vecType> &>::type
	operator+=(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		for (size_t i = 0; i < sizeVec2; ++i)
			vec1[i] += vec2[i];
		return vec1;
	}

// same but with minus operator
	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size <= sizeVec2), ZenithVector<sizeVec2, vecType2>>::type
	operator-(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		ZenithVector<sizeVec2, vecType2> retValue;

		for (size_t i = 0; i < sizeVec2; ++i)
			retValue[i] = vec1[i] - vec2[i];
		for (size_t i = sizeVec2 - (sizeVec2 - size); i < sizeVec2; ++i)
			retValue[i] = vec2[i];
		return retValue;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size > sizeVec2), ZenithVector<size, vecType>>::type
	operator-(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < sizeVec2; ++i)
			retValue[i] = vec1[i] - vec2[i];
		for (size_t i = size - (size - sizeVec2); i < size; ++i)
			retValue[i] = vec1[i];
		return retValue;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size < sizeVec2), ZenithVector<sizeVec2, vecType2> &>::type
	operator-=(ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		for (size_t i = 0; i < sizeVec2; ++i)
			vec1[i] -= vec2[i];
		for (size_t i = sizeVec2 - (sizeVec2 - size); i < sizeVec2; ++i)
			vec1[i] = vec2[i];
		return vec1;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size >= sizeVec2), ZenithVector<size, vecType> &>::type
	operator-=(ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		for (size_t i = 0; i < sizeVec2; ++i)
			vec1[i] -= vec2[i];
		return vec1;
	}

// cross product, can only work with 3 dimensionnal vectors for now
	template<size_t sizeVec1, typename vecType1, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(sizeVec1 == sizeVec2 && sizeVec2 == 3), ZenithVector<sizeVec1, vecType1>>::type
	operator*(ZenithVector<sizeVec1, vecType1> vec1, ZenithVector<sizeVec2, vecType2> &vec2)
	{
		ZenithVector<sizeVec2, vecType2> retValue = {{vec1[1] * vec2[2] - vec2[1] * vec1[2],
												 vec1[2] * vec2[0] - vec2[2] * vec1[0],
												 vec1[0] * vec2[1] - vec2[0] * vec1[1]}};
		return retValue;
	}

	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> operator-(ZenithVector<size, vecType> vec, type var)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < size; ++i)
			retValue[i] = vec[i] - var;
		return retValue;
	}

	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> &operator-=(ZenithVector<size, vecType> &vec, type var)
	{
		for (size_t i = 0; i < size; ++i)
			vec[i] -= var;
		return vec;
	}

// vector division
	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> operator/(const ZenithVector<size, vecType> &vec, type var)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < size; ++i)
			retValue[i] = vec[i] / var;
		return retValue;
	}

	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> &operator/=(ZenithVector<size, vecType> &vec, type var)
	{
		for (size_t i = 0; i < size; ++i)
			vec[i] = vec[i] / var;
		return vec;
	}

	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> operator*(ZenithVector<size, vecType> vec, type var)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < size; ++i)
			retValue[i] = vec[i] * var;
		return retValue;
	}

	template<size_t size, typename vecType, typename type>
	ZenithVector<size, vecType> operator*(type var, ZenithVector<size, vecType> vec)
	{
		ZenithVector<size, vecType> retValue;

		for (size_t i = 0; i < size; ++i)
			retValue[i] = vec[i] * var;
		return retValue;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size == sizeVec2), bool>::type
	operator==(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		for (size_t i = 0; i < size; ++i)
			if (vec1[i] != vec2[i])
				return false;
		return true;
	}

	template<size_t size, typename vecType, size_t sizeVec2, typename vecType2>
	typename std::enable_if<(size == sizeVec2), bool>::type
	operator!=(const ZenithVector<size, vecType> &vec1, const ZenithVector<sizeVec2, vecType2> &vec2)
	{
		return !(vec1 == vec2);
	}

// print the vector in the following format:
// dim1 : dim2 : dim3 : dimx
	template<unsigned char size, typename vectorType>
	std::ostream &operator<<(std::ostream &o, const ZenithVector<size, vectorType> &vec)
	{
		size_t i = 0;
		do {
			o << vec[i];
			o << " : ";
			++i;
		} while (i < size - 1);
		o << vec[size - 1];
		return o;
	}

	/**********************************************************************************/
	//             THIS PART IS FOR PRE-IMPLEMENTATIONS AND ALIASES.
	/**********************************************************************************/

	// 4 dimensions
	typedef ZenithVector<4, float> ZenithVector4f;
	typedef ZenithVector<4, double> ZenithVector4d;
	typedef ZenithVector<4, int> ZenithVector4i;
	typedef ZenithVector<4, unsigned int> ZenithVector4ui;

	// 3 dimensions
	typedef ZenithVector<3, float> ZenithVector3f;
	typedef ZenithVector<3, double> ZenithVector3d;
	typedef ZenithVector<3, int> ZenithVector3i;
	typedef ZenithVector<3, unsigned int> ZenithVector3ui;

	// 2 dimensions
	typedef ZenithVector<2, float> ZenithVector2f;
	typedef ZenithVector<2, double> ZenithVector2d;
	typedef ZenithVector<2, int> ZenithVector2i;
	typedef ZenithVector<2, unsigned int> ZenithVector2ui;
}


#endif //UNTITLED_EPIVECTOR_HPP
