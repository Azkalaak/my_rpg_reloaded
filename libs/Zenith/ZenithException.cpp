//
// Created by azkal on 01/03/2022.
//

#include "ZenithException.hpp"

using namespace Zenith;

const char *ZenithException::what() const noexcept
{
	return problem.c_str();
}
