//
// Created by azkal on 11/12/2021.
//

#ifndef POKEMONTITANE_ZENITHCONDITIONALEVENT_HPP
#define POKEMONTITANE_ZENITHCONDITIONALEVENT_HPP

#include "ZenithEvent.hpp"

namespace Zenith {

	template<typename... aAgs>
	struct TypeList {
	};

	// dummy declaration to allow prettier one
	template<typename params, typename retType, typename... Args>
	class ZenithConditionalEvent;

	// Cute lil' template :)
	template<typename... condParam, typename retType, typename... Args>
	class ZenithConditionalEvent<Zenith::TypeList<condParam...>, retType(Args...)> {
	public:

		// alias of the types used for better reading comprehension
		using func1Template = std::function<bool(condParam...)>;
		using func2Template = std::function<retType(Args...)>;
		using pairTemplate = std::pair<func1Template, func2Template>;
		using pairSignature = std::pair<unsigned long, unsigned long>;

		ZenithConditionalEvent() = default;

		// to add condition + execution
		pairSignature operator+=(pairTemplate pair)
		{
			_callbackList.emplace_back(pair);
			return {pair.first.target_type().hash_code(), pair.second.target_type().hash_code()};
		}

		// to remove a condition (from signature)
		void operator-=(pairSignature signature)
		{
			auto it = _callbackList.begin();
			for (; ((*it).first.target_type.hash_code() != signature.first ||
					(*it).first.target_type.hash_code() != signature.second)
				   && it != _callbackList.end(); ++it);
			if (it == _callbackList.end())
				return;
			_callbackList.erase(it);
		}

		// fires every element matching the condition. Please don't put execution code inside the condition
		void fireIf(condParam... params, Args... args)
		{
			for (auto &i: _callbackList)
				if (i.first(params...))
					i.second(args...);
		}

		void fireIfSafe(condParam... params, Args... args)
		{
			std::string what;

			for (auto &i: _callbackList) {
				try {
					if (i.first(params...))
						i.second(args...);
				} catch (const std::exception &e) {
					// hash code of the function + exception message.
					what += std::to_string(i.target_type().hash_code()) + " : " + e.what();
				}
			}
			if (!what.empty())
				throw ZenithException("Exception caught during event fire: " + what);
		}

	private:
		std::vector<pairTemplate> _callbackList;
	};
}

#endif //POKEMONTITANE_ZENITHCONDITIONALEVENT_HPP
