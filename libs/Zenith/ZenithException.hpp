//
// Created by azkalaak on 21. 3. 21..
//

#ifndef ZENITH_ZENITHEXCEPTION_HPP
#define ZENITH_ZENITHEXCEPTION_HPP

#include <utility>
#include <sstream>

namespace Zenith {

	class ZenithException : public std::exception {
	private:
		template<typename Arg>
		void concatenateMessage(std::stringstream &stream, Arg arg) const
		{
			stream << arg;
		}

		template<typename Arg, typename ...Args>
		void concatenateMessage(std::stringstream &stream, Arg arg, Args ...args) const
		{
			stream << arg;
			concatenateMessage(stream, args...);
		}
	public:
		explicit ZenithException(std::string message) : problem(std::move(message))
		{
		}

		template<typename... Args>
		explicit ZenithException(Args...args)
		{
			std::stringstream stream;
			concatenateMessage(stream, args...);
			problem = stream.str();
		}

		[[nodiscard]] const char *what() const noexcept override;

	private:
		std::string problem{};
	};
}

#endif //ZENITH_ZENITHEXCEPTION_HPP
