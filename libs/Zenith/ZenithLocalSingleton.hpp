//
// Created by azkal on 23/03/2021.
//

#ifndef ZENITH_ZENITHLOCALSINGLETON_HPP
#define ZENITH_ZENITHLOCALSINGLETON_HPP

#include <mutex>
#include "ZenithException.hpp"

namespace Zenith {
	// if ever the class actually does not inherit from the singleton, it will not be able to access its constructor.
	template<class childName>
	class ZenithLocalSingleton {
	private:
		// static mutex to allow only one instance at a time in a thread safe way
		static std::mutex singletonLock;

	protected:
		// protected constructor only classes that inherit from our friend can call it
		ZenithLocalSingleton()
		{
			if (!singletonLock.try_lock())
				throw ZenithException("Singleton is already invoked!!");
			return;
			static_assert(!std::is_base_of<childName, ZenithLocalSingleton<childName>>::value,
						  "Please pass as template argument the type of your class. it must inherit from this one");
		}

	public:

		// release of the mutex so another instance can be created after the destruction of this one
		virtual ~ZenithLocalSingleton()
		{ singletonLock.unlock(); }

		// we delete other constructors
		ZenithLocalSingleton(const ZenithLocalSingleton &) = delete;

		ZenithLocalSingleton(ZenithLocalSingleton &&) = delete;

		ZenithLocalSingleton &operator=(const ZenithLocalSingleton &) = delete;

		ZenithLocalSingleton &operator=(ZenithLocalSingleton &&) = delete;
	};

	template<typename childName>
	std::mutex ZenithLocalSingleton<childName>::singletonLock = std::mutex();
}

#endif //ZENITH_ZENITHLOCALSINGLETON_HPP
