//
// Created by azkal on 27/03/2021.
//

#ifndef FRAMEWORK_ZENITHCOLLIDER_HPP
#define FRAMEWORK_ZENITHCOLLIDER_HPP

namespace Zenith::Abstracts {
	class ZenithCollider {
	public:
		ZenithCollider() = default;;

		virtual int doesCollide(const ZenithCollider &zenithCollider);

		virtual ~ZenithCollider() = 0;
	};
}

#endif //FRAMEWORK_ZENITHCOLLIDER_HPP
