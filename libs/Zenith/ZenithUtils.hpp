//
// Created by azkal on 25/01/2022.
//

#ifndef POKEMONTITANE_ZENITHUTILS_HPP
#define POKEMONTITANE_ZENITHUTILS_HPP

#include <vector>
#include <string>
#include <functional>

namespace Zenith {
	class Utils {
	public:

		template<typename T>
		static std::vector<T> split(std::string str, std::function<T(const std::string &)> func, const std::string &sep = " ")
		{
			std::vector<T> vec;
			for (unsigned long long pos = str.find(sep); pos != std::string::npos; pos = str.find(sep))
			{
				std::string token = str.substr(0, pos);
				if (!token.empty())
					vec.emplace_back(func(token.erase(0, token.find_first_not_of(sep))));
				str.erase(0, pos + sep.length());
			}
			if (!str.empty())
				vec.emplace_back(func(str.erase(0, str.find_first_not_of(sep))));
			return vec;
		}
	};
}

#endif //POKEMONTITANE_ZENITHUTILS_HPP
