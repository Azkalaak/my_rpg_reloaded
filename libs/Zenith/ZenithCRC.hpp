//
// Created by azkal on 08/04/2021.
//

#ifndef POKEMONTITANE_ZENITHCRC_HPP
#define POKEMONTITANE_ZENITHCRC_HPP

namespace Zenith {

	// CRC 32 bits
	class ZenithCRC {
	private:
		uint32_t crc = 0;

	public:
		void feed(const void *buff, size_t buffSize) noexcept
		{
			static constexpr uint64_t POLY = 0x82f63b78;
			const auto *buf = static_cast<const unsigned char *>(buff);

			for (size_t i = 0; i < buffSize; ++i) {
				crc ^= buf[i];
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
				crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
			}
		}

		void setCRC(uint32_t newCRC) noexcept
		{ crc = newCRC; }

		[[nodiscard]] uint32_t getCRC() const noexcept
		{ return crc; }
	};
}

#endif //POKEMONTITANE_ZENITHCRC_HPP
