//
// Created by azkalaak on 21. 3. 21..
//

#ifndef ZENITH_ZENITHGLOBALSINGLETON_HPP
#define ZENITH_ZENITHGLOBALSINGLETON_HPP


#include <mutex>

namespace Zenith {

	template<typename Child>
	class ZenithGlobalSingleton : public Child {
	private:
		static inline ZenithGlobalSingleton<Child> *child {nullptr};
	public:

		static inline Child &instance()
		{
			static auto *tmp = child = new ZenithGlobalSingleton<Child>;
			return *child;
			(void)tmp;
		}

		ZenithGlobalSingleton(const ZenithGlobalSingleton &) = delete;

		ZenithGlobalSingleton(ZenithGlobalSingleton &&) = delete;

		ZenithGlobalSingleton &operator=(const ZenithGlobalSingleton &) = delete;

		ZenithGlobalSingleton &operator=(ZenithGlobalSingleton &&) = delete;

	protected:

		ZenithGlobalSingleton() = default;

		~ZenithGlobalSingleton() override { delete child; }

	};
}

#endif //ZENITH_ZENITHGLOBALSINGLETON_HPP
