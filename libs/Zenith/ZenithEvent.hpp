//
// Created by azkal on 20/03/2021.
//

#ifndef ZENITH_ZENITHEVENT_HPP
#define ZENITH_ZENITHEVENT_HPP


#include <functional>
#include <string>
#include "ZenithException.hpp"

namespace Zenith {
// overload of operator == for comparison between std::function. if collision with other libs, then will put it into a namespace
	template<typename retType, typename... Args>
	bool
	operator==(const std::function<retType(Args...)> &function1, const std::function<retType(Args...)> &function2)
	{
		return function1.target_type().hash_code() == function2.target_type().hash_code();
	}

	template<typename retType, typename... Args>
	bool
	operator!=(const std::function<retType(Args...)> &function1, const std::function<retType(Args...)> &function2)
	{
		return function1.target_type().hash_code() != function2.target_type().hash_code();
	}

// dummy implementation for possibility of pretty template
	template<typename retType, typename... Args>
	class ZenithEvent;

	template<typename retType, typename... Args>
	class ZenithEvent<retType(Args...)> {
	public:
		ZenithEvent() = default;

		// add a function into the event list
		// PLEASE USE A LAMBDA FOR MEMBER FUNCTIONS, STD::BIND IS NOT AND WILL NOT BE SUPPORTED
		size_t operator+=(std::function<retType(Args...)> function)
		{
			callbacks.emplace_back(function);
			return function.target_type().hash_code();
		}

		// removes a function from the event list
		// THIS DOES NOT WORK FOR LAMBDA, PLEASE USE THE ULONG RETURNED BY THE ADDING FUNCTION TO REMOVE IT
		void operator-=(std::function<retType(Args...)> function)
		{
			auto it = callbacks.begin();
			for (; *it != function && it != callbacks.cend(); ++it);
			if (it == callbacks.end())
				return;
			callbacks.erase(it);
		}

		// removes a function from the event list using an ulong
		// this function is here to provide lambda support
		void operator-=(size_t hashCode)
		{
			auto it = callbacks.begin();
			if (it == callbacks.end())
				return;
			for (; it != callbacks.end() && (*it).target_type().hash_code() != hashCode; ++it);
			if (it == callbacks.end())
				return;
			callbacks.erase(it);
		}

		// fires all callbacks. will probably be in protected soon
		void fire(Args... args)
		{
			for (auto &i: callbacks)
				i(args...);
		}

		// throws FunctionExceptionError in case of an exception.
		// do not catch exceptions that does not inherit from std::exception
		// THIS FUNCTION SHOULD ONLY BE USED TO DEBUG AND NEVER IN PRODUCTION AS THE EXCEPTION THREW CAN BE DANGEROUS
		void fireSafe(Args... args)
		{
			std::string what;

			for (auto &i: callbacks) {
				try {
					i(args...);
				} catch (const std::exception &e) {
					// hash code of the function + exception message.
					what += std::to_string(i.target_type().hash_code()) + " : " + e.what();
				}
			}
			if (!what.empty())
				throw ZenithException("Exception caught during event fire: " + what);
		}

	protected:


	private:

		// list of callbacks
		std::vector<std::function<retType(Args...)>> callbacks;
	};
}
#endif //ZENITH_ZENITHEVENT_HPP
