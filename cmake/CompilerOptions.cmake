if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
	add_compile_options(
			-Weverything
			-Wno-c++98-compat
			-Wno-c++98-compat-pedantic
			-Wno-system-headers
			-Wno-padded
			-Wno-packed
			-Wno-disabled-macro-expansion
	)
endif()

if(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
	add_compile_options(
			-W
			-Wall
			-Wextra
			-Wshadow
			-Wnon-virtual-dtor
			-pedantic
			-Wold-style-cast
			-Wcast-align
			-Wunused
			-Woverloaded-virtual
			-Wpedantic
			-Wconversion
			-Wsign-conversion
			-Wmisleading-indentation
			-Wduplicated-cond
			-Wduplicated-branches
			-Wlogical-op
			-Wnull-dereference
			-Wuseless-cast
			-Wdouble-promotion
			-Wformat=2
	)
endif()