macro(run_conan)

    if (NOT EXISTS "${CMAKE_BINARY_DIR}/download/conan.cmake")
        message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
        file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
                "${CMAKE_BINARY_DIR}/download/conan.cmake")
    endif ()

    include(${CMAKE_BINARY_DIR}/download/conan.cmake)


    conan_add_remote(NAME bincrafters URL https://bincrafters.jfrog.io/artifactory/api/conan/public-conan)

    execute_process(COMMAND conan config set general.revisions_enabled=1)
    execute_process(COMMAND conan config set settings.compiler.cppstd=20)


    if(CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang" AND WIN32)
        set(IS_DEBUG Debug)
    else()
        set(IS_DEBUG ${CMAKE_BUILD_TYPE})
    endif()
    # cmake-format: off
    conan_cmake_run(
            BASIC_SETUP
            NO_OUTPUT_DIRS
            CMAKE_TARGETS

            CONANFILE conanfile.txt
            BUILD missing
            INSTALL_FOLDER ${CMAKE_BINARY_DIR}/conan
            BUILD_TYPE ${IS_DEBUG}
    )
    # cmake-format: on

endmacro()